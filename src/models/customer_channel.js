/**
 * Created by chenxq on 2017/12/12.
 */
import {customerChannelApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace:'customer_channel',
  state:{
    list:[],
    loading:false,
    total:0
  },
  effects:{
    *fetchList({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(customerChannelApi.queryCustomerChannelList,payload);
      if(jsonData.result){
        yield put({
          type: 'saveList',
          payload:jsonData.data || []
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });

    },
    *fetchAdd({payload},{call,put}){
      let jsonData=yield call(customerChannelApi.addCustomerChannel,payload);
      return jsonData;
    },
    *fetchEdit({payload},{call,put}){
      let jsonData=yield call(customerChannelApi.editCustomerChannel,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      let jsonData=yield call(productApi.removeCustomerChannel,payload);
      return jsonData;
    }
  },
  reducers:{
    changeLoading(state,{payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state,{payload}){
      return {
        ...state,
        list:payload.list || [],
        total:payload.total || 0
      }
    }
  }

}
