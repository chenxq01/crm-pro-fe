/**
 * Created by chenxq on 2017/12/25.
 */
//企业
import {EnterpriseApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace:'creditCard',
  state:{
    list:[],
    loading:false,
  },
  effects:{
    *fetchList({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(CreditCardApi.queryCreditCardList,payload);
      console.log('jsonDAta',jsonData);
      if(jsonData.result){
        yield put({
          type: 'saveList',
          payload:jsonData.data || []
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });

    },
    *fetchAdd({payload},{call,put}){
      let jsonData=yield call(CreditCardApi.addCreditCard,payload);
      return jsonData;
    },
    *fetchEdit({payload},{call,put}){
      let jsonData=yield call(CreditCardApi.editCreditCard,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      let jsonData=yield call(CreditCardApi.removeCreditCard,payload);
      return jsonData;
    },
  },
  reducers:{
    changeLoading(state,{payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state,{payload}){
      console.log('sabe',payload)
      return {
        ...state,
        list:payload || [],
      }
    },

  }

}
