/**
 * Created by chenxq on 2017/12/11.
 */
import {groupManageApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace:'group',
  state:{
    loading:false,
    groupList:[],
    sameGroupList:[]
  },
  effects:{
    *fetchList(_, { call, put }){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(groupManageApi.queryGroupList);
      if(jsonData.result){
        yield put({
          type: 'saveList',
          payload:jsonData.data || []
        })
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });
      return jsonData;
    },
    *fetchSameGroupUserList({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(groupManageApi.getSameGroupUser);
      if(jsonData.result){
        yield put({
          type: 'saveSameList',
          payload:jsonData.data || []
        })
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *fetchAdd({payload},{ call, put }){
      const jsonData=yield call(groupManageApi.addGroup,payload);
      return jsonData;
    },
    *fetchEdit({payload},{call,put}){
      const jsonData=yield call(groupManageApi.editGroup,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      const jsonData=yield call(groupManageApi.removeGroup,payload)
      return jsonData;
    }

  },
  reducers:{
    changeLoading(state,{payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state,{payload}){
      return {
        ...state,
        groupList:payload
      }
    },
    saveSameList(state,{payload}){
      return {
        ...state,
        sameGroupList:payload
      }
    }
  }
}
