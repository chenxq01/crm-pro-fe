/**
 * Created by chenxq on 2017/12/29.
 */
import {ChartApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace:'chart',
  state:{
    signData:{},
    signList:[]
  },
  effects:{
    *fetchSignData({payload},{call,put}){
      let jsonData=yield call(ChartApi.getLoanchart,payload);
      yield put({
        type:'saveSignData',
        payload:jsonData.data || {}
      })
      return jsonData;
    }
  },
  reducers:{
    saveSignData(state,{payload}){
      return {
        ...state,
        signData:payload
      }
    }
  }
}
