/**
 * Created by chenxq on 2017/12/28.
 */
import {AccountApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace:'account',
  state:{
    list:[],  //通过 customerId 获取的list
    icList:[], // 通过loanid 获取的list
    loading:false,
    total:0,
    pagination:{}
  },
  effects:{
    *fetchList({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      yield put({
        type:'setPagination',
        payload
      })
      const jsonData = yield call(AccountApi.queryAccountList,payload);
      if(jsonData.result){
        yield put({
          type: 'saveList',
          payload:jsonData.data || {}
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });

    },
    *fetchListByLoanId({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(AccountApi.queryAccountListByLoanId,payload);
      if(jsonData.result){
        yield put({
          type: 'saveListByLoanId',
          payload:jsonData.data || {}
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *fetchAdd({payload},{call,put,select}){
      let jsonData=yield call(AccountApi.addAccount,payload);
      return jsonData;
    },
    *fetchEdit({payload},{call,put}){
      let jsonData=yield call(AccountApi.editAccount,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      let jsonData=yield call(AccountApi.removeAccount,payload);
      return jsonData;
    },
  },
  reducers:{
    changeLoading(state,{payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state,{payload}){
      return {
        ...state,
        list:payload.list || [],
        total:payload.total || 0
      }
    },
    saveListByLoanId(state,{payload}){
      return {
        ...state,
        icList:payload || []
      }
    },
    setPagination(state,{payload}){
      return {
        ...state,
        pagination:payload
      }
    }

  }

}
