/**
 * Created by chenxq on 2017/12/22.
 */
import { routerRedux } from 'dva/router';
import {signApi} from './../services/api';

export default {
  namespace: 'sign',
  state: {
    list:[],
    total:0,
    allList:[],
    loading:false,
    detail:{}
  },

  effects: {
    *fetchList({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(signApi.querySignList, payload);
      if(response.result){
        yield put({
          type: 'saveList',
          payload: response.data || {},
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *querySignListById({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(signApi.querySignListById, payload);
      if(response.result){
        yield put({
          type: 'saveAllList',
          payload: response.data || [],
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *fetchAdd({payload},{call,put}){
      const jsonData= yield call(signApi.addSign,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      const jsonData= yield call(signApi.removeSign,payload);
      return jsonData
    },
    *fetchEdit({payload},{call,put}){
      const jsonData=yield call(signApi.editSign,payload);
      if(jsonData.result){
        yield put({
          type:'saveCurrentCustomer',
          payload
        });
      }
      return jsonData;
    },
    *fetchGetDetail({payload},{call,put}){
      const jsonData=yield call(signApi.getSignDetail,payload);
      yield put({
        type:'saveDetail',
        payload:jsonData.data || {}
      })
    },
    *assign({payload},{call,put}){
      const jsonData=yield call(signApi.signAssign,payload);
      return jsonData;
    }


  },
  reducers: {
    saveList(state, { payload }) {
      return {
        ...state,
        list:payload.list || [],
        total:payload.total
      };

    },
    saveAllList(state,{payload}){
      return {
        ...state,
        allList:payload || [],
      };
    },
    changeLoading(state, { payload }) {
      return {
        ...state,
        loading: payload,
      };
    },
    saveDetail(state,{payload}){
      return {
        ...state,
        detail:payload || {}
      }
    }

  },
};
