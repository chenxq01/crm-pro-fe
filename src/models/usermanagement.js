/**
 * Created by chenxq on 2017/12/11.
 */
import {userManageApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace:'usermanagement',
  state:{
    loading:false,
    userList:[], // 分页
    allList:[], // 全部
    userTotal:0,
    roleList:[],
    groupList:[],
    assignList:[],  //指派
  },
  effects:{
    *fetchList({ payload }, { call, put }){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(userManageApi.queryUserList, payload);
      if(jsonData.result){
        yield put({
          type: 'saveList',
          payload:jsonData.data
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *fetchRoleList(_,{call,put}){
      const jsonData = yield call(userManageApi.queryRoleList);
      if(jsonData.result){
        yield put({
          type: 'saveRoleList',
          payload:jsonData.data || []
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
    },
    *fetchAdd({ payload },{ call, put }){
      const jsonData= yield call(userManageApi.addUser,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      const jsonData= yield call(userManageApi.removeUser,payload);
      return jsonData;
    },
    *fetchEdit({payload},{call,put}){
      const jsonData= yield call(userManageApi.editUser,payload);
      return jsonData;
    },
    *fetchAllList({payload},{call,put}){
      const jsonData = yield call(userManageApi.getAllUserList, payload);
      if(jsonData.result){
        yield put({
          type: 'saveAllList',
          payload:jsonData.data
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
    },
    *fetchListByGroup({payload},{call,put}){
      const jsonData= yield call(userManageApi.getUserByGroup,payload);
      if(jsonData.result){
        console.log('hehheh')
        yield put({
          type: 'saveGroupList',
          payload:jsonData.data
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
    },
    *fetchAssignList(_,{call,put}){
      const jsonData=yield call(userManageApi.getAssignList);
      yield put({
        type:'saveAssignList',
        payload:jsonData.data || []
      })
    }
  },
  reducers:{
    changeLoading(state,{payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state,{payload}){
      return {
        ...state,
        userList:payload.list || [],
        userTotal:payload.total	||0
      }
    },
    saveRoleList(state,{payload}){
      return {
        ...state,
        roleList:payload
      }
    },
    saveAllList(state,{payload}){
      return {
        ...state,
        allList:payload || []
      }
    },
    saveGroupList(state,{payload}){
      return {
        ...state,
        groupList:payload || []
      }
    },
    saveAssignList(state,{payload}){
      return {
        ...state,
        assignList:payload || []
      }
    }


  },


}
