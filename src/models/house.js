/**
 * Created by chenxq on 2017/12/25.
 */
//企业
import {HouseApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace:'house',
  state:{
    list:[],
    loading:false,
  },
  effects:{
    *fetchList({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(HouseApi.queryHouseList,payload);
      console.log('jsonDAta',jsonData);
      if(jsonData.result){
        yield put({
          type: 'saveList',
          payload:jsonData.data || []
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });

    },
    *fetchAdd({payload},{call,put}){
      let jsonData=yield call(HouseApi.addHouse,payload);
      return jsonData;
    },
    *fetchEdit({payload},{call,put}){
      let jsonData=yield call(HouseApi.editHouse,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      let jsonData=yield call(HouseApi.removeHouse,payload);
      return jsonData;
    },
  },
  reducers:{
    changeLoading(state,{payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state,{payload}){
      console.log('sabe',payload)
      return {
        ...state,
        list:payload || [],
      }
    },

  }

}
