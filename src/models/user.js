import { query as queryUsers, queryCurrent ,queryAuth} from '../services/user';

export default {
  namespace: 'user',

  state: {
    list: [],
    loading: false,
    currentUser: {},
    authList:[]
  },
  effects: {
    *fetch(_, { call, put }) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(queryUsers);
      yield put({
        type: 'save',
        payload: response,
      });
      yield put({
        type: 'changeLoading',
        payload: false,
      });
    },
    *fetchCurrent(_, { call, put }) {
      const response = yield call(queryCurrent);
      yield put({
        type: 'saveCurrentUser',
        payload: response.data || {},
      });
      yield put({
        type:'fetchAuth',
        payload:''
      })


    },
    *fetchAuth(_,{call,put}){  // 权限
      const response=yield call(queryAuth);
      yield put({
        type:'saveAuthList',
        payload: response.data || []
      })

    },
    *setCurrent(_, { put, select }) {
      const { userInfo } = yield select(state => state.login);
      yield put({
        type: 'saveCurrentUser',
        payload: userInfo || {},
      });
    },
    *fetchList({},{call,put}){

    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    changeLoading(state, action) {
      return {
        ...state,
        loading: action.payload,
      };
    },
    saveCurrentUser(state, { payload }) {
      return {
        ...state,
        currentUser: payload,
      };
    },
    saveAuthList(state,{payload}){
      return {
        ...state,
        authList:payload
      }

    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload,
        },
      };
    },
  },
};
