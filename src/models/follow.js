/**
 * Created by zhouli on 2017/12/25.
 */
//职业
import {FollowApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace:'follow',
  state:{
    list:[],
    loading:false,
  },
  effects:{
    *fetchList({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      console.log('ffdfffff')
      const jsonData = yield call(FollowApi.queryFollowList,payload);
      console.log('jsonDAta',jsonData);
      if(jsonData.result){
        yield put({
          type: 'saveList',
          payload:jsonData.data || []
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });

    },
    *fetchAdd({payload},{call,put}){
      let jsonData=yield call(FollowApi.addFollow,payload);
      return jsonData;
    },
    *fetchEdit({payload},{call,put}){
      let jsonData=yield call(FollowApi.editFollow,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      let jsonData=yield call(FollowApi.removeFollow,payload);
      return jsonData;
    },
  },
  reducers:{
    changeLoading(state,{payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state,{payload}){
      console.log('sabe',payload)
      return {
        ...state,
        list:payload || [],
      }
    },

  }

}
