/**
 * Created by chenxq on 2017/12/11.
 */
import {productApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace: 'product',
  state: {
    loading: false,
    productList:[],
    allList:[],
    total:0
  },
  effects: {
    *fetchList({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(productApi.queryProductList,payload);
      if(jsonData.result){
        yield put({
          type: 'saveList',
          payload:jsonData.data || []
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });

    },
    *fetchAdd({payload},{call,put}){
      let jsonData=yield call(productApi.addProduct,payload);
      return jsonData;
    },
    *fetchEdit({payload},{call,put}){
      let jsonData=yield call(productApi.editProduct,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      let jsonData=yield call(productApi.removeProduct,payload);
      return jsonData;
    },
    *fetchAllList({payload},{call,put}){
      let jsonData=yield call(productApi.getAllProductList,payload);
      if(jsonData.result){
        yield put({
          type:'saveAllList',
          payload: jsonData.data || []
        })
      }
    }
  },
  reducers: {
    changeLoading(state,{payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state,{payload}){
      return {
        ...state,
        productList:payload.list || [],
        total:payload.total || 0
      }
    },
    saveAllList(state,{payload}){
      return {
        ...state,
        allList: payload
      }
    }
  },
}
