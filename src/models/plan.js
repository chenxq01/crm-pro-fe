/**
 * Created by zhouli on 2017/12/25.
 */
//职业
import {PlanApi} from "../services/api";
import {notification} from "antd";
export default {
  namespace: 'plan',
  state: {
    list: [],
    loading: false,
  },
  effects: {
    *fetchList({payload}, {call, put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(PlanApi.queryPlanList, payload);
      console.log('jsonDAta', jsonData);
      if (jsonData.result) {
        yield put({
          type: 'saveList',
          payload: jsonData.data || []
        })
      } else {
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });

    },
    *fetchAdd({payload}, {call, put}){
      let jsonData = yield call(PlanApi.addPlan, payload);
      return jsonData;
    },
    *fetchEdit({payload}, {call, put}){
      let jsonData = yield call(PlanApi.editPlan, payload);
      return jsonData;
    },
    // *fetchRemove({payload}, {call, put}){
    //   let jsonData = yield call(PlanApi.removePlan, payload);
    //   return jsonData;
    // },
    *fetchRemove({payload}, {call, put}){
      let jsonData = yield call(PlanApi.executePlan, payload);
      return jsonData;
    },
  },
  reducers: {
    changeLoading(state, {payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state, {payload}){
      console.log('sabe', payload)
      return {
        ...state,
        list: payload || [],
      }
    },

  }

}
