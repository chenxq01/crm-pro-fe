/**
 * Created by chenxq on 2017/12/13.
 */
import {loanChannelApi} from '../services/api';
import { notification } from 'antd';
export default {
  namespace:'loan_channel',
  state:{
    list:[],
    loading:false,
    total:0,
    loanList:[]
  },
  effects:{
    *fetchList({payload},{call,put}){
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const jsonData = yield call(loanChannelApi.queryLoanChannelList,payload);
      if(jsonData.result){
        yield put({
          type: 'saveList',
          payload:jsonData.data || []
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
      yield put({
        type: 'changeLoading',
        payload: false,
      });

    },
    *fetchAdd({payload},{call,put}){
      let jsonData=yield call(loanChannelApi.addLoanChannel,payload);
      return jsonData;
    },
    *fetchEdit({payload},{call,put}){
      let jsonData=yield call(loanChannelApi.editLoanChannel,payload);
      return jsonData;
    },
    *fetchRemove({payload},{call,put}){
      let jsonData=yield call(productApi.removeLoanChannel,payload);
      return jsonData;
    },
    *fetchLoanList({payload},{call,put}){
      const jsonData = yield call(loanChannelApi.getLoanList,payload);
      if(jsonData.result){
        yield put({
          type: 'saveLoanList',
          payload:jsonData.data || []
        })
      }else{
        notification.error({
          message: `请求错误`,
          description: jsonData.msg,
        });
      }
    }
  },
  reducers:{
    changeLoading(state,{payload}){
      return {
        ...state,
        loading: payload,
      };
    },
    saveList(state,{payload}){
      return {
        ...state,
        list:payload.list || [],
        total:payload.total || 0
      }
    },
    saveLoanList(state,{payload}){
      return{
        ...state,
        loanList:payload || []
      }
    }
  }

}
