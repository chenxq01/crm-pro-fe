import { stringify } from 'qs';
import request from '../utils/request';
import { notification } from 'antd';
export async function fakeAccountLogin(params) {
  return request('/v1/user/login', {
    method: 'POST',
    body: params,
  });
}

export async function AccountLogout(){
 return request('/v1/user/logout',{
   method:'POST'
 })
}


/**** customer api *******/
//客户管理
let CustomerManage = {
  applyCustomer:{
    url: '/v1/customer/apply',
    method:"post",
  },
  add: {
    url:  "/v1/customer/add",
    method: "post"
  },
  list: {
    url:  "/v1/customer/list",
    method: "post"
  },
  edit: (customerId)=>({
    url:  `/v1/customer/${customerId}/edit`,
    method: "post"
  }),
  detail: (customerId)=>({
    url:  `/v1/customer/${customerId}`,
    method: "get"
  }),
  allocate: (userId)=>({
    url:  `/v1/customer/allocate/${userId}`,
    method: "post"
  }),
  claim: {
    url:  "/v1/customer/claim",
    method: "post"
  },
  remove: {
    url:  "/v1/customer/remove",
    method: "post"
  },
  addContacts: (customerId)=>({
    url:  `/v1/customer/contacts/${customerId}/add`,
    method: "post"
  }),
  editContacts: (customerId)=>({
    url:  `/v1/customer/contacts/${customerId}/edit`,
    method: "post"
  }),
  getContactsList: (customerId)=>({
    url:  `/v1/customer/contacts/${customerId}/list`,
    method: "get"
  }),
  addJob: (customerId)=>({
    url:  `/v1/customer/jobs/${customerId}/add`,
    method: "post"
  }),
  getJobList: (customerId)=>({
    url:  `/v1/customer/jobs/${customerId}/list`,
    method: "get"
  }),
  editJob: (customerId)=>({
    url:  `/v1/customer/jobs/${customerId}/edit`,
    method: "post"
  }),
  removeJob: (customerId)=>({
    url:  `/v1/customer/jobs/${customerId}/remove`,
    method: "post"
  }),
  removeContacts: (customerId)=>({
    url:  `/v1/customer/contacts/${customerId}/remove`,
    method: "post"
  }),
  customerImportHigh: {
    url:  "/v1/customer/excel/high/import",
    method: "post"
  },
  customerImportOrdinary: {
    url:  "/v1/customer/excel/ordinary/import",
    method: "post"
  },
  customerModelExport: {
    url:  "/v1/customer/excel/download",
    method: "get"
  },
  quit: {
    url:  "/v1/customer/quit",
    method: "post"
  },
  invalid: {
    url:  "/v1/customer/invalid",
    method: "post"
  },
  housesAdd: (customerId)=>({
    url:  `/v1/customer/houses/${customerId}/add`,
    method: "post"
  }),
  housesEdit: (customerId)=>({
    url:  `/v1/customer/houses/${customerId}/edit`,
    method: "post"
  }),
  housesList: (customerId)=>({
    url:  `/v1/customer/houses/${customerId}/list`,
    method: "get"
  }),
  housesRemove: (customerId)=>({
    url:  `/v1/customer/houses/${customerId}/remove`,
    method: "post"
  }),
  //标记（取消标记）为意向用户
  intend: (customerId)=>({
    url:  `/v1/customer/intend/${customerId}`,
    method: "post",
  }),


  carAdd: (customerId)=>({
    url:  `/v1/customer/car/${customerId}/add`,
    method: "post"
  }),

  enterpriseAdd: (customerId)=>({
    url:  `/v1/customer/enterprise/${customerId}/add`,
    method: "post"
  }),

  cardAdd: (customerId)=>({
    url:  `/v1/customer/creditCard/${customerId}/add`,
    method: "post"
  }),

  creditAdd: (customerId)=>({
    url:  `/v1/customer/credit/${customerId}/add`,
    method: "post"
  }),

  billAdd: (customerId)=>({
    url:  `/v1/customer/bill/${customerId}/add`,
    method: "post"
  }),

  carEdit: (customerId)=>({
    url:  `/v1/customer/car/${customerId}/edit`,
    method: "post"
  }),

  enterpriseEdit: (customerId)=>({
    url:  `/v1/customer/enterprise/${customerId}/edit`,
    method: "post"
  }),

  cardEdit: (customerId)=>({
    url:  `/v1/customer/creditCard/${customerId}/edit`,
    method: "post"
  }),

  creditEdit: (customerId)=>({
    url:  `/v1/customer/credit/${customerId}/edit`,
    method: "post"
  }),

  billEdit: (customerId)=>({
    url:  `/v1/customer/bill/${customerId}/edit`,
    method: "post"
  }),

  carList: (customerId)=>({
    url:  `/v1/customer/car/${customerId}/list`,
    method: "get"
  }),

  enterpriseList: (customerId)=>({
    url:  `/v1/customer/enterprise/${customerId}/list`,
    method: "get"
  }),

  cardDetail: (customerId)=>({
    url:  `/v1/customer/creditCard/${customerId}/detail`,
    method: "get"
  }),

  creditDetail: (customerId)=>({
    url:  `/v1/customer/credit/${customerId}/detail`,
    method: "get"
  }),

  billList: (customerId)=>({
    url:  `/v1/customer/bill/${customerId}/list`,
    method: "get"
  }),

  carRemove: (customerId)=>({
    url:  `/v1/customer/car/${customerId}/remove`,
    method: "post"
  }),

  enterpriseRemove: (customerId)=>({
    url:  `/v1/customer/enterprise/${customerId}/remove`,
    method: "post"
  }),

  cardRemove: (customerId)=>({
    url:  `/v1/customer/creditCard/${customerId}/remove`,
    method: "post"
  }),

  billRemove: (customerId)=>({
    url:  `/v1/customer/bill/${customerId}/remove`,
    method: "post"
  }),

  makeACall: (customerId)=>({
    url:  `/v1/call/make/${customerId}`,
    method:"post"
  }),

  statisticsChart:{
    url: '/v1/call/statistics/chart',
    method:"post"
  },
  statisticsCount:{
    url: '/v1/call/statistics/count',
    method:"post"
  },
  statisticsCountList:{
    url: '/v1/call/statistics/count/list',
    method:"post",
  },
  customerStatisticsCount:{
    url: '/v1/customer/statistics/count',
    method:"post",
  },
  customerStatisticsChart:{
    url: '/v1/customer/statistics/chart',
    method:"post",
  },
  customerStatisticsList:{
    url: '/v1/customer/statistics/list',
    method:"post"
  }

};
async function queryCustomerList(params){
  params.page=params.current;
  return request(`/v1/customer/list`,{
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  })
}

async function addCustomer(params){
  return request(`/v1/customer/add`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function applyCustomer(params){
  return request(`/v1/customer/apply`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function removeCustomer(ids){
  return request('/v1/customer/remove',{
    method:'POST',
    body:ids
  })
}
async function shift(params) {
  return request('/v1/customer/shift',{
    method:'POST',
    body:{
      ...params
    }
  })
}
async function editCustomerInfo(params){
  return request(`/v1/customer/${params.id}/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function allocate(params){
  return request(`/v1/customer/allocate`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function getCustomerDetail(customerId) {
  return request(`/v1/customer/${customerId}`);
}

async function getCustomerIds(params) {
  return request(`/v1/customer/listIds`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

export const customerApi={
  queryCustomerList,
  addCustomer,
  applyCustomer,
  removeCustomer,
  editCustomerInfo,
  allocate,
  getCustomerDetail,
  shift,
  getCustomerIds
}

//用户管理
async function queryUserList(params){
  params.page=params.current;
  return request('/v1/user/list',{
    method:'POST',
    body:{
      ...params,
      method: 'post',
    }
  })
}
async function queryRoleList(){
  return request('/v1/role/list');
}
async function addUser(params){
  return request('/v1/user/add',{
    method:'POST',
    body:{
      ...params,
      method: 'post',
    }
  })
}

async function removeUser(ids){
  return request('/v1/user/remove',{
    method:'POST',
    body:ids
  })
}

async function editUser(params){
  return request(`/v1/user/${params.id}/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function getAllUserList(){
  return request('/v1/user/listAll',{
    method:"POST",
    body:{}
  })
}

async function getUserByGroup(){
  return request(`/v1/user/listByGroup`)
}

async function getAssignList() {
  return request(`/v1/user/dropdown`)
}
export let userManageApi={
  queryUserList,
  queryRoleList,
  addUser,
  removeUser,
  editUser,
  getAllUserList,
  getUserByGroup,
  getAssignList
}

//用户组管理
async function queryGroupList(){
  return request('/v1/group/list');
}

//获取同组用户
async function getSameGroupUser(){
  return request(`/v1/user/group/list`)
}


async function addGroup(params){
  return request('/v1/group/add',{
    method:"POST",
    body:{
      ...params
    }
  })
}
async function editGroup(params){
  ///v1/group/{groupId}/edit
  return request(`/v1/group/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}
async function removeGroup(id){
  return request(`/v1/group/${id}/remove`,{
    method:'POST',
    body:{
      id
    }
  })
}

export let groupManageApi={
  queryGroupList,
  addGroup,
  editGroup,
  removeGroup,
  getSameGroupUser
}

//产品相关的
async function queryProductList(params){
  params.page=params.current;
  return request(`/v1/product/page`,{
    method:'POST',
    body:{
      ...params,
      method:'post'
    }
  })
}
async function addProduct(params){
  return request(`/v1/product/add`,{
    method:'POST',
    body:{
      ...params,
      method:'post'
    }
  })
}
async function editProduct(params){
  return request(`/v1/product/update`,{
    method:'POST',
    body:{
      ...params,
      method:'post'
    }
  })
}

async function removeProduct(params){
  return request(`/v1/product/remove/${params.id}`,{
    method:'POST',
    body:{}
  })
}

async function getAllProductList(state) {
  return request(`/v1/product/list/${state}`)
}

export let productApi={
  queryProductList,
  addProduct,
  editProduct,
  removeProduct,
  getAllProductList
}

// 客户渠道
async function queryCustomerChannelList(params){
  params.page=params.current;
  return request(`/v1/channel/customer/page`,{
    method:'POST',
    body:{
      ...params,
      method:'post'
    }
  })
}
async function addCustomerChannel(params){
  return request(`/v1/channel/customer/add`,{
    method:'POST',
    body:{
      ...params,
      method:'post'
    }
  })
}
async function editCustomerChannel(params){
  return request(`/v1/channel/customer/update`,{
    method:'POST',
    body:{
      ...params,
      method:'post'
    }
  })
}

async function removeCustomerChannel(params){
  return request(`/v1/channel/customer/remove/${params.id}`,{
    method:'POST',
    body:{}
  })
}
export let customerChannelApi={
  queryCustomerChannelList,
  addCustomerChannel,
  editCustomerChannel,
  removeCustomerChannel,

}

//放款渠道
async function queryLoanChannelList(params){
  params.page=params.current;
  return request(`/v1/channel/loan/page`,{
    method:'POST',
    body:{
      ...params,
      method:'post'
    }
  })
}
async function addLoanChannel(params){
  return request(`/v1/channel/loan/add`,{
    method:'POST',
    body:{
      ...params,
      method:'post'
    }
  })
}
async function editLoanChannel(params){
  return request(`/v1/channel/loan/update`,{
    method:'POST',
    body:{
      ...params,
      method:'post'
    }
  })
}

async function removeLoanChannel(params){
  return request(`/v1/channel/loan/remove/${params.id}`,{
    method:'POST',
    body:{}
  })
}

async function getLoanList(){
  return request('/v1/channel/loan/list')
}

export let loanChannelApi={
  queryLoanChannelList,
  addLoanChannel,
  editLoanChannel,
  removeLoanChannel,
  getLoanList
}

async function querySignList(params){
  params.page=params.current;
  //console.log('paras',params);
 return request(`/v1/loan/page`,{
   method:'POST',
   body:{
     ...params
   }
 })
}

async function addSign(params){
  return request(`/v1/loan/create`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function editSign(params){
  return request(`/v1/loan/update`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function removeSign(id){
  return request(`/v1/loan/delete/${id}`,{
    method:'POST',
    body:{}
  })
}

async function querySignListById(id){
  return request(`/v1/loan/list?customerId=${id}`)
}

async function getSignDetail(id){
  return request(`/v1/loan/${id}`)
}
async function signAssign(params) {
  return request(`/v1/loan/assign`,{
    method:'POST',
    body:{
      ...params
    }
  })
}
export let signApi={
  querySignList,
  addSign,
  editSign,
  removeSign,
  querySignListById,
  getSignDetail,
  signAssign

}

async function queryEnterpriseList(customerId){
  return request(`/v1/customer/enterprise/${customerId}/list`)
}

async function addEnterprise({customerId,params={}}) {
  return request(`/v1/customer/enterprise/${customerId}/add`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function editEnterprise({customerId,params={}}) {
  return request(`/v1/customer/enterprise/${customerId}/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}
async function removeEnterprise({customerId,ids=[]}){
  return request(`/v1/customer/enterprise/${customerId}/remove`,{
    method:'POST',
    body:ids
  })
}
export let EnterpriseApi={
  queryEnterpriseList,
  addEnterprise,
  editEnterprise,
  removeEnterprise
}


//job
async function queryJobList(customerId){
  return request(`/v1/customer/jobs/${customerId}/list`)
}

async function addJob({customerId,params={}}) {
  return request(`/v1/customer/jobs/${customerId}/add`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function editJob({customerId,params={}}) {
  return request(`/v1/customer/jobs/${customerId}/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}
async function removeJob({customerId,ids=[]}){
  return request(`/v1/customer/jobs/${customerId}/remove`,{
    method:'POST',
    body:ids
  })
}
export let JobApi={
  queryJobList,
  addJob,
  editJob,
  removeJob
};

//houses
async function queryHouseList(customerId){
  return request(`/v1/customer/houses/${customerId}/list`)
}

async function addHouse({customerId,params={}}) {
  return request(`/v1/customer/houses/${customerId}/add`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function editHouse({customerId,params={}}) {
  return request(`/v1/customer/houses/${customerId}/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}
async function removeHouse({customerId,ids=[]}){
  return request(`/v1/customer/houses/${customerId}/remove`,{
    method:'POST',
    body:ids
  })
}
export let HouseApi={
  queryHouseList,
  addHouse,
  editHouse,
  removeHouse
};
//cars
async function queryCarList(customerId){
  return request(`/v1/customer/car/${customerId}/list`)
}

async function addCar({customerId,params={}}) {
  return request(`/v1/customer/car/${customerId}/add`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function editCar({customerId,params={}}) {
  return request(`/v1/customer/car/${customerId}/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}
async function removeCar({customerId,ids=[]}){
  return request(`/v1/customer/car/${customerId}/remove`,{
    method:'POST',
    body:ids
  })
}
export let CarApi={
  queryCarList,
  addCar,
  editCar,
  removeCar
};
//creditCard
async function queryCreditCardList(customerId){
  return request(`/v1/customer/creditCard/${customerId}/list`)
}

async function addCreditCard({customerId,params={}}) {
  return request(`/v1/customer/creditCard/${customerId}/add`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function editCreditCard({customerId,params={}}) {
  return request(`/v1/customer/creditCard/${customerId}/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}
async function removeCreditCard({customerId,ids=[]}){
  return request(`/v1/customer/creditCard/${customerId}/remove`,{
    method:'POST',
    body:ids
  })
}
export let CreditCardApi={
  queryCreditCardList,
  addCreditCard,
  editCreditCard,
  removeCreditCard
};
//follow
async function queryFollowList(customerId){
  return request(`/v1/customer/follow/${customerId}/list`);
}

async function addFollow({customerId,params={}}) {
  return request(`/v1/customer/follow/add`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function editFollow({customerId,params={}}) {
  return request(`/v1/customer/follow/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}
async function removeFollow({customerId,ids=[]}){
  return request(`/v1/customer/follow/remove`,{
    method:'POST',
    body:ids
  })
}
export let FollowApi={
  queryFollowList,
  addFollow,
  editFollow,
  removeFollow
};

//plan
async function queryPlanList(customerId){
  return request(`/v1/customer/plan/${customerId}/list`)
}
async function queryPlanTODO(customerId){
  return request(`/v1/customer/plan/todo`)
}
async function addPlan({customerId,params={}}) {
  return request(`/v1/customer/plan/add`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function editPlan({customerId,params={}}) {
  return request(`/v1/customer/plan/edit`,{
    method:'POST',
    body:{
      ...params
    }
  })
}
async function removePlan({customerId,ids=[]}){
  return request(`/v1/customer/plan/remove`,{
    method:'POST',
    body:ids
  })
}
async function executePlan({planId}){
  return request(`/v1/plan/loan/${planId}/execute`,{
    method:'POST',
  })
}
export let PlanApi={
  queryPlanList,
  queryPlanTODO,
  addPlan,
  editPlan,
  removePlan,
  executePlan
};

async function queryAccountList(params){
  params.page=params.current;
  return request(`/v1/loan/ic/page`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function addAccount(params){
  return request(`/v1/loan/ic/create`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function editAccount(params){
  return request(`/v1/loan/ic/update`,{
    method:'POST',
    body:{
      ...params
    }
  })
}

async function removeAccount(id){
  return request(`/v1/loan/ic/delete/${id}`,{
    method:"POST"
  })
}

async function queryAccountListByLoanId(loanId) {
  return request(`/v1/loan/ic/list?loanId=${loanId}`);
}

export let AccountApi={
  queryAccountList,
  addAccount,
  editAccount,
  removeAccount,
  queryAccountListByLoanId
}

async function getLoanchart({start,end,...others}){
  return request(`/v1/loan/stat/count?start=${start}&end=${end}`)
}
export const ChartApi={
  getLoanchart
}



async function makeCall(customerId){
  return request(`/v1/call/make/${customerId}`,{
    method:'POST'
  })
}

async function hangupCall({customerId,callId}){
  return request(`/v1/call/hangup/${customerId}/${callId}`,{
    method:"POST"
  })
}
async function queryCallStatus({customerId,callId}) {
  return request(`/v1/call/listen/${customerId}?callId=${callId}`);
}

export const CallApi={
  makeCall,
  hangupCall,
  queryCallStatus
}
