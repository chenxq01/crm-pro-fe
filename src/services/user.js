import request from '../utils/request';

export async function query() {
  return request('/api/users');
}

export async function queryCurrent() {
  return request('/v1/user/currentUser');
}


export async function queryAuth() {
  return request('/v1/user/authority/list');
}
