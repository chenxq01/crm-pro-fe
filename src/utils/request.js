import fetch from 'dva/fetch';
import { notification } from 'antd';
import PREFIX from './prefix-hot';
import { routerRedux } from 'dva/router';

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  notification.error({
    message: `请求错误 ${response.status}: ${response.url}`,
    description: response.statusText,
  });
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */

// 获取当前的token值（主要用于登录）
export const getToken = () => {
  return localStorage.token;
};

// 处理url
const handleUrl = (url) => {
  if (url.indexOf('//') >= 0) {
    return url;
  } else {
    return `${PREFIX}${url}`;
  }
};

export default function request(url, options) {
  const defaultOptions = {
    // credentials: 'include',
  };
  const newOptions = { ...defaultOptions, ...options };
  const token = getToken();
  newOptions.headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'auth-token': token,
    ...newOptions.headers,
  };
  if (newOptions.method === 'POST' || newOptions.method === 'PUT') {
    newOptions.body = JSON.stringify(newOptions.body);
  }
  url = handleUrl(url);
  return fetch(url, newOptions)
    .then(checkStatus)
    .then(response => response.json())
    .catch((error) => {
      if(error.response && error.response.status === 401){
        //routerRedux.push('/user/login');
        window.location.href='/#/index';
        return {};
      }
      if (error.code) {
        notification.error({
          message: error.name,
          description: error.message,
        });
      }
      if ('stack' in error && 'message' in error) {
        notification.error({
          message: `请求错误: ${url}`,
          description: error.message,
        });
      }
      return error;
    });
}
