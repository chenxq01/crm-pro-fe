/**
 * Created by CoolGuy on 2017/7/2.
 */
const ENV = process.env.NODE_ENV === 'development'; // npm start会将当前环境设置为development
// 所以开发环境下用我们线上服务器的；产品环境下用内网的
const PREFIX = ENV ? '//121.40.214.161:9090' : '//192.168.1.100:9090';
export default PREFIX;
