import dynamic from 'dva/dynamic';
import {Public,Private,Follow,Intention,Maintain,Lose,Success,Invalid} from '../routes/Customer/Customer';
import {SignAccept,SignBack,SignDone,SignSettle,SignSuspend,SignWait} from '../routes/Sign';
// wrapper of dynamic
const dynamicWrapper = (app, models, component) =>
  dynamic({
    app,
    models: () => models.map(m => import(`../models/${m}.js`)),
    component,
  });

// nav data
export const getNavData = app => [
  {
    component: dynamicWrapper(app, ['user', 'login'], () =>
      import('../layouts/BasicLayout')
    ),
    layout: 'BasicLayout',
    name: '首页', // for breadcrumb
    path: '/',
    children: [
      {
        name: '工作台',
        icon: 'dashboard',
        path: 'work',
        children:[{
          name:'坐席工作台',
          path:'customer',
          code:'/work/customer',
          component: dynamicWrapper(app, ['user','plan'], () =>
            import('../routes/Work/Customer')
          ),
        },{
          name:'管理工作台',
          path:'management',
          code:'/work/management',
          component:dynamicWrapper(app,['user'],()=>
            import('../routes/Work/Manage')
          )
        }]

      },
      {
        name: '统计管理',
        icon: 'area-chart',
        path: 'statistics',
        children: [
          /*{
            name: '客户统计',
            path: 'seat',
            component: null,
          },*/
          {
            name: '坐席统计',
            path: 'customer',
            code:'/statistics/customer',
            component: dynamicWrapper(app,['group','usermanagement'],()=>(
              import('../routes/Statistics/Customer')
            )),
          },
          {
            name: '通话统计',
            path: 'conversation',
            code:'/statistics/conversation',
            component:dynamicWrapper(app,['group','usermanagement'],()=>(
              import('../routes/Statistics/Call')
            )),
          },
          {
            name: '签单统计',
            path: 'sign',
            code:'/statistics/sign',
            component: dynamicWrapper(app, ['chart','group','usermanagement'],()=>(
              import('../routes/Statistics/Sign')
            )),
          },
        ],
      },
      {
        name: '客户管理',
        icon: 'customer-service',
        path: 'customer',
        children: [
          {
            name: '公共资源池',
            path: 'public',
            code:'/customer/public',
            component: dynamicWrapper(app, ['customer','usermanagement'],()=>Public),
          },
          {
            name: '私有资源池',
            path: 'private',
            code:'/customer/private',
            component: dynamicWrapper(app, ['customer','usermanagement'],()=>Private),
          },
          {
            name: '营销客户',
            path: 'marketing',
            code:'/customer/marketing',
            component: dynamicWrapper(app, ['customer','usermanagement'],()=>Follow),
          },
          {
            name: '意向客户',
            path: 'intention',
            code:'/customer/intention',
            component: dynamicWrapper(app, ['customer','usermanagement'],()=>Intention),
          },
          {
            name: '签约客户',
            path: 'sign',
            code:'/customer/sign',
            component: dynamicWrapper(app, ['customer','usermanagement'],()=>Maintain),
          },
          {
            name: '赢单客户',
            path: 'win',
            code:'/customer/win',
            component: dynamicWrapper(app, ['customer','usermanagement'],()=>Success),
          },
          {
            name: '输单客户',
            path: 'lose',
            code:'/customer/lose',
            component: dynamicWrapper(app, ['customer','usermanagement'],()=>Lose),
          },
          {
            name: '无效客户',
            path: 'invalid',
            code:'/customer/invalid',
            component: dynamicWrapper(app, ['customer','usermanagement'],()=>Invalid),
          },
          {
            name:'客户详情',
            path: 'detail/:id',  //客户详情
            hidden:true, // 不展示在左侧菜单栏上
            code:'',
            component: dynamicWrapper(app, ['customer','usermanagement','sign','enterprise','product','job','car','house','account','follow','plan','call'],()=>(
              import('../routes/Customer/Detail')
            ))
          }
        ],
      },
      {
        name: '签单管理',
        icon: 'edit',
        path: 'sign',
        children: [
          {
            name: '待签单',
            path: 'wait',
            code:'/sign/wait',
            component: dynamicWrapper(app, ['sign','usermanagement','product','account'],()=>SignWait),
          },
          {
            name: '做单中',
            path: 'accept',
            code:'/sign/accept',
            component: dynamicWrapper(app, ['sign','usermanagement','product','account'],()=>SignAccept),
          },
          {
            name: '结算中',
            path: 'settle',
            code:'/sign/Settle',
            component: dynamicWrapper(app, ['sign','usermanagement','product','account'],()=>SignSettle),
          },
          {
            name: '挂起',
            path: 'suspend',
            code:'/sign/Suspend',
            component: dynamicWrapper(app, ['sign','usermanagement','product','account'],()=>SignSuspend),
          },
          {
            name: '退单',
            path: 'back',
            code:'/sign/back',
            component: dynamicWrapper(app, ['sign','usermanagement','product','account'],()=>SignBack),
          },
          {
            name: '完成',
            path: 'done',
            code:'/sign/done',
            component: dynamicWrapper(app, ['sign','usermanagement','product','account'],()=>SignDone),
          },
          {
            name:'签单详情',
            path:'detail/:id',
            hidden:true,
            code:'',
            component:dynamicWrapper(app,['sign','usermanagement','product','account'],()=>
              import('../routes/Sign/SignDetail')
            )
          }
        ],
      },
      {
        name: '配置管理',
        icon: 'book',
        path: 'config',
        children: [
          {
            name: '产品列表',
            path: 'list',
            code:'/config/list',
            component: dynamicWrapper(app,['product'],()=>
              import('../routes/Config/ProductList')
            ),
          },
          {
            name: '客户渠道列表',
            path: 'customer',
            code:'/config/customer',
            component: dynamicWrapper(app,['customer_channel','usermanagement'],()=>
              import('../routes/Config/CustomerChannelList')
            ),
          },
          {
            name:'放款渠道列表',
            path:'loan',
            code:'/config/loan',
            component: dynamicWrapper(app,['loan_channel'],()=>
              import('../routes/Config/LoanChannelList')
            ),
          },
        ],
      },
      {
        name: '用户管理',
        icon: 'user',
        path: 'usermanagement',
        children: [
          {
            name: '用户管理',
            path: 'index',
            code:'/usermanagement/index',
            component: dynamicWrapper(app, ['usermanagement','group'], () =>
              import('../routes/UserManagement/index')
            ),
          },
          {
            name: '分组管理',
            path: 'team',
            code:'/usermanagement/team',
            component: dynamicWrapper(app, ['group'], () =>
              import('../routes/UserManagement/Group')
            ),
          },
        ],
      },
      /*{
        name: 'Dashboard',
        icon: 'dashboard',
        path: 'dashboard',
        children: [
          {
            name: '分析页',
            path: 'analysis',
            component: dynamicWrapper(app, ['chart'], () =>
              import('../routes/Dashboard/Analysis')
            ),
          },
          {
            name: '监控页',
            path: 'monitor',
            component: dynamicWrapper(app, ['monitor'], () =>
              import('../routes/Dashboard/Monitor')
            ),
          },
          {
            name: '工作台',
            path: 'workplace',
            component: dynamicWrapper(
              app,
              ['project', 'activities', 'chart'],
              () => import('../routes/Dashboard/Workplace')
            ),
          },
        ],
      },
      {
        name: '表单页',
        path: 'form',
        icon: 'form',
        children: [
          {
            name: '基础表单',
            path: 'basic-form',
            component: dynamicWrapper(app, ['form'], () =>
              import('../routes/Forms/BasicForm')
            ),
          },
          {
            name: '分步表单',
            path: 'step-form',
            component: dynamicWrapper(app, ['form'], () =>
              import('../routes/Forms/StepForm')
            ),
            children: [
              {
                path: 'confirm',
                component: dynamicWrapper(app, ['form'], () =>
                  import('../routes/Forms/StepForm/Step2')
                ),
              },
              {
                path: 'result',
                component: dynamicWrapper(app, ['form'], () =>
                  import('../routes/Forms/StepForm/Step3')
                ),
              },
            ],
          },
          {
            name: '高级表单',
            path: 'advanced-form',
            component: dynamicWrapper(app, ['form'], () =>
              import('../routes/Forms/AdvancedForm')
            ),
          },
        ],
      },
      {
        name: '列表页',
        path: 'list',
        icon: 'table',
        children: [
          {
            name: '查询表格',
            path: 'table-list',
            component: dynamicWrapper(app, ['rule'], () =>
              import('../routes/List/TableList')
            ),
          },
          {
            name: '标准列表',
            path: 'basic-list',
            component: dynamicWrapper(app, ['list'], () =>
              import('../routes/List/BasicList')
            ),
          },
          {
            name: '卡片列表',
            path: 'card-list',
            component: dynamicWrapper(app, ['list'], () =>
              import('../routes/List/CardList')
            ),
          },
          {
            name: '搜索列表（项目）',
            path: 'cover-card-list',
            component: dynamicWrapper(app, ['list'], () =>
              import('../routes/List/CoverCardList')
            ),
          },
          {
            name: '搜索列表（应用）',
            path: 'filter-card-list',
            component: dynamicWrapper(app, ['list'], () =>
              import('../routes/List/FilterCardList')
            ),
          },
          {
            name: '搜索列表（文章）',
            path: 'search',
            component: dynamicWrapper(app, ['list'], () =>
              import('../routes/List/SearchList')
            ),
          },
        ],
      },
      {
        name: '详情页',
        path: 'profile',
        icon: 'profile',
        children: [
          {
            name: '基础详情页',
            path: 'basic',
            component: dynamicWrapper(app, ['profile'], () =>
              import('../routes/Profile/BasicProfile')
            ),
          },
          {
            name: '高级详情页',
            path: 'advanced',
            component: dynamicWrapper(app, ['profile'], () =>
              import('../routes/Profile/AdvancedProfile')
            ),
          },
        ],
      },*/
      {
        name: '结果',
        path: 'result',
        icon: 'check-circle-o',
        hidden:true, // 不展示在左侧菜单栏上
        children: [
          {
            name: '成功',
            path: 'success',
            component: dynamicWrapper(app, [], () =>
              import('../routes/Result/Success')
            ),
          },
          {
            name: '失败',
            path: 'fail',
            component: dynamicWrapper(app, [], () =>
              import('../routes/Result/Error')
            ),
          },
        ],
      },
      {
        name: '异常',
        path: 'exception',
        icon: 'warning',
        hidden:true, // 不展示在左侧菜单栏上
        children: [
          {
            name: '403',
            path: '403',
            component: dynamicWrapper(app, [], () =>
              import('../routes/Exception/403')
            ),
          },
          {
            name: '404',
            path: '404',
            component: dynamicWrapper(app, [], () =>
              import('../routes/Exception/404')
            ),
          },
          {
            name: '500',
            path: '500',
            component: dynamicWrapper(app, [], () =>
              import('../routes/Exception/500')
            ),
          },
        ],
      },
    ],
  },
  {
    component: dynamicWrapper(app, [], () => import('../layouts/UserLayout')),
    path: '/index',
    layout: 'UserLayout',
    children: [{
      name:'首页',
      path: 'index',
      hidden:true, // 不展示在左侧菜单栏上
      component: dynamicWrapper(app, ['login'], () =>
        import('../routes/User/Login')
      ),
    }]
  },
/*  {
    component: dynamicWrapper(app, [], () => import('../layouts/UserLayout')),
    path: '/user',
    layout: 'UserLayout',
    children: [
      {
        name: '帐户',
        icon: 'user',
        path: 'user',
        hidden:true, // 不展示在左侧菜单栏上
        children: [
          {
            name: '',
            path: 'login',
            component: dynamicWrapper(app, ['login'], () =>
              import('../routes/User/Login')
            ),
          },
         /!* {
            name: '注册',
            path: 'register',
            component: dynamicWrapper(app, ['register'], () =>
              import('../routes/User/Register')
            ),
          },
          {
            name: '注册结果',
            path: 'register-result',
            component: dynamicWrapper(app, [], () =>
              import('../routes/User/RegisterResult')
            ),
          },*!/
        ],
      },
    ],
  }*/
];
