/**
 * Created by coolguy on 2017/4/17.
 */
export const callState = [{
    key:"START",
    name:"发起"
},{
    key:"ACCESS",
    name:"接通"
},{
    key:"LOST",
    name:"未接通"
},{
    key:"DURATION",
    name:"通话时长"
},]
export const contactsRelation = [{
   key:"1",
    name:"配偶(直系)"
}, {
    key:"2",
    name:"父母(直系)"
},{
    key:"3",
    name:"子女(直系)"
},{
    key:"4",
    name:"兄妹(直系)"
},{
    key:"5",
    name:"同事"
},{
    key:"6",
    name:"其他"
},{
    key:"7",
    name:"本人"
},{
    key:"8",
    name:"其他"
}];
export const followType = [{
    key: "PHONE",
    name: "电话"
},{
    key: "SMS",
    name: "短信",
},  {
    key: "INVITE",
    name: "邀请"
},{
    key: "VISIT",
    name: "拜访"
}, {
    key: "POST_LOAN",
    name: "贷后"
}, {
    key: "OTHERS",
    name: "其他"
}];
export const planType = followType;



export const marriage = [{
    key:"1",
    name:"未婚"
}, {
    key:"2",
    name:"已婚"
}, {
    key:"3",
    name:"离婚"
}, {
    key:"4",
    name:"丧偶"
}, {
    key:"5",
    name:"再婚"
}];
export const education = [{
    key:"1",
    name:"小学"
}, {
    key:"2",
    name:"初中"
}, {
    key:"3",
    name:"高中"
}, {
    key:"4",
    name:"中专"
}, {
    key:"5",
    name:"大专"
}, {
    key:"6",
    name:"本科"
}, {
    key:"7",
    name:"硕士"
}, {
    key:"8",
    name:"博士"
}, {
    key:"9",
    name:"其他"
}];



export const channelList=[{
  key:'METRO_AD',
  name:'地铁广告',
},{
  key:'ELEVATOR_AD',
  name:'电梯广告',
},{
  key:'VILLAGE_AD',
  name:'小区广告',
},{
  key:'APP',
  name:'手机客户端',
},{
  key:'SEARCH_ENGINE',
  name:'搜索引擎营销',
},{
  key:'SOCIAL_MEDIA',
  name:'社会化媒体营销',
},{
  key:'NEWSPAPER',
  name:'报纸',
},{
  key:'MAGAZINE',
  name:'杂志',
},{
  key:'BANK',
  name:'银行',
},{
  key:'OLD_CRM',
  name:'老CRM渠道',
},{
  key:'NEW_CUSTOMER',
  name:'新呼入客户',
}];


export const productType=[{
  key:'CREDIT_LOAN',
  name:'信用贷款',
},{
  key:'SHORT_TERM_LEND',
  name:'短期拆贷',
},{
  key:'MORTGAGE',
  name:'抵押贷款',
},{
  key:'ENTERPRISE_LOAN',
  name:'企业贷款',
},{
  key:'OTHERS',
  name:'其他',
}]

export const productStatus=[{
  key:'USING',
  name:'使用中',
},{
  key:'ABANDON',
  name:'废弃',
},{
  key:'EXPIRED',
  name:'已过期',
}]

export const channelStatus=[{
  key:'USING',
  name:'使用中',
},{
  key:'ABANDON',
  name:'废弃',
}]
export const customerType=[{
  key:'PUBLIC',   //普通
  name:'普通客户'
},{
  key:'PRIVATE',
  name:'私有客户'
}]

export const customerStatus=[{
  key:'INIT',name:'新建'},{
  key:'FOLLOW',name:'跟进营销'},{
  key:'INTENTION',name:'意向池'},{
  key:'MAINTAIN',name:'做单中'},{
  key:'LOSE',name:'输单'},{
  key:'SUCCESS',name:'赢单'},{
  key:'INVALID',name:'无效'
}]

export const payTypes=[
  {key:'CASH',name:'现金'},
  {key:'BANK',name:'银行'},
  {key:'WEIXIN',name:'微信'},
  {key:'ALIPAY',name:'支付宝'},
  {key:'OTHERS',name:'其他'}]

export const signStatus=[
  {key:'WAIT_SIGN',name:'待签单'},
  {key:'ACCEPT',name:'做单中'},
  {key:'SETTLE',name:'结算中'},
  {key:'SUSPEND',name:'挂起'},
  {key:'BACK',name:'退单'},
  {key:'DONE',name:'完成'}
]

export const listParams = {
    "dir": "DESC",
    "current": 1,//保持antd的风格
    "pageSize": 5,
    "sortKey": "createTime",
};

export const Incomes=[{key:'IA',name:'综合收入'},
  {key:'IB',name:'预收定金'},
  {key:'IC',name:'贷款服务费(不含垫金)'},
  {key:'ID',name:'违约金'},
  {key:'IE',name:'资金服务费'},
  {key:'IF',name:'垫资及房贷利息'},
  {key:'IG',name:'其它加收费'},
  {key:'IH',name:'材料费代收'},
  {key:'II',name:'资方返点'},
  {key:'IJ',name:'利收'},
  {key:'IK',name:'代收款'},
  {key:'IL',name:'代查档费'},
  {key:'IM',name:'公证费'},
  {key:'IN',name:'评估费'},
  {key:'IO',name:'家访考察费'},
  {key:'IP',name:'合作分成'},
  {key:'IQ',name:'担保费'},
]

export const Costs=[{key:'CA',name:'评估费'},
  {key:'CB',name:'看房费'},
  {key:'CC',name:'房管局换证费'},
  {key:'CD',name:'车辆使用费'},
  {key:'CF',name:'见单放款费'},
  {key:'CE',name:'完善资料费'},
  {key:'CG',name:'垫资公正费'},
  {key:'CH',name:'提前取它项费'},
  {key:'CI',name:'退换定金'},
  {key:'CJ',name:'综合支出'},
  {key:'CK',name:'银行回报（返佣）'},
  {key:'CL',name:'公关费'},
  {key:'CM',name:'资金成本'},
  {key:'CN',name:'快件费'},
  {key:'CO',name:'重单返佣'},
  {key:'CP',name:'活动基金'},
  {key:'CQ',name:'外联在途费'},
  {key:'CR',name:'预评费'},
  {key:'CS',name:'材料费'},
  {key:'CT',name:'CES'},
]
