/**
 * Created by chenxq on 2017/12/12.
 */
import React,{PureComponent} from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { Table, Card, Button ,Form ,Input ,Row ,Col ,Select ,Popconfirm ,Modal ,message ,Divider,DatePicker,Radio} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from '../TableList.less';
import {listParams,channelList} from '../defaultData';
import {findObjByKey ,convertLongToString} from '../../utils/index';
import {reg} from '../../utils/CheckList';
const FormItem = Form.Item;
const { Option } = Select;
const {RangePicker} = DatePicker;
const RadioGroup = Radio.Group;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 2 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 5 },
  },
};

@connect(state=>({
  channel:state.customer_channel,
  userManage:state.usermanagement
}))
@Form.create()
export default class extends PureComponent{
  state={
    pagination:{
      ...listParams,
    },
    isvisible:false,
    editObj:{},
    key:Math.random()
  }
  defaultData={
    name:'',
    address:'',
    remark:'',
    work:'',
    type:channelList[0].key,
    phone:'',
    sex:'MAN',

  }
  columns=[{
    title: '名称',
    dataIndex: 'name',
  },{
    title:'手机号',
    dataIndex:'phone'
  },{
    title:'性别',
    dataIndex:'sex',
    render(sex){
      return sex==='MAN'?'男':'女'
    }
  },{
    title:'地址',
    dataIndex:'address'
  },{
    title:'工作单位',
    dataIndex:'work'
  },{
    title: '渠道类型',
    dataIndex: 'type',
    render:(type)=>{
      let obj = findObjByKey(channelList,type,"key");
      return obj ? obj.name : "未知"
    }
  },{
    title: '创建时间',
    dataIndex: 'createdTime',
    render:(createdTime)=>{
      return createdTime? convertLongToString(createdTime) : '';

    }
  },{
    title:'更新时间',
    dataIndex:'updatedTime',
    render(updatedTime){
      return updatedTime? convertLongToString(updatedTime) : '';
    }
  },{
    title: '备注',
    dataIndex: 'remark'
  },{
    title: "操作",
    dataIndex: "id",
    render: (id,record) =>
      <div className="operations">
        <a onClick={this.showDetail.bind(this,record)}>编辑</a>
        <Divider type="vertical" />
        <Popconfirm title="确认要删除该客户渠道?" onConfirm={this.remove.bind(this,record.id)}><a>删除</a></Popconfirm>
      </div>
  }];
  showDetail=(params)=>{
    if(this.props.userManage.allList.length<=0){
      message.error('请先添加用户');
      return ;
    }
    //this.props.form.setFieldsValue(obj);
    this.setState({
      editObj:{...params},
      isvisible:true,
      key:Math.random()
    })

  }
  remove=(id)=>{
    let _this=this;
    this.props.dispatch({
      type:'customer_channel/fetchRemove',
      payload:{id}
    }).then(jsonData=>{
      if(jsonData.result){
        message.success('删除成功',0.8,()=>{
          _this.getList();
        })
      }else{
        message.error(jsonData.msg);
      }
    })
  }
  submit=()=>{
    let _this=this;
    this.props.form.validateFieldsAndScroll((err,values)=>{
      if(!err){
        let params={};
        for(var i in values){
          if(i.indexOf('search')<0){
            params[i]=values[i];
          }
        }
        console.log('params',params);
        let editObj=Object.assign({},this.state.editObj,params);
        let patchType='customer_channel/fetchAdd';
        if(editObj.id){
          patchType='customer_channel/fetchEdit'
        }
        this.props.dispatch({
          type:patchType,
          payload:editObj
        }).then(jsonData=>{
          if(jsonData.result){
            message.success(`${editObj.id?'修改':'添加'}成功`,0.8,()=>{
              _this.handleCancel();
              _this.getList();
            })

          }else{
            message.error(jsonData.msg)
          }
        });
      }
    })

  }
  handleCancel=()=>{
    this.setState({
      isvisible:false
    })
  }

  handleSearch=(e)=>{
    e.preventDefault();
    let values=this.props.form.getFieldsValue();
    let params={};
    for(var i in values){
      if(i==='searchTime'){
        if(values[i] && values[i].length==2){
          params.start=new Date(values[i][0]).getTime();
          params.end=new Date(values[i][1]).getTime();
        }else{
          params.start=null;
          params.end=null;
        }

      }
      if(i.indexOf('search')>=0 && i!=='searchTime'){
        let index=i.toLowerCase().replace('search','');
        params[index]=values[i];
      }
    }
    params.current=1;
    this.setState({
      pagination:Object.assign(this.state.pagination,params)
    },()=>{
      this.getList();
    })
  }

  getList=(page={current:1})=>{
    let pagination=this.state.pagination;
    this.props.dispatch({
      type:'customer_channel/fetchList',
      payload:Object.assign({},pagination,page)
    })
  }
  renderForm=()=>{
    const { getFieldDecorator } = this.props.form;
    return (<Form onSubmit={this.handleSearch} layout="inline" className="ant-advanced-search-form">
      <Row gutter={{ md: 24, lg: 24, xl: 24 }}>
        <Col md={8}>
          <FormItem label="名称" >
            {getFieldDecorator('searchName')(
              <Input placeholder="请输入名称" />
            )}
          </FormItem>
        </Col>
        <Col md={8}>
          <FormItem label="手机号" >
            {getFieldDecorator('searchPhone')(
              <Input placeholder="请输入手机号" />
            )}
          </FormItem>
        </Col>
        <Col md={8}>
          <FormItem label="地址" >
            {getFieldDecorator('searchAddress')(
              <Input placeholder="请输入地址" />
            )}
          </FormItem>
        </Col>

      </Row>
      <Row gutter={{ md: 24, lg: 24, xl: 24 }} >
        <Col md={8} sm={10}>
          <FormItem label="渠道类型" >
            {getFieldDecorator('searchType')(
              <Select placeholder="请选择" style={{ width: '100%' }}>
                <Option value={null}>请选择</Option>
                {
                  channelList.map(item=><Option key={item.key} value={item.key}>{item.name}</Option>)
                }
              </Select>
            )}
          </FormItem>
        </Col>
        <Col md={10}>
          <FormItem label="起止时间" >
            {getFieldDecorator('searchTime',{
              rules: [{ type: 'array', required: false, message: 'Please select time!' }],
            })(
              <RangePicker />
            )}
          </FormItem>
        </Col>
        <Col md={1} sm={24}>
          <FormItem >
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
            </span>
          </FormItem>
        </Col>
      </Row>
    </Form>)
  }

  componentDidMount(){
    this.getList();
    this.props.dispatch({
      type:'usermanagement/fetchAllList',
      payload:{}
    })
  }
  render(){
    let {getFieldDecorator} = this.props.form;
    let {editObj}= this.state;
    let {userManage} = this.props;
    return (<PageHeaderLayout title="客户渠道列表" >
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>
            {this.renderForm()}
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={this.showDetail.bind(this,this.defaultData)}>
              新增客户渠道
            </Button>
          </div>
          <div style={{marginTop: "20px"}}>
            <Table rowKey="id"
                   loading={this.props.channel.loading}
                   columns={this.columns}
                   pagination={{...this.state.pagination,total:this.props.channel.total}}
                   dataSource={this.props.channel.list}
                   onChange={this.getList}
            />
          </div>
        </div>
      </Card>
      <Modal title={editObj.id ? "编辑客户渠道":"新增客户渠道"}
             onOk={this.submit}
             onCancel={this.handleCancel}
             visible={this.state.isvisible}
             key={this.state.key}
      >
        <Form className={styles.modal}>
          <div className="flex-row">
            <div className="flex1">
              <FormItem label="名称">
                {getFieldDecorator('name', {
                  initialValue:editObj.name || '',
                  rules: [{
                    required: true, message: '请输入渠道名称'
                  }],
                })(
                  <Input placeholder='请输入渠道名称' style={{width:"80%"}}  />
                )}
              </FormItem>
            </div>
            <div className="flex1">
              <FormItem label="手机号">
                {getFieldDecorator('phone', {
                  initialValue:editObj.phone || '',
                  rules: [{
                    required: true, message: '请输入手机号'
                  },{pattern:reg.mobile,message:'手机号格式不正确'}],
                })(
                  <Input placeholder='请输入手机号' maxLength={"11"} style={{width:"80%"}}  />
                )}
              </FormItem>
            </div>
          </div>
          <div className="flex-row">
            <div className="flex1">
              <FormItem label="渠道类型">
                {getFieldDecorator('type',{
                  initialValue: editObj.type || channelList[0].key,
                  rules:[{
                    required:true,
                    message:'请选择渠道类型'
                  }]
                })(
                  <Select style={{width:"80%"}}>
                    {channelList.map(item=><Option key={item.key} value={item.key}>{item.name}</Option>)}
                  </Select>
                )}
              </FormItem>
            </div>
            <div className="flex1">
              {userManage.allList.length>0 &&<FormItem label="跟进人">
                {getFieldDecorator('followUserId',{
                  initialValue: editObj.followUserId || userManage.allList[0].id,
                  rules:[{
                    required:true,
                    message:'请选择跟进人'
                  }]
                })(
                  <Select style={{width:"80%"}}>
                    {userManage.allList.map(item=><Option key={item.id} value={item.id}>{item.userName}</Option>)}
                  </Select>
                )}
              </FormItem>}
            </div>
          </div>
          <div className="flex-row">
            <div className="flex1 text-left">
              <FormItem label="性别"
                labelCol={{
                xs: { span: 24 },
                sm: { span: 6 ,offset:0},
              }}
                wrapperCol= {{
                xs: { span: 24 },
                sm: { span: 16 },
              }}
              >
                {getFieldDecorator('sex',{
                  initialValue:editObj.sex || 'MAN',
                  rules: [{
                    required: true, message: '请输入渠道名称'
                  }],
                })(
                  <RadioGroup>
                    <Radio value="MAN">男</Radio>
                    <Radio value="WOMAN">女</Radio>
                  </RadioGroup>
                )}
              </FormItem>
            </div>
            <div className="flex1"></div>
          </div>

          <FormItem label="工作单位">
            {getFieldDecorator('work',{
              initialValue: editObj.work || '',
              rules:[{
                required:true,
                message:'请输入工作单位'
              }]
            })(
              <Input placeholder='请输入工作单位'/>
            )}
          </FormItem>
          <FormItem label="地址">
            {getFieldDecorator('address',{
              initialValue:editObj.address || '',
              rules:[{
                required:true,message: '请输入地址'
              }]
            })(
              <Input placeholder='请输入地址' />
            )}
          </FormItem>
          <FormItem label="备注">
            {getFieldDecorator('remark',{
              initialValue:editObj.remark || '',
            })(
              <Input.TextArea placeholder='请输入备注' style={{height:"70px"}}  />
            )}
          </FormItem>
        </Form>
      </Modal>
    </PageHeaderLayout>)
  }
}
