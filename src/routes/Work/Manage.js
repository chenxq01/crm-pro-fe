/**
 * Created by chenxq on 2018/1/3.
 */
/**
 * Created by chenxq on 2018/1/2.
 */
import React,{PureComponent,Component} from 'react';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import {Card,Table,Row,Col,Select,DatePicker,Divider,Avatar,Input,Button,Form,message,Badge} from 'antd';
import request from '../../utils/request';
import {connect} from 'dva';
import {listParams} from '../defaultData';
import {convertLongToString } from '../../utils/index'
const FormItem = Form.Item;
const { Option } = Select;
const {RangePicker}= DatePicker;
@connect(state=>({
    currentUser: state.user.currentUser,
  })
)
 export default class extends PureComponent{

  state={
    detail:{},
    list:[],
    pagination:{...listParams},
    loading:false,
    total:0
  }
  getDetail=()=>{
    let _this=this;
    request(`/v1/customer/plan/manage/overview`,{method:'POST'}).then(jsonData=>{
      if(jsonData.result){
        console.log('jsss',jsonData);
        _this.setState({
          detail:jsonData.data || {}
        })
      }
    })
  }
  search=(params)=>{
    let pagination={
      ...listParams,
      ...params
    }
    this.getList(pagination);
  }
  getList=(page={current:1})=>{
    let _this=this;
    let pagination=this.state.pagination;
    let params={...pagination,
      ...page
    }
    params.page=params.current;
    this.setState({
      pagination:params,
      loading:true
    })
    request(`/v1/user/member/stat?page=${params.page}&pageSize=${params.pageSize}`).then(jsonData=>{
      let data=jsonData.data || {};
      let total=data.total || 0;
      let list=data.list || [];
      console.log('list',jsonData);
      _this.setState({
        loading:false,
        total,
        list
      })
      if(!jsonData.result){
        message.error(jsonData.msg || '获取数据失败');
      }
    })

  }
  componentDidMount(){
    this.getDetail();
    this.getList();

  }
  getDescription=()=>{
    let {detail} = this.state;
    return (<div>
      {detail.group ||'分组名'}
      <Divider type="vertical" />
      {detail.role || '角色'}
    </div>)
  }
  columns=[{
    title:'坐席名称',
    dataIndex:'userName'
  },{
    title:'分组',
    dataIndex:'groupName',
  },{
    title:'分机号',
    dataIndex:'agentId',
  },{
    title:'今日通话时长',
    dataIndex:'dayDuration'
  },{
    title:'当前状态',
    dataIndex:'status',
    render(status){
      return status==1?<Badge status="success" text="通话中" />:<Badge status="error" text="空闲" />
    }
  },{
    title:'状态时长',
    dataIndex:'holdDuration'
  }]
  render(){
    let {currentUser} = this.props;
    let {detail} = this.state;
    return (<div>
      <Card title="工作台">
        <Card.Meta style={{float:'left'}}
                   avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                   title={`您好，${currentUser.userName}，祝您开心每一天`}
                   description={this.getDescription()}
        />
        <div className="flex-row" style={{float:"right"}}>
          <div style={{textAlign:"center"}}>
            <p>今日分配客户数</p>
            <p style={{fontSize:24}}>{detail.todayCustomerNum}</p>
          </div>
          <Divider type="vertical" style={{height:40,margin:20}}></Divider>
          <div style={{textAlign:"center"}}>
            <p>团队客户总数</p>
            <p style={{fontSize:24}}>{detail.totalCustomerNum}</p>
          </div>
        </div>
      </Card>
      <Card title="呼叫监控" style={{marginTop:20}}
            extra={<a onClick={this.getList}>刷新</a>}
      >
        <Table columns={this.columns}
               rowKey="userId"
               loading={this.state.loading}
               pagination={{...this.state.pagination,total:this.state.total}}
               dataSource={this.state.list}
               onChange={this.getList}
               style={{marginTop:20}}

        ></Table>
      </Card>
    </div>)
  }

}



