/**
 * Created by chenxq on 2017/12/16.
 */
import React, { Component} from 'react';
import BaseCustomer from './BaseCustomer';

// 公共资源池
export class Public extends Component{
  render(){
    let params={
      type:'PUBLIC',
      state:'INIT'
    }
    return <BaseCustomer params={params}></BaseCustomer>
  }
}
// 私有资源池
export class Private extends Component{
  render(){
    let params={
      type:'PRIVATE',
      state:'INIT'
    }
    return <BaseCustomer params={params}></BaseCustomer>
  }
}

//营销客户
export class Follow extends Component{
  render(){
    let params={
      state:'FOLLOW'
    }
    return <BaseCustomer params={params}></BaseCustomer>
  }
}

//意向客户
export class Intention extends Component{
  render(){
    let params={
      state:'INTENTION'
    }
    return <BaseCustomer params={params}></BaseCustomer>
  }
}
//签约
export class Maintain extends Component{
  render(){
    let params={
      state:'MAINTAIN'
    }
    return <BaseCustomer params={params}></BaseCustomer>
  }
}

//输单
export class Lose extends Component{
  render(){
    let params={
      loseStep:1,
      state:'LOSE'
    }
    return <BaseCustomer params={params}></BaseCustomer>
  }
}

//赢单
export class Success extends Component{
  render(){
    let params={
      state:'SUCCESS'
    }
    return <BaseCustomer params={params}></BaseCustomer>
  }
}




//无效
export class Invalid extends Component{
  render(){
    let params={
      state:'INVALID'
    }
    return <BaseCustomer params={params}></BaseCustomer>
  }
}
