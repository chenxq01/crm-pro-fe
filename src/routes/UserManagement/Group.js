/**
 * Created by chenxq on 2017/12/11.
 */
import React,{PureComponent} from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { Table, Card, Button, Input ,Popconfirm,Modal, message,Divider} from 'antd';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import styles from './index.less';
@connect(state => ({
  user:state.usermanagement,
  roleList:[],
  group:state.group
}))
export default class extends PureComponent{
  state={
    isvisible:false,
    editObj:{}
  }
  columns = [{
    title: '分组名',
    dataIndex: 'name',
    //render: text => <a href="#">{text}</a>,
  },  {
    title: "操作",
    dataIndex: "id",
    render: (id,record) =>
      <div className="operations">
        <a onClick={this.showDetail.bind(this,record)}>编辑</a>
        <Divider type="vertical" />
        <Popconfirm title="确认要删除该用户组?" onConfirm={this.remove.bind(this,record.id)}><a>删除</a></Popconfirm>
      </div>
  }];
  showDetail=(editObj)=>{
    console.log('record',editObj);
    this.setState({
      editObj:{
        ...editObj
      },
      isvisible:true
    })
  }
  remove=(id)=>{
    let _this=this;
    this.props.dispatch({
      type:'group/fetchRemove',
      payload: id
    }).then(jsonData=>{
      if(jsonData.result){
        message.success('删除成功',0.8,()=>{
          _this.getGroupList();
        })
      }else{
        message.error(jsonData.msg)
      }
    })
  }
  addUserGroup=()=>{
    let _this=this;
    let {editObj} = this.state;
    if(!editObj.name){
      message.error('请输入组名');
      return ;
    }
    let params={
      name:editObj.name
    }
    let type='group/fetchAdd';
    if(editObj.id){
      params.id=editObj.id;
      type='group/fetchEdit'
    }
    this.props.dispatch({
      type,
      payload:params
    }).then(jsonData=>{
      if(jsonData.result){
        message.success(`${editObj.id?'修改':'添加'}成功`,0.8,()=>{
          _this.handleCancel();
          _this.getGroupList();
        })
      }else{
        message.error(jsonData.msg);
      }
    })

  }
  handleCancel=()=>{
    this.setState({
      isvisible:false
    })
  }
  changeGroupName=(e)=>{
    this.setState({
      editObj:{
        ...this.state.editObj,
        name:e.target.value
      }
    })
  }
  getGroupList=()=>{
    this.props.dispatch({
      type:'group/fetchList'
    })
  }
  componentDidMount(){
    this.getGroupList();
  }
  render(){
    let {editObj={}} = this.state;
    return (<PageHeaderLayout title="分组管理">
      <Card bordered={false}>
        <div className={styles.tableListOperator}>
          <Button icon="plus" type="primary" onClick={this.showDetail.bind(this,{})}>
            新增用户组
          </Button>
        </div>
        <div style={{marginTop: 20}}>
          <Table rowKey="id"
                 loading={this.props.group.loading}
                 columns={this.columns}
                 dataSource={this.props.group.groupList}
          />
        </div>
      </Card>
      <Modal title={editObj.id ? "编辑组":"新建组"}
             onOk={this.addUserGroup}
             onCancel={this.handleCancel}
             visible={this.state.isvisible}>
        <div className="flex1 dp-f a-c" style={{marginRight: '20px',marginBottom:'10px'}}>
          <div className="label">组名<span className="required">*</span></div>
          <div className="value flex1">
            <Input placeholder="请输入组名" value={editObj.name} onChange={this.changeGroupName}/>
          </div>
        </div>
      </Modal>
    </PageHeaderLayout>)
  }
}
