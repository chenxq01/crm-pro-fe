/**
 * Created by chenxq on 2018/1/3.
 */

import React, {Component} from 'react';
import SignList from './SignList';


export class SignWait extends Component{
  render(){
    return <SignList state="WAIT_SIGN"></SignList>
  }
}

export class SignAccept extends Component{
  render(){
    return <SignList state="ACCEPT"></SignList>
  }
}

export class SignSettle extends Component{
  render(){
    return <SignList state="SETTLE"></SignList>
  }
}

export class SignSuspend extends Component{
  render(){
    return <SignList state="SUSPEND"></SignList>
  }
}

export class SignBack extends Component{
  render(){
    return <SignList state="BACK"></SignList>
  }
}

export class SignDone extends Component{
  render(){
    return <SignList state="DONE"></SignList>
  }
}
