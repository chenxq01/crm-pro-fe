/**
 * Created by chenxq on 2017/12/29.
 */
/*
import React, { Component } from 'react';
import { connect } from 'dva';
import { Row, Col, Card, Tabs,DatePicker,Progress } from 'antd';
import {
  ChartCard,
} from '../../components/Charts';
import { getTimeDistance } from '../../utils/utils';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import {SignMonth,SignWeek,SignDay} from '../../components/Statistics/SignStatistics';
import SwitchRole from '../../components/SwitchRole';
import Title from '../../components/Statistics/Title';
import styles from './Analysis.less';
const { RangePicker } = DatePicker;
const TabPane = Tabs.TabPane;
import Debounce from 'lodash-decorators/debounce';
import Bind from 'lodash-decorators/bind';
import request from '../../utils/request';
@connect(state=>({
  chart:state.chart,
  group:state.group,
  userManage:state.usermanagement
}))
export default class extends Component{
  state={
    rangePickerValue: [],
    defaultType:'today',  // today week month year
    dayData:[],
    weekData:[],
    monthData:[],
    title:"",
    key:Math.random(),
    isvisible:false,
    searchInfo:{
      radio:1,
      selectArr:[]
    }
  }
  selectDate = (type) => {
    let momentArr=getTimeDistance(type);
    this.setState({
      rangePickerValue:momentArr ,
    });
    this.props.dispatch({
      type: 'chart/fetchSignData',
      payload:{
        start:momentArr[0].format('YYYY-MM-DD'),
        end:momentArr[1].format('YYYY-MM-DD')
      }
    });
  }
  getCountData=()=>{

  }
  isActive(type) {
    const { rangePickerValue } = this.state;
    const value = getTimeDistance(type);
    if (!rangePickerValue[0] || !rangePickerValue[1]) {
      return;
    }
    if (rangePickerValue[0].isSame(value[0], 'day') && rangePickerValue[1].isSame(value[1], 'day')) {
      return styles.currentDate;
    }
  }
  handleRangePickerChange = (rangePickerValue,dateString) => {
    this.setState({
      rangePickerValue,
    });
    this.props.dispatch({
      type: 'chart/fetchSignData',
      payload:{
        start:dateString[0],
        end:dateString[1]
      }
    });
  }
  @Debounce(500)
  getDayData=(params)=>{
    let {start,end} = params;
    //为空时不做处理
    if(start === end && !end){
      return;
    }
    this.getList({
      ...params,
      periodType:'DAY'
    });
  }
  @Debounce(500)
  getWeekData=(params)=>{
    let {start,end} = params;
    //为空时不做处理
    if(start === end && !end){
      return;
    }
    this.getList({
      ...params,
      periodType:'WEEK'
    });

  }
  @Debounce(500)
  getMonthData=(params)=>{
    let {start,end} = params;
    //为空时不做处理
    if(start === end && !end){
      return;
    }
    this.getList({
      ...params,
      periodType:'MONTH'
    });

  }
  getList=(params)=>{
    let {start,end,periodType}=params;
    if(start){
      start=start.format("YYYY-MM-DD");
    }
    if(end){
      end=end.format("YYYY-MM-DD");
    }
    let _this=this;
    request(`/v1/loan/stat/chart?start=${start}&end=${end}&periodType=${periodType}`).then(jsonData=>{
      if(jsonData.result){
        let data=jsonData.data || [];
        if(periodType==='DAY'){
          _this.setState({
            dayData:data
          })
        }
        if(periodType==='WEEK'){
          _this.setState({
            weekData:data
          })
        }
        if(periodType==='MONTH'){
          _this.setState({
            monthData:data
          })
        }

      }
    })
  }
  getUserList=()=>{
    let _this=this;
    this.props.dispatch({
      type:'group/fetchList',
      payload:{}
    }).then(jsonData=>{
      if(jsonData.result){
        _this.getData({
          radio:1,
          selectArr: jsonData.data.map(item=>item.id)
        })
      }
    })
    this.props.dispatch({
      type:'group/fetchSameGroupUserList',
      payload:{}
    })
  }
  getData=(params)=>{
    let {radio,selectArr} = params;
    let title='';
    if(selectArr.length>0){
      let arr= radio==1? this.props.group.groupList : this.props.group.sameGroupList;
      if(selectArr.length==arr.length){
        title=radio==1?'全部分组':'全部组员';
      }else{
        let nameArr=[];
        selectArr.forEach(one=>{
          let obj=arr.find(item=>item.id==one.id) || {};
          if(obj){
            nameArr.push(obj.name || obj.userName);
          }
        })
        title=nameArr.join(',');
      }
    }
    this.setState({
      searchInfo:params,
      isvisible:false,
      title
    },()=>{
      this.getCountData();
      this.getWeekData();
      this.getDayData();
      this.getMonthData();
    })
  }
  componentDidMount(){
    this.selectDate(this.state.defaultType);
    this.getUserList();
  }
  render(){
    let {rangePickerValue} = this.state;
    const topColResponsiveProps = {
      span:4
    };
    let {group} = this.props;
    const salesExtra = (
      <div className={styles.salesExtraWrap}>
        <div className={styles.salesExtra}>
          <a className={this.isActive('today')} onClick={() => this.selectDate('today')}>
            今日
          </a>
          <a className={this.isActive('week')} onClick={() => this.selectDate('week')}>
            本周
          </a>
          <a className={this.isActive('month')} onClick={() => this.selectDate('month')}>
            本月
          </a>
          <a className={this.isActive('year')} onClick={() => this.selectDate('year')}>
            全年
          </a>
        </div>
        <RangePicker
          value={rangePickerValue}
          onChange={this.handleRangePickerChange}
          style={{ width: 256 }}
        />
      </div>
    );
    let {signData,signList} = this.props.chart;
    return(<div>
      <PageHeaderLayout title="签单统计">
        <Title content={this.state.title} showButton={true} onClick={()=>{
          this.setState({
            isvisible:true,
            key:Math.random()
          })
        }} />
        <Card extra={salesExtra} title="统计" style={{marginTop:20}}>
          <Row gutter={24}>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span>签单总数</span>
                <p>{signData.total || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span >新增签单数</span>
                <p>{signData.waitSignNum || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span>做单中数</span>
                <p>{signData.acceptNum || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span>结算中数</span>
                <p>{signData.settleNum || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span>退单数</span>
                <p>{signData.backNum || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span>挂单数</span>
                <p>{signData.suspendNum || 0}</p>
              </div>
            </Col>
          </Row>
        </Card>
        <Row style={{marginTop:20}}>
          <Col {...topColResponsiveProps}>
            <ChartCard
              bordered={false}
              title="转化率"
              total={`${signData.percent || 0}%`}
              footer={null}
              contentHeight={46}
            >
              <Progress percent={signData.percent || 0} showInfo={false} />
            </ChartCard>
          </Col>
        </Row>
        <div style={{marginTop:40,padding:40,backgroundColor:"#fff"}}>
          <Tabs type="card">
            <TabPane tab="日" key="1"><SignDay getData={this.getDayData} data={this.state.dayData}/></TabPane>
            <TabPane tab="周" key="2"><SignWeek getData={this.getWeekData} data={this.state.weekData}/></TabPane>
            <TabPane tab="月" key="3"><SignMonth getData={this.getMonthData} data={this.state.monthData}/></TabPane>
          </Tabs>
        </div>
        <SwitchRole userList={group.sameGroupList || [] }
                    groupList={group.groupList || [] }
                    isvisible={this.state.isvisible}
                    onSubmit={this.getData}
                    {...this.state.searchInfo}
                    onCancel={()=>{
                      this.setState({
                        isvisible:false
                      })
                    }}
        />
      </PageHeaderLayout>
    </div>)
  }
}
*/
import React, { Component } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import { Row, Col, Card, Tabs,DatePicker,Progress ,Modal,Table,message} from 'antd';
import {
  ChartCard,
} from '../../components/Charts';
import { getTimeDistance } from '../../utils/utils';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import SwitchRole from '../../components/SwitchRole';
import styles from './Analysis.less';
const { RangePicker } = DatePicker;
const TabPane = Tabs.TabPane;
import Debounce from 'lodash-decorators/debounce';
import request from '../../utils/request';
import Title from '../../components/Statistics/Title';
import {SignMonth,SignWeek,SignDay} from '../../components/Statistics/SignStatistics';
import {findObjByKey,convertLongToString} from '../../utils/index';
import {payTypes,signStatus,listParams} from '../defaultData';
@connect(state=>({
  group:state.group,
  userManage:state.usermanagement
}))
export default class extends Component{
  state={
    rangePickerValue: [],
    daySendData:{},
    weekSendData:{},
    monthSendData:{},
    defaultType:'today',  // today week month year
    dayData:[],
    weekData:[],
    monthData:[],
    signData:{},
    signList:[],
    key:Math.random(),
    isvisible:false,
    title:"",
    searchInfo:{
      radio:1,
      selectArr:[]
    },
    countList:[],
    countListVisible:false,
    pagination:{
      ...listParams,
      total:0
    }
  }
  selectDate = (type) => {
    let momentArr=getTimeDistance(type);
    this.setState({
      rangePickerValue:momentArr ,
    },()=>{
      this.getCountData();
    });
  }
  isActive(type) {
    const { rangePickerValue } = this.state;
    const value = getTimeDistance(type);
    if (!rangePickerValue[0] || !rangePickerValue[1]) {
      return;
    }
    if (rangePickerValue[0].isSame(value[0], 'day') && rangePickerValue[1].isSame(value[1], 'day')) {
      return styles.currentDate;
    }
  }
  handleRangePickerChange = (rangePickerValue,dateString) => {
    this.setState({
      rangePickerValue,
    },()=>{
      this.getCountData()
    });
  }



  @Debounce(500)
  getDayData=(params)=>{
    if(!params){
      params=this.state.daySendData;
    }
    let {start,end} = params;
    //为空时不做处理
    if(start === end && !end){
      return;
    }
    this.setState({daySendData:params});
    this.getList({
      ...params,
      countBy:'DAY'
    });
  }
  @Debounce(500)
  getWeekData=(params)=>{
    if(!params){
      params=this.state.weekSendData;
    }
    let {start,end} = params;
    //为空时不做处理
    if(start === end && !end){
      return;
    }
    this.setState({weekSendData:params});
    this.getList({
      ...params,
      countBy:'WEEK'
    });

  }
  @Debounce(500)
  getMonthData=(params)=>{
    if(!params){
      params=this.state.monthSendData;
    }
    let {start,end} = params;
    //为空时不做处理
    if(start === end && !end){
      return;
    }
    this.setState({monthSendData:params});
    this.getList({
      ...params,
      countBy:'MONTH'
    });

  }
  getList=(params)=>{
    let {start,end,countBy}=params;
    this.setState({
        searchListParams:params
      }
    )
    if(start){
      params.start=start.format("YYYY-MM-DD") //+' 00:00:00';
    }
    if(end){
      params.end=end.format("YYYY-MM-DD") //+' 23:59:59';
    }
    let _this=this;
    let {searchInfo} = this.state;
    let stateName=countBy==='DAY'?'dayData':(countBy==='WEEK'?'weekData':'monthData');
    if(searchInfo.selectArr.length===0){
      this.setState({
        [stateName]:[]
      })
    }
    if(searchInfo.radio===1){
      params.groupIds=searchInfo.selectArr;
    }else{
      params.userIds=searchInfo.selectArr;
    }
    request(`/v1/loan/stat/chart`,{  ///v1/loan/stat/chart
      method:'POST',
      body:{
        ...params
      }
    }).then(jsonData=>{
      console.log('chart',jsonData,params)
      if(jsonData.result){
        let data=jsonData.data || [];
        _this.setState({
          [stateName]:data
        })
        /*if(periodType==='DAY'){
         _this.setState({
         dayData:data
         })
         }
         if(periodType==='WEEK'){
         _this.setState({
         weekData:data
         })
         }
         if(periodType==='MONTH'){
         _this.setState({
         monthData:data
         })
         }*/
      }
    })
  }
  getUserList=()=>{
    let _this=this;
    this.props.dispatch({
      type:'group/fetchList',
      payload:{}
    });
    this.props.dispatch({
      type:'group/fetchSameGroupUserList',
      payload:{}
    })
  }

  getCountData=()=>{
    let {searchInfo,rangePickerValue} = this.state;
    let params={};
    if(searchInfo.radio==1){
      params.groupIds=searchInfo.selectArr;
    }else{
      params.userIds=searchInfo.selectArr;
    }
    if(rangePickerValue.length>0){
      params.start=rangePickerValue[0].format("YYYY-MM-DD") //+' 00:00:00';
      params.end=rangePickerValue[1].format("YYYY-MM-DD") //+' 23:59:59';
    };
    let _this=this;
    request('/v1/loan/stat/count',{
      method:'POST',
      body:{
        ...params
      }
    }).then(jsonData=>{
      console.log('count',jsonData,params)
      if(jsonData.result){
        _this.setState({
          signData:jsonData.data
        })
      }
    })
  }

  getData=(params)=>{
    let {radio,selectArr} = params;
    let title='';
    if(selectArr.length>0){
      let arr= radio==1? this.props.group.groupList : this.props.group.sameGroupList;
      if(selectArr.length==arr.length){
        title=radio==1?'全部分组':'全部组员';
      }else{
        let nameArr=[];
        selectArr.forEach(one=>{
          let obj=arr.find(item=>item.id==one);
          if(obj){
            let name=obj.name || obj.userName;
            nameArr.push(name);
          }
        })
        title=nameArr.join('，');
      }
    }

    this.setState({
      searchInfo:params,
      isvisible:false,
      title
    },()=>{
      this.getCountData();
      this.getWeekData();
      this.getDayData();
      this.getMonthData();
    })
  }

  columns=[{
    title:'客户名称',
    dataIndex:'customerName'
  },{
    title:'产品名称',
    dataIndex:'productId'
  },{
    title:'支付方式',
    dataIndex:'payWay',
    render:(payWay)=>{
      return payWay? findObjByKey(payTypes,payWay).name :''
    }
  },{
    title:'当前状态',
    dataIndex:'currentState',
    render:(state)=>{
      return state ? findObjByKey(signStatus,state).name :''
    }
  },{
    title:'指派给',
    dataIndex:'assignedTo'
  },{
    title:'批复额度',
    dataIndex:'replyQuotas',
    render:(val)=>{
      if(typeof val != 'undefined'){
        return (val/100).toFixed(2)
      }else{
        return '';
      }
    }
  },{
    title:'批复利息',
    dataIndex:'replyInterest',
    render:(val)=>{
      if(typeof val != 'undefined'){
        return (val/100).toFixed(2)
      }else{
        return '';
      }
    }
  },{
    title:'批复周期',
    dataIndex:'replyPeriod'
  },{
    title:'应收总费用',
    dataIndex:'receivable',
    render:(val)=>{
      if(typeof val != 'undefined'){
        return (val/100).toFixed(2)
      }else{
        return '';
      }
    }
  },{
    title:'实收总费用',
    dataIndex:'netReceipt',
    render:(val)=>{
      if(typeof val != 'undefined'){
        return (val/100).toFixed(2)
      }else{
        return '';
      }
    }
  },{
    title:'创建时间',
    dataIndex:'createdTime',
    render:(time)=>{
      return time? convertLongToString(time) :''
    }
  },{
    title:'操作',
    dataIndex:'id',
    render:(id,record)=>{
      return <Link to={`/sign/detail/${id}`}>详情</Link>
    }
  },]

  setCountListType=(type)=>{
    this.loanState=type;
    this.getCountList();
  }
   //获取列表
  getCountList=(page={current:1})=>{
    let {searchInfo,rangePickerValue} = this.state;
    let pagination={
      ...this.state.pagination,
      ...page
    }
    let params={
      loanState:this.loanState,
      ...pagination
    }
    if(rangePickerValue.length>0){
      params.start=rangePickerValue[0].format("YYYY-MM-DD") //+' 00:00:00';
      params.end=rangePickerValue[1].format("YYYY-MM-DD") //+' 23:59:59';
    };
    if(searchInfo.radio==1){
      params.groupIds=searchInfo.selectArr;
    }else{
      params.userIds=searchInfo.selectArr;
    }
    let _this=this;
    params.page=params.current;
    request(`/v1/loan/stat/count/list`,{
      method:'POST',
      body:{
        ...params
      }
    }).then(jsonData=>{
      if(jsonData.result){
        let data=jsonData.data || {};
        let {list=[],total=0} = data;
        pagination.total=total || 0;
       _this.setState({
         countList:list,
         countListVisible:true,
         pagination
       })
      }else{
        message.error(jsonData.msg || '获取错误');
      }
    })
  }


  componentDidMount(){
    this.selectDate(this.state.defaultType);
    this.getUserList();
  }
  render(){
    let {rangePickerValue} = this.state;
    let {group} = this.props;
    const topColResponsiveProps = {
      span:4
    };
    const salesExtra = (
      <div className={styles.salesExtraWrap}>
        <div className={styles.salesExtra}>
          <a className={this.isActive('today')} onClick={() => this.selectDate('today')}>
            今日
          </a>
          <a className={this.isActive('week')} onClick={() => this.selectDate('week')}>
            本周
          </a>
          <a className={this.isActive('month')} onClick={() => this.selectDate('month')}>
            本月
          </a>
          <a className={this.isActive('year')} onClick={() => this.selectDate('year')}>
            全年
          </a>
        </div>
        <RangePicker
          value={rangePickerValue}
          onChange={this.handleRangePickerChange}
          style={{ width: 256 }}
        />
      </div>
    );
    let {signData} = this.state;
    return(<div>
      <PageHeaderLayout title="坐席统计">
        <Title content={this.state.title} showButton={true} onClick={()=>{
          this.setState({
            isvisible:true,
            key:Math.random()
          })
        }} />
        <Card extra={salesExtra} title="统计" style={{marginTop:20}}>
          <Row gutter={24}>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData} onClick={this.setCountListType.bind(this,'')}>
                <span>签单总数</span>
                <p>{signData.total || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData} onClick={this.setCountListType.bind(this,'WAIT_SIGN')}>
                <span >新增签单数</span>
                <p>{signData.waitSignNum || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span>做单中数</span>
                <p>{signData.acceptNum || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span>结算中数</span>
                <p>{signData.settleNum || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span>退单数</span>
                <p>{signData.backNum || 0}</p>
              </div>
            </Col>
            <Col {...topColResponsiveProps}>
              <div style={{textAlign:'center'}} className={styles.oneData}>
                <span>挂单数</span>
                <p>{signData.suspendNum || 0}</p>
              </div>
            </Col>
          </Row>
        </Card>
        <Row style={{marginTop:20}}>
          <Col {...topColResponsiveProps}>
            <ChartCard
              bordered={false}
              title="转化率"
              total={`${signData.percent || 0}%`}
              footer={null}
              contentHeight={46}
            >
              <Progress percent={signData.percent || 0} showInfo={false} />
            </ChartCard>
          </Col>
        </Row>
        <div style={{marginTop:40,padding:40,backgroundColor:"#fff"}}>
          <Tabs type="card">
            <TabPane tab="日" key="1"><SignDay getData={this.getDayData} data={this.state.dayData}/></TabPane>
            <TabPane tab="周" key="2"><SignWeek getData={this.getWeekData} data={this.state.weekData}/></TabPane>
            <TabPane tab="月" key="3"><SignMonth getData={this.getMonthData} data={this.state.monthData}/></TabPane>
          </Tabs>
        </div>
        <SwitchRole userList={group.sameGroupList || [] }
                    groupList={group.groupList || [] }
                    isvisible={this.state.isvisible}
                    onSubmit={this.getData}
                    {...this.state.searchInfo}
                    onCancel={()=>{
                      this.setState({
                        isvisible:false
                      })
                    }}


        />
        <Modal title="签单列表"
               visible={this.state.countListVisible}
               width="800"
               onCancel={()=>{
          this.setState({
            countListVisible:false
          })
        }}>
          <Table rowKey="id"
                 columns={this.columns}
                 dataSource={this.state.countList}
                 pagination={{...this.state.pagination}}
                 onChange={this.getCountList}
          />
        </Modal>
      </PageHeaderLayout>

    </div>)
  }
}
