/**
 * Created by CoolGuy on 2017/6/28.
 */
import React, { Component } from 'react';
import { DatePicker,message,Row,Col } from 'antd';
import moment from 'moment';
const RangePicker = DatePicker.RangePicker;

const disabledDate = (start,end)=>{
    // Can not select days before today and today
    //console.log(start.format("ll"),end.format("ll"));
    //let diff = start.diff(end,"days");
    return false;
    // return diff  > 12 || diff < -12;
};

class MonthStatistics extends Component{

    state = {
        start:moment().subtract(5, 'months').startOf("month"),
        end:moment().endOf("month"),
    };
    onChange = (dates, dateStrings)=> {
        console.log('From: ', dates[0], ', to: ', dates[1]);
        console.log('From: ', dateStrings[0], ', to: ', dateStrings[1]);
        let diff = dates[0].startOf("month").diff(dates[1].endOf("month"),"months");
        if(diff  >= 12 || diff <= -12){
            message.warn("所选时间超过了12个月，请重新选择");
            return false;
        }
        this.setState({
            start:dates[0].startOf("month"),
            end:dates[1].endOf("month"),
        },()=>{
            this.props.getData(this.state);
        })
    };

    componentDidMount() {
        this.props.getData(this.state);
    }
    render(){
        let {data,Graph} = this.props;

        return (
            <div>
                <Row>
                    <Col span={24} style={{textAlign:"right",marginRight:20}}>
                        <span style={{marginRight:20,marginLeft:20}}>请选择一个时间段，最多可选12个自然月</span>
                        <RangePicker
                            format="YYYY-MM"
                            ranges={{
                            '当月':[moment().startOf("month"),moment().endOf("month")],
                            '最近6个月': [moment().subtract(5, 'months').startOf("month"),moment()],
                            "最近12个月":[moment().subtract(11, 'months').startOf("month"),moment()]}}
                            value={[this.state.start,this.state.end]}
                            onChange={this.onChange}
                            disabledDate={disabledDate}
                        />
                    </Col>
                </Row>

                <div className="graph">
                    {(data&&data.length)?
                        <Graph data={data}/>:
                        <div style={{textAlign:"center",height:400,lineHeight:"400px"}}>咦，你选的时间段没有数据呢</div>
                    }
                </div>
            </div>
        )
    }
}

export default MonthStatistics;