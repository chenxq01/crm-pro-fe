/**
 * Created by CoolGuy on 2017/6/28.
 */
import React, { Component } from 'react';
import { DatePicker,message,Row,Col } from 'antd';
import moment from 'moment';
const RangePicker = DatePicker.RangePicker;

const disabledDate = (start,end)=>{
    // Can not select days before today and today
    //console.log(start.format("ll"),end.format("ll"));
    //let diff = start.diff(end,"days");
    return false;
    // return diff  > 12 || diff < -12;
};

class DayStatistics extends Component{

    state = {
        start:moment().subtract(7, 'days'),
        end:moment(),
    };
    onChange = (dates, dateStrings)=> {
        //console.log('From: ', dates[0], ', to: ', dates[1]);
        //console.log('From: ', dateStrings[0], ', to: ', dateStrings[1]);
        let diff = dates[0].diff(dates[1],"days");
        if(diff  > 12 || diff < -12){
            message.warn("所选时间超过了12天，请重新选择");
            return false;
        }
        this.setState({
            start:dates[0],
            end:dates[1],
        },()=>{
            this.props.getData(this.state);
        })
    };

    componentDidMount() {
        this.props.getData(this.state);
    }
    render(){
        let {data,Graph} = this.props;
        console.log(data);
        return (
            <div>
                <Row>
                    <Col span={24} style={{textAlign:"right",marginRight:20}}>
                        <span style={{marginRight:20,marginLeft:20}}>请选择一个时间段，最多可选12天</span>
                        <RangePicker
                            ranges={{ '最近7天': [moment().subtract(7, 'days'),moment()]}}
                            defaultValue={[moment().subtract(7, 'days'),moment()]}
                            value={[this.state.start,this.state.end]}
                            onChange={this.onChange}
                            disabledDate={disabledDate}
                        />
                    </Col>
                </Row>

                <div className="graph">
                    {(data&&data.length)?
                        <Graph data={data}/>:
                        <div style={{textAlign:"center",height:400,lineHeight:"400px"}}>咦，你选的时间段没有数据呢</div>
                    }
                </div>
            </div>
        )
    }
}

export default DayStatistics;