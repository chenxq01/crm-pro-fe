/**
 * Created by chenxq on 2017/12/29.
 */
import React,{Component} from 'react';
import Month from './MonthStatistics';
import Week from './WeekStatistics';
import Day from './DayStatistics';
import G2 from 'g2';
import Slider from 'g2-plugin-slider';
import uuid from '../../utils/uuid';
Array.prototype.max = function(){   //最大值
  return Math.max.apply({},this)
}
Array.prototype.min = function(){   //最小值
  return Math.min.apply({},this)
}
class App extends Component {
  state = {
    plotCfg: {
      margin: [20, 90, 60, 60]
    }
  };

  renderChart(data) {
    const { height = 400, margin = [60, 20, 40, 40], titleMap, borderWidth = 2 } = this.props;

    if (!data || (data && data.length < 1)) {
      return;
    }

    // clean
    if (this.sliderId) {
      document.getElementById(this.sliderId).innerHTML = '';
    }
    this.node.innerHTML = '';

    const chart = new G2.Chart({
      container: this.node,
      forceFit: true, //宽度自适应
      height, // 图标高度
      plotCfg: {
        margin,
      },
    });
    /*
    *
    * "date":"第45周",   // 日期，按返回数据直接显示
                 "waitSignNum":1,  // 新增签单数
                 "settleNum":1,    // 结算中签单数
                 "backNum":0       // 退单数
     */

    chart.axis('date', {
      title: false,
    });
    chart.axis('waitSignNum', {
      title: false,
    });
    chart.axis('settleNum', false);
    chart.axis('backNum', false);

    chart.legend({
      mode: false,
      position: 'top',
    });
    let Arr=[];
    for(var i=0; i<data.length; i++){
      let one =data[i];
      for(var j in one){
        if(j!=='date'){
          Arr.push(one[j])
        }
      }
    };
    let max=Arr.max();
    chart.source(data, {  // 设置数据源
      date:{
        //type:'timeCat',
       // tickCount: 16,
       // mask:'yyyy-mm-dd',
        //range:[0,1],
      },
      waitSignNum: {
        alias: '新增签单数',
        max,
        min: 0,
      },
      settleNum: {
        alias: '结算中签单数',
        max,
        min: 0,
      },
      backNum:{
        alias:'退单数',
        max,
        min:0
      }

    });

    chart.line().position('date*waitSignNum').color('#1890FF').size(borderWidth);
    chart.line().position('date*settleNum').color('#2FC25B').size(borderWidth);
    chart.line().position('date*backNum').color('#ff5678').size(borderWidth);
    chart.point().position('date*waitSignNum').color('#1890FF').size(4).shape('circle').style({
      stroke: '#fff',
      lineWidth: 1
    });

    chart.point().position('date*settleNum').color('#2FC25B').size(4).shape('circle').style({
      stroke: '#fff',
      lineWidth: 1
    });
    chart.point().position('date*backNum').color('#ff5678').size(4).shape('circle').style({
      stroke: '#fff',
      lineWidth: 1
    });
    this.chart = chart;
    chart.render();
  }
  handleRef = (n) => {
    this.node = n;
  }
  sliderId = `timeline-chart-slider-${uuid()}`;
  componentDidMount() {
    this.renderChart(this.props.data);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data !== this.props.data) {
      this.renderChart(nextProps.data);
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.destroy();
    }
    if (this.slider) {
      this.slider.destroy();
    }
  }
  render() {
    let {data} = this.props;
    if(!data || !data.length){
      return null;
    }
    return (
      <div className="chart" style={{height:400}}>
        <div>
          <div ref={this.handleRef} />
          <div id={this.sliderId} />
        </div>
      </div>
    );
  }
}

export const SignWeek=function(props){
  let {getData,data} = props;
  return <Week getData={getData} data={data} Graph={App}/>
}

export const SignDay=function(props){
  let {getData,data} = props;
  return <Day getData={getData} data={data} Graph={App}/>
}

export const SignMonth=function(props){
  let {getData,data} = props;
  return <Month getData={getData} data={data} Graph={App}/>
}
