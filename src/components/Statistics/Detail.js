/**
 * Created by CoolGuy on 2017/6/21.
 */
import React from 'react';
import {Layout,Row,Table,message} from 'antd';
const { Header, Content ,Footer} = Layout;
import {connect} from 'react-redux';
import actions from '../../actions';
import AjaxAction from '../../actions/AjaxAction';
import moment from 'moment';
import {callState} from '../../defaultData';
import {findObjByKey} from '../../util';
import CustomerDetail from '../ClientDetail';
class Detail extends React.Component{
    state = {
        loading:false,
        pagination:{
            page:1,
            pageSize:10,
        }
    };
    showDetail = (id, activeTab)=> {
        let {dispatch} = this.props;
        activeTab = typeof activeTab === "string" ? activeTab : undefined;
        //this.close();
        dispatch(actions.showDetail(true, activeTab));
        dispatch(actions.setCurrentCustomerId(id));
        dispatch(AjaxAction.customerDetail(id));
    };
    getColumns = ()=>{
      let {showStatisticsDetailData} = this.props;
        let {sType} = showStatisticsDetailData;
        if(sType === 'call'){
            return this.columnsForCall.concat(this.columnsPublic);
        }
        return this.columnsForCustomer.concat(this.columnsPublic);
    };
    columnsPublic = [{
        title: "操作",
        dataIndex: "customerId",
        render: (id, record) => {
            return (
                <div className="operations">
                    <a onClick={this.showDetail.bind(this,id)}>{"详情"}</a>
                </div>
            )
        }

    }];
    columnsForCustomer = [{
        title:"客户名",
        dataIndex:'customerName',
    },{
        title:"客户类型",
        dataIndex:"customerType",
        render:(type)=>{
            return type==='HIGH'?"优质用户":"普通用户"
        }
    },{
        title:"操作时间",
        dataIndex:"operationTime",
        render:(operationTime)=>{
            return moment(operationTime).format("YYYY-MM-DD HH:mm:ss");
        }
    },{
        title:"跟进人",
        dataIndex:"followerName"
    },{
        title:"操作人",
        dataIndex:"operationUserName"
    }];
    columnsForCall = [{
        title: "通话状态",
        dataIndex: "callState",
        render:(state)=>{
            let obj = findObjByKey(callState,state);
            return obj ? obj.name : state;
        }
    },{
        title:"拨打时间",
        dataIndex:"callTime",
        render:(time)=>{
            return moment(time).format("YYYY-MM-DD HH:mm:ss");
        }
    },{
        title:"通话时长(秒)",
        dataIndex:"duration",
        render:(time)=>{
            return time;
        }
    },{
        title:"客户名",
        dataIndex:"customerName"
    },{
        title:"客户类型",
        dataIndex:"customerType",
        render:(type)=>{
            return type==='HIGH'?"优质用户":"普通用户"
        }
    },{
        title:"拨打者姓名",
        dataIndex:"callerName"
    }];
    close = (e)=>{
        e.stopPropagation();
        let {dispatch} = this.props;
        dispatch(actions.showStatisticsDetail(false));
        //关闭后必须将相关数据恢复到初始状态，否则下次进入会记录
        this.setState({
            loading:false,
            pagination:{
                page:1,
                pageSize:10,
            }
        })
    };
    mergePagination = ()=>{
        return Object.assign({},this.state.pagination,this.props.showStatisticsDetailData.pagination);
    };
    tableChange = (pagination)=>{
        console.log(pagination);
        pagination.page = pagination.current;
        this.setState({
            pagination
        },()=>{
            this.getTheList();
        })

    };

    getTheList = ()=>{
        let {dispatch,showStatisticsDetailData} = this.props;
        let {sType} = showStatisticsDetailData;
        let {pagination} = this.state;
        let params = Object.assign({},pagination);
        //params.start && (params.start = (params.start).startOf("day").format("YYYY-MM-DD HH:mm:ss"));
        //params.end && (params.end = (params.end).endOf("day").format("YYYY-MM-DD HH:mm:ss"));

        let theAction = sType==="call"?AjaxAction.statisticsCountList:AjaxAction.customerStatisticsList;
        //CUSTOMER_STATISTICS_LIST
        this.setState({
            loading:true
        },()=>{
            dispatch(theAction(params)).then((data)=>{
                if(data.result){
                    console.log(data.data);
                    let {list,total} = data.data;
                    this.setState({
                        loading:false,
                        list,
                        pagination:Object.assign({},this.state.pagination,{
                            total
                        })
                    })
                }else{
                    message.error(data.msg);
                }
            })
        })
    }

    componentWillReceiveProps(newProps) {
        if(newProps && newProps.showStatisticsDetailData && newProps.showStatisticsDetailData.pagination){
            console.log(newProps);
            Object.assign(this.state.pagination,newProps.showStatisticsDetailData.pagination);
            this.setState({
                pagination:this.state.pagination
            },()=>{
                this.getTheList();
            })

        }
    }

    componentDidMount() {
        //console.log("detail mounted");
    }

    render(){
        let {showStatisticsDetail,showStatisticsDetailData} = this.props;
        let {sType} = showStatisticsDetailData || {};
        return (
        showStatisticsDetail ? <div className="client-detail" onClick={this.close}>
                <div className="content" onClick={(e)=>{e.stopPropagation();}}>
                    <Layout>
                        <Header style={{backgroundColor:"#ffffff",borderBottom:"1px solid #ccc"}}>
                            <Row type="flex" align="middle" >
                                <div>
                                    {showStatisticsDetailData.title}
                                </div>
                            </Row>
                        </Header>
                        <Layout style={{background:"#ffffff"}}>
                            <Content style={{margin:20}}>
                                <Table rowKey={sType==="call"?"recordId":""}
                                       loading={this.state.loading}
                                       columns={this.getColumns()}
                                       dataSource={this.state.list}
                                       pagination={this.mergePagination()}
                                       onChange={this.tableChange}
                                />
                            </Content>
                        </Layout>
                        <Footer style={{position:"fixed",right:10,bottom:10}}>

                        </Footer>
                    </Layout>
                </div>
                <CustomerDetail/>
            </div>:null
        )
    }
}

export default connect((state)=>({
    showStatisticsDetail:state.ui.showStatisticsDetail,
    showStatisticsDetailData:state.ui.showStatisticsDetailData,
}))(Detail);