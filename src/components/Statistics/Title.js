/**
 * Created by chenxq on 2017/12/31.
 */
import {Row,Col,Button} from 'antd';
export default function (props){
  return (
    <div className="title" style={{backgroundColor:'#fff',padding:20}}>
      <Row type="flex" align="middle">
        <Col span={16}>
          当前统计：{props.content}
        </Col>
        {props.showButton &&
        <Col span={8} style={{textAlign:"right"}}>
          <Button type="primary" onClick={props.onClick}>切换</Button>
        </Col>
        }
      </Row>
    </div>
  )
}
