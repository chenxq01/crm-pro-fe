/**
 * Created by CoolGuy on 2017/6/20.
 */
import React from 'react';
import {Modal,Row,Col,Checkbox,Radio} from 'antd';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
const RadioGroup = Radio.Group;
import AjaxAction from '../../actions/AjaxAction';
import actions from '../../actions';
class SwitchRole extends React.Component{

    state = {
        radio:0,
        checkedGroupList:[],
        checkedMemberList:[],
    };
    radioChange = (e)=>{
        this.setState({
            radio: e.target.value
        })
    };
    checkAllGroup = ()=>{
        let {userGroupList} = this.props;
        let {checkedGroupList} = this.state;
        //没有被全选时点击需要全选，否则就取消全选
        if(userGroupList.length !== checkedGroupList.length){
            this.setState({
                checkedGroupList:userGroupList.map((v)=>{
                    return v.id
                })
            })
        }else{
            this.setState({
                checkedGroupList:[]
            })
        }

    };
    checkAllMember = ()=>{
        let {userGroupSameList} = this.props;
        let {checkedMemberList} = this.state;
        if(userGroupSameList.length !== checkedMemberList.length){
            this.setState({
                checkedMemberList:userGroupSameList.map((v)=>{
                    return v.id
                })
            })
        }else{
            this.setState({
                checkedMemberList:[]
            })
        }
    };

    componentDidMount() {

    }
    handleOk = ()=>{
        let {state} = this;
        this.handleCancel();
        this.props.onOk && this.props.onOk(state);

    };
    handleCancel = ()=>{
        this.props.actionShowSwitchRole(false);
    }


    render(){
        let {state} = this;
        let {radio,checkedGroupList,checkedMemberList} = state;
        let {userGroupList,userGroupSameList,showSwitchRole,user} = this.props;
        return (
            <Modal title="切换角色"
                   visible={showSwitchRole}
                   onOk={this.handleOk}
                   onCancel={this.handleCancel}>

                <Row>
                    <Col span={24}>
                        <RadioGroup style={{width:"100%"}} value={radio} onChange={this.radioChange}>
                            <Row type="flex" align="middle" style={{borderBottom:"1px solid #dfdfdf",padding:10}}>
                                <Col span={8}>
                                    <Radio value={0}>分组</Radio>
                                </Col>
                                {user.roleCode === "FrontDeskGroupLeader" ? user.groupName :
                                <Col span={16}>

                                    <Row style={{borderBottom:"1px dashed #dfdfdf",padding:5}}>
                                        <Col span={24}>
                                            <Checkbox disabled={radio!==0} onClick={this.checkAllGroup}
                                                      checked={userGroupList &&userGroupList.length && userGroupList.length === checkedGroupList.length}>全部</Checkbox>
                                        </Col>
                                    </Row>
                                    <Row style={{padding:5}}>
                                        <Checkbox.Group value={checkedGroupList} onChange={(checkedGroupList)=>{this.setState({checkedGroupList})}}>
                                            {userGroupList.map((one)=>{
                                                return (<Checkbox disabled={radio!==0} key={one.id} value={one.id}>{one.name}</Checkbox>)
                                            })}
                                        </Checkbox.Group>

                                    </Row>
                                </Col>
                                }
                            </Row>


                            <Row type="flex" align="middle" style={{padding:10}}>
                                <Col span={8}>
                                    <Radio value={1}>组员</Radio>
                                </Col>
                                <Col span={16}>
                                    <Row style={{borderBottom:"1px dashed #dfdfdf",padding:5}}>
                                        <Col span={24}>
                                            <Checkbox disabled={radio!==1} onClick={this.checkAllMember}
                                                      checked={userGroupSameList && userGroupSameList.length && userGroupSameList.length === checkedMemberList.length}>全部</Checkbox>
                                        </Col>
                                    </Row>
                                    <Row style={{padding:5}}>
                                        <Checkbox.Group value={checkedMemberList} onChange={(checkedMemberList)=>{this.setState({checkedMemberList})}}>
                                            {userGroupSameList.map((one)=>{
                                                return (<Checkbox disabled={radio!==1} key={one.id} value={one.id}>{one.userName}</Checkbox>)
                                            })}
                                        </Checkbox.Group>
                                    </Row>
                                </Col>
                            </Row>


                        </RadioGroup>
                    </Col>

                </Row>




            </Modal>
        )
    }

}
export default connect((state)=>({
    userGroupList:state.userGroupList,
    userGroupSameList:state.userGroupSameList,//Layout会获取这个
    showSwitchRole:state.ui.showSwitchRole,
    user:state.user
}),dispatch=>{
    return {
        AjaxUserGroupList:bindActionCreators(AjaxAction.userGroupList,dispatch),
        actionShowSwitchRole:bindActionCreators(actions.showSwitchRole,dispatch),
    }
})(SwitchRole);