/**
 * Created by CoolGuy on 2017/6/20.
 */
import React from 'react';
import {DatePicker,Col,Row,Button} from 'antd';
import moment from 'moment';
const RangePicker = DatePicker.RangePicker;
export const OneData = (props)=>{
    let {mark} = props;
    if(!mark){
        mark = "-";
    }
    return (
        <div className="one-data">
            <div className="head">{props.title}</div>
            <div className="num">{props.content !== undefined ? props.content : mark}</div>
        </div>
    )
};
export const TitleDatePicker = (props)=>{
    return (
        <div>
            请选择时间段
            <RangePicker
                style={{marginLeft:20}}
                format="YYYY-MM-DD"
                ranges={{
                            "今天":[moment(),moment()],
                            "本周":[moment().weekday(0),moment()],
                            "本月":[moment().startOf('month'),moment()],
                            "所有":[],
                        }}
                defaultValue={[moment(),moment()]}
                value={[props.start,props.end]}
                onChange={props.onChange}
            />
        </div>

    )
};
export const Title = (props)=>{
    return (
        <div className="title">
            <Row type="flex" align="middle">
                <Col span={16}>
                    当前统计：{props.content}
                </Col>
                {props.showButton &&
                <Col span={8} style={{textAlign:"right"}}>
                    <Button type="primary" onClick={props.onClick}>切换</Button>
                </Col>
                }
            </Row>
        </div>
    )
}