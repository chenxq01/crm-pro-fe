/**
 * Created by chenxq on 2017/12/28.
 */
import React,{PureComponent} from 'react';
import { connect } from 'dva';
import { Table, Card, Button ,Form ,Input ,Row ,Col ,Select ,Popconfirm ,Modal ,message ,Divider,DatePicker,Radio, } from 'antd';
import {findObjByKey ,convertLongToString} from '../../utils/index';
import {Costs,Incomes,payTypes,signStatus,listParams} from '../../routes/defaultData';
import styles from '../../routes/TableList.less';
import reg from '../../utils/CheckList';
import AddAccount from './AddAccount';
const FormItem = Form.Item;
const { Option } = Select;
const {RangePicker} = DatePicker;
const RadioGroup = Radio.Group;
@connect(state=>({
  account:state.account,
  customer:state.customer,
  userManage:state.usermanagement,
}))
@Form.create()
export default class extends PureComponent{
  static defaultProps={
    type:1  // 1 根据客户获取的支出列表   2 根据签单获取的支出列表
  }
  state={
    pagination:{
      ...listParams,
    },
    editObj:{
      info:{},
      type:'income'
    },
    isvisible:false,
    key:Math.random()
  }

  columns=[{
    title:'记录类别',
    dataIndex:'flag',
    render:(flag)=>{
      return flag=='income'?'收入':'支出'
    }
  },{
    title:'类别',
    dataIndex:'type',
    render(type,record){
      let arr=record.flag==='income'?Incomes:Costs;
      return type ? findObjByKey(arr,type).name :''
    }
  },{
    title:'金额',
    dataIndex:'amount',
    render:(amount)=>{
      return (!!amount || amount===0) ? (amount/100).toFixed(2) : '';
    }
  },{
    title:'付款方式',
    dataIndex:'payWay',
    render:(payWay)=>{
      return payWay && findObjByKey(payTypes,payWay).name || ''
    }
  },{
    title:'日期',
    dataIndex:'date'
  },{
    title:'状态',
    dataIndex:'state',
    render:(state)=>{
      return state?'未支付':'已支付'
    }
  },{
    title:'收款人或支出人',
    dataIndex:'person',
    render:(person)=>{
      if(!person){
        return '';
      }
      let userList= this.props.userManage.allList || [];
      let obj= userList.find(item=>item.id == person) || {};
      return obj.userName || '';
    }
  },{
    title:'备注',
    dataIndex:'remark'
  },{
    title:'操作',
    dataIndex:'id',
    render:(id,record)=>{
      return (<div className="operations">
        <a onClick={this.showDetail.bind(this,record)}>编辑</a>
        <Divider type="vertical" />
        <Popconfirm title="确认要删除该条记录?" onConfirm={this.remove.bind(this,record.id)}><a>删除</a></Popconfirm>
      </div>)
    }
  }];
  showDetail=(editObj)=>{
    this.setState({
      isvisible:true,
      editObj:{
        info:{...editObj},
        type:editObj.flag
      },
      key:Math.random()
    })
  }
  handleCancel=()=>{
    this.setState({
      isvisible:false
    })
  }
  remove=(id)=>{
    let _this=this;
    this.props.dispatch({
      type:'account/fetchRemove',
      payload:id
    }).then(jsonData=>{
      if(jsonData.result){
        message.success('删除成功',0.8,()=>{
          _this.getList();
        })
      }else{
        message.error(jsonData.msg || '删除失败')
      }
    })
  }
  getList=(page={current:1})=>{
    let {type,loanId} = this.props;
    if(type==1){
      let pagination=this.state.pagination;
      let {curInfo} = this.props.customer ;
      let customerId=curInfo.id;
      let params={
        ...pagination,
        ...page,
        customerId
      }
      this.props.dispatch({
        type:'account/fetchList',
        payload:params
      });
    }
    if(type==2 && loanId){
      this.props.dispatch({
        type:'account/fetchListByLoanId',
        payload:loanId
      })
    }

  }

  componentDidMount(){
    this.getList();
  }
  render(){
    let {type} = this.props;
    return (<Card bordered={false}>
      <div className={styles.tableListOperator}>
        <div className={styles.tableListForm}>
        </div>
      </div>
      <div style={{marginTop: "20px"}}>
        {type==1 && <Table rowKey="id"
               loading={this.props.account.loading}
               columns={this.columns}
               pagination={{...this.state.pagination,total:this.props.account.total}}
               dataSource={this.props.account.list}
               onChange={this.getList}
        />}
        {type==2 && <Table rowKey="id"
                           loading={this.props.account.loading}
                           columns={this.columns}
                           dataSource={this.props.account.icList}
        />}
      </div>
      <AddAccount isvisible={this.state.isvisible}
                  key={this.state.key}
                  {...this.state.editObj}
                  onCancel={()=>{
                    this.setState({
                      isvisible:false
                    })
                  }}/>
    </Card>)
  }
}
