/**
 * Created by chenxq on 2018/1/4.
 */
import React,{Component} from 'react';
import {Modal,Form,Select,Row,Col,message} from 'antd';
import {connect} from 'dva';
import {signStatus} from '../../routes/defaultData';
const {Option} =Select;
const FormItem=Form.Item;
@connect(state=>({
  userManage:state.usermanagement
}))
@Form.create()
export default class extends Component{
  static defaultProps={
    signObj:{}
  }
   handleValidator=(rule,val,callback)=>{
     let userList=this.props.userManage.allList || [];
      if(!val){
        callback();
        return;
      }
      if(userList.findIndex(item=>item.id==val)>=0){
        callback();
      }else{
        callback('用户不存在')
      }
  }
  submit=()=>{
    let _this=this;
    this.props.form.validateFieldsAndScroll((err,values)=> {
      if (!err) {
        _this.props.dispatch({
          type:'sign/assign',
          payload:{
            ...values,
            id:_this.props.signObj.id
          }
        }).then(jsonData=>{
          if(jsonData.result){
            message.success('指派成功',0.8,()=>{
              _this.props.onSubmit && _this.props.onSubmit();
            })
          }else{
            message.error(jsonData.msg || '指派错误')
          }
        })
      }
    })
  }
  cancel=()=>{
    this.props.onCancel && this.props.onCancel();
  }
  render(){
    let {getFieldDecorator} = this.props.form;
    let {userManage,signObj} = this.props;
    let userList=userManage.allList || [];
    let _this=this;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (<Modal title="签单指派"
                   onOk={this.submit}
                   onCancel={this.cancel}
                   width={750}
                   visible={this.props.visible}
    >
      <Form >
        <Row gutter={24}>
          <Col span={12}>
            <FormItem label={'指派人'} {...formItemLayout}>
              {getFieldDecorator('userId',{
                initialValue:signObj.assignedTo || '',
                rules:[{required:true,message:`请选择指派人`},{
                  validator: _this.handleValidator
                }]
              })(
                <Select mode="combobox"
                        placeholder="请选择"
                        optionFilterProp="children"
                        optionLabelProp="children"
                >
                  {userList.map((item,index)=>(<Option key={item.id} value={item.id}>{item.userName}</Option>))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col span={12}>
            <FormItem label={'签单状态'} {...formItemLayout}>
              {getFieldDecorator('state',{
                initialValue:signObj.state,
                rules:[{required:true,message:`请选择签单状态`}]
              })(
                <Select placeholder="请选择">
                  {signStatus.map((item,index)=>(<Option key={item.key} value={item.key}>{item.name}</Option>))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
      </Form>
    </Modal>)
  }
}
