/**
 * Created by chenxq on 2017/12/28.
 */
import React,{Component} from 'react';
import style from './index.less';
import {listParams,payTypes,signStatus} from '../../routes/defaultData';
import {findObjByKey ,convertLongToString} from '../../utils/index';
export default class extends Component{
  getProName=(id,arr)=>{
    let obj=arr.find(item=>item.id==id) || {};
    return obj.name || '';
  }
  getUserName=(id,arr)=>{
    let obj= arr.find(item=>item.id==id) || {};
    return obj.userName || '';
  }
  render(){
    let {info,proList=[],userList=[]} = this.props;
    return (<div className={style.table}>
      <table>
        <thead>
        </thead>
        <tbody>
        <tr>
          <td>创建日期</td>
          <td>{info.createdTime? convertLongToString(info.createdTime) : ''}</td>
          <td>所做产品</td>
          <td colSpan="10">
            {this.getProName(info.productId,proList)}
          </td>
        </tr>
        <tr>
          <td>客户姓名</td>
          <td>{info.customerName}</td>
          <td>客户联系方式</td>
          <td colSpan="10"></td>
        </tr>
        <tr>
          <td>批复额度</td>
          <td>{info.replyQuotas && (info.replyQuotas /100).toFixed(2) || ''}</td>
          <td>批复利息</td>
          <td>{info.replyInterest && (info.replyInterest /100).toFixed(2) || ''}</td>
          <td>批复周期</td>
          <td>{info.replyPeriod}</td>
        </tr>
        <tr>
          <td>产品成本</td>
          <td>{info.cost? (info.cost/100).toFixed(2):''}</td>
          <td>定金</td>
          <td>{info.downpayment && (info.downpayment /100).toFixed(2) || ''}</td>
          <td>征信费</td>
          <td>{info.creditInvestigationFee && (info.creditInvestigationFee /100).toFixed(2) || ''}</td>
        </tr>
        <tr>
          <td>是否押还款期数</td>
          <td>{info.holdRepayment?'是':'否'}</td>
          <td>资料费</td>
          <td>{info.infoFee && (info.infoFee /100).toFixed(2) || ''}</td>
          <td>调查费</td>
          <td>{info.investigateFee && (info.investigateFee /100).toFixed(2) || ''}</td>
        </tr>
        <tr>
          <td>合同约定费用</td>
          <td>{info.contractualFee && (info.contractualFee /100).toFixed(2) || ''}</td>
          <td>加收费</td>
          <td>{info.additionalFee && (info.additionalFee /100).toFixed(2) || ''}</td>
          <td>是否有返点</td>
          <td>{info.rebate?'是':'否'}</td>
        </tr>
        <tr>
          <td>应收费用明细</td>
          <td colSpan={3}></td>
          <td>返点金额</td>
          <td>{info.rebateAmount && (info.rebateAmount /100).toFixed(2) || ''}</td>
        </tr>
        <tr>
          <td>应收费用总计</td>
          <td>{info.receivable && (info.receivable /100).toFixed(2) || ''}</td>
          <td>实收费用总计</td>
          <td>{info.netReceipt && (info.netReceipt /100).toFixed(2) || ''}</td>
          <td>付款方式</td>
          <td>{info.payWay ? findObjByKey(payTypes,info.payWay).name :''}</td>
        </tr>
        <tr>
          <td>备注</td>
          <td colSpan={10}></td>
        </tr>
        <tr>
          <td>客户经理</td>
          <td>{this.getUserName(info.customerManager,userList)}</td>
          <td>权证经理</td>
          <td>{this.getUserName(info.warrantManager,userList)}</td>
          <td>贷款经理</td>
          <td>{this.getUserName(info.loanManager,userList)}</td>
        </tr>
        <tr>
          <td colSpan={2}></td>
          <td>经手人</td>
          <td>{this.getUserName(info.brokerage,userList)}</td>
          <td>复合人</td>
          <td>{this.getUserName(info.reviewPerson,userList)}</td>
        </tr>
        </tbody>
      </table>
    </div>)
  }
}
