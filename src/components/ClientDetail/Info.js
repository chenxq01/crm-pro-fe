/**
 * Created by coolguy on 2017/4/15.
 */
import React from 'react';
import {Input,Select,Radio,DatePicker} from 'antd';
const RadioGroup = Radio.Group;
const Option = Select.Option;

export default class Info extends React.Component {
    changeValue = (e)=>{
        let value = e;
        let {type} = this.props;
        if(typeof e === "object" && typeof e.length !='number'){
            if(type === "datetime"){
                value = e;//.format("YYYY-MM-DD HH:mm:ss");
            }else if(type === "date"){
                value = e;//.format("YYYY-MM-DD");
            }else{
                value = e.target.value;
            }
        }
        this.props.onChange && this.props.onChange(value);
    };
    renderSelect = (arr,value)=>{
        return (
            <Select id={this.props.id}
                    placeholder="请选择"
                    disabled={this.props.disabled}
                    value={this.props.value}
                    onChange={this.changeValue}
                    mode={this.props.mode || ''}
            >
                {arr.map((v,i)=>{
                    return (
                        <Option key={i} value={v.key}>{v.name}</Option>
                    )
                })}
            </Select>
        )
    };
    renderRadio = (arr,value)=>{
        return (
            <RadioGroup id={this.props.id} disabled={this.props.disabled} value={this.props.value} onChange={this.changeValue}>
                {arr.map((v,i)=>{
                    return (
                        <Radio key={i} value={v.key}>{v.name}</Radio>
                    )
                })}
            </RadioGroup>
        )
    };
    renderDateTime = () => {
        //console.log(this.props.value,"datetime");
        return (
            <DatePicker id={this.props.id}
                        showTime
                        format="YYYY-MM-DD HH:mm:ss"
                        placeholder="选择时间"
                        disabled={this.props.disabled}
                        value={this.props.value}
                        onChange={this.changeValue}/>
        )
    };
    renderDate = ()=>{
        return (
            <DatePicker id={this.props.id}  disabled={this.props.disabled} format="YYYY-MM-DD" value={this.props.value} onChange={this.changeValue}/>
        )
    };
    renderInput = (id,value,type)=>{
        return <Input id={id}
                      type={type || "text"}
                      disabled={this.props.disabled}
                      placeholder={this.props.placeholder}
                      value={this.props.value}
                      onChange={this.changeValue}/>
    }
    renderWhich = (data,value)=>{
        let {type,id} = this.props;
        switch (type){
            case "select":return this.renderSelect(data,value);
            case "radio": return this.renderRadio(data,value);
            case "datetime": return this.renderDateTime();
            case "date": return this.renderDate();
            //case "password":return this.renderInput(id,value,type);
            default: return this.renderInput(id,value,type);
        }
    };
    getValue = ()=>{
        // let {type,id} = this.props;
        // switch (type){
        //     case "select": return
        // }
    };
    render() {
        let {label, value,required,data,isText} = this.props;
        return (
            <div className="flex1 dp-f a-c" style={{marginRight: 20+'px',marginBottom:10+'px'}}>
                <div className="label">{label}{required && <span className="required">*</span>}</div>
                <div className="value flex1">
                    {isText ? value : this.renderWhich(data,value)}
                </div>
            </div>

        )
    }

}
