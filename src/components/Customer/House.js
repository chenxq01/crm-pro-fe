/**
 * Created by chenxq on 2017/12/25.
 */
import React, {PureComponent} from "react";
import {connect} from "dva";
import {
  Button,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  message,
  Modal,
  Popconfirm,
  Radio,
  Row,
  Select,
  Table
} from "antd";
import styles from "../../routes/TableList.less";
const FormItem = Form.Item;
const {Option} = Select;
const {RangePicker} = DatePicker;
const RadioGroup = Radio.Group;
@connect(state => ({
  house: state.house
}))
@Form.create()
export default class extends PureComponent {
  state = {
    customerId: this.props.customerId || 0,
    editObj: {},
    isvisible: false,
    loading:false,
  };
  defaultData = {};
  columns = [{
    title: '房屋坐落',
    dataIndex: 'address'
  }, {
    title: '所有权人',
    dataIndex: 'ownRight'
  }, {
    title: '监证号',
    dataIndex: 'superviseNo'
  }, {
    title: '权证号',
    dataIndex: 'ownershipNo'
  }, {
    title: '不动产权第号',
    dataIndex: 'estateOwnershipNo'
  }, {
    title: '不动产单元号',
    dataIndex: 'estateUnitNo'
  }, {
    title: '首付金额',
    dataIndex: 'downPayment'
  }, {
    title: '月供金额',
    dataIndex: 'monthlyPayments'
  }, {
    title: '是否抵押',
    dataIndex: 'isPledge'
  }, {
    title: '备注',
    dataIndex: 'remark'
  }, {
    title: '操作',
    dataIndex: 'houseId',
    render: (houseId, record) => {
      return (<div className="operations">
        <a onClick={this.showDetail.bind(this, record)}>详情</a>
        <Divider type="vertical"/>
        <Popconfirm title="确认要删除该住宅信息?" onConfirm={this.remove.bind(this, record.houseId)}><a>删除</a></Popconfirm>
      </div>)
    }
  }];
  showDetail = (editObj) => {
    this.setState({
      isvisible: true,
      editObj: {
        ...editObj
      }
    })
  };
  handleCancel = () => {
    this.setState({
      isvisible: false
    })
  };
  remove = (id) => {
    let _this = this;
    this.props.dispatch({
      type: 'house/fetchRemove',
      payload: {
        customerId: this.state.customerId,
        ids: [id]
      }
    }).then(jsonData => {
      if (jsonData.result) {
        message.success('删除成功', 0.8, () => {
          _this.getList();
        })
      } else {
        message.error(jsonData.msg || '删除失败')
      }
    })
  };
  submit = () => {
    let _this = this;
    let {editObj} = this.state;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('values', values);
        let type = editObj.houseId ? 'house/fetchEdit' : 'house/fetchAdd';
        values.houseId = editObj.houseId;
        _this.props.dispatch({
          type,
          payload: {
            customerId: this.state.customerId,
            params: values
          }
        }).then(jsonData => {
          if (jsonData.result) {
            message.success('保存成功', 0.8, () => {
              _this.setState({
                isvisible: false
              });
              _this.getList();
            })
          } else {
            message.error(jsonData.msg || '操作失败')
          }
        })
      }
    })
  };
  getList = () => {
    let {customerId} = this.state;
    this.props.dispatch({
      type: 'house/fetchList',
      payload: customerId
    })
  };
  getFormCell = () => {
    return [{
      label: '房屋坐落',
      key: 'address',
      component: <Input placeholder=""/>,
      field: {rules: [{required: true, message: '请输入单位名称'}]},
      text: ''
    }, {
      label: '所有权人',
      key: 'ownRight',
      component: <Input placeholder=""/>,
      field: {rules: [{required: true, message: '请输入单位性质'}]},
      text: ''
    }, {
      label: '是否共有',
      key: 'isOwnership',
      component: <Input placeholder=""/>,
    }, {
      label: '共有人',
      key: 'ownership',
      component: <Input placeholder=""/>,
    }, {
      label: '共有人份额',
      key: 'ownershipShare',
      component: <Input placeholder=""/>,
    }, {
      label: '与所有权人关系',
      key: 'ownershipRelation',
      component: <Input placeholder=""/>,
    }, {
      label: '监证号',//TODO
      key: 'superviseNo',
      component: <Input placeholder=""/>,
    }, {
      label: '权证号',
      key: 'ownershipNo',
      component: <Input placeholder=""/>,
    }, {
      label: '不动产权第号',
      key: 'estateOwnershipNo',
      component: <Input placeholder=""/>,
    }, {
      label: '不动产单元号',
      key: 'estateUnitNo',
      component: <Input placeholder=""/>,
    }, {
      label: '业务号',
      key: 'businessNo',
      component: <Input placeholder=""/>,
    }, {
      label: '规划用途',
      key: 'purpose',
      component: <Input placeholder=""/>,
    }, {
      label: '土地性质',
      key: 'land',
      component: <Input placeholder=""/>,
    }, {
      label: '建筑面积',
      key: 'area',
      component: <Input placeholder=""/>,
    }, {
      label: '登记时间',//TODO
      key: 'registerTime',
      component: <Input placeholder=""/>,
    }, {
      label: '是否按揭',
      key: 'isMortgage',
      component: <Input placeholder=""/>,
    }, {
      label: '按揭银行',
      key: 'mortgageBank',
      component: <Input placeholder=""/>,
    }, {
      label: '按揭金额',
      key: 'mortgageAmount',
      component: <Input placeholder=""/>,
    }, {
      label: '按揭时间',
      key: 'mortgageTime',
      component: <Input placeholder=""/>,
    }, {
      label: '首付金额',
      key: 'downPayment',
      component: <Input placeholder=""/>,
    }, {
      label: '月供金额',
      key: 'monthlyPayments',
      component: <Input placeholder=""/>,
    }, {
      label: '是否抵押',
      key: 'isPledge',
      component: <Input placeholder=""/>,
    }, {
      label: '抵押机构',
      key: 'pledgeOrganization',
      component: <Input placeholder=""/>,
    }, {
      label: '抵押金额',
      key: 'pledgeAmount',
      component: <Input placeholder=""/>,
    }, {
      label: '抵押时间',
      key: 'pledgeTime',
      component: <Input placeholder=""/>,
    }, {
      label: '抵押周期',
      key: 'pledgePperiod',
      component: <Input placeholder=""/>,
    }, {
      label: '到期时间',
      key: 'pledgeDeadline',
      component: <Input placeholder=""/>,
    }, {
      label: '房屋性质',//TODO
      key: 'property',
      component: <Input placeholder=""/>,
    }, {
      label: '备注',
      key: 'remark',
      span: 24,
      formItemLayout: {
        labelCol: {
          xs: {span: 24},
          sm: {span: 4},
        },
        wrapperCol: {
          xs: {span: 24},
          sm: {span: 16},
        },
      },
      component: <Input.TextArea placeholder=""/>
    }]
  }

  componentDidMount() {
    let {customerId} = this.state;
    if (!customerId) {
      message.error('用户信息有误');
    }
    this.getList();
  }

  render() {
    let {getFieldDecorator} = this.props.form;
    let {editObj} = this.state;
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 8},
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
      },
    };

    return (<div>
      <div className={styles.tableListOperator}>
        <Button icon="plus" type="primary" onClick={this.showDetail.bind(this, this.defaultData)}>
          新增住宅
        </Button>
      </div>
      <div style={{marginTop: "20px"}}>
        <Table rowKey="houseId"
               loading={this.props.house.loading}
               columns={this.columns}
               dataSource={this.props.house.list}
        />
      </div>
      <Modal title={editObj.houseId ? "编辑住宅信息" : "新增住宅信息"}
             onOk={this.submit}
             onCancel={this.handleCancel}
             visible={this.state.isvisible}
             width={700}
      >
        <Form className={styles.modal}>
          <Row gutter={24}>
            {this.getFormCell().map((item, index) => {
              return <Col span={item.span || 12} key={item.key}>
                <FormItem label={item.label} {...(item.formItemLayout || formItemLayout)}>
                  {getFieldDecorator(item.key, {
                    initialValue: editObj[item.key] || '',
                    ...item.field
                  })(
                    item.component
                  )}
                </FormItem>
              </Col>
            })}
          </Row>
        </Form>
      </Modal>

    </div>)
  }
}
