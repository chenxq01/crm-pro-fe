/**
 * Created by chenxq on 2017/12/25.
 */
import React, {PureComponent} from "react";
import {connect} from "dva";
import {
  Button,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  message,
  Modal,
  Popconfirm,
  Radio,
  Row,
  Select,
  Table
} from "antd";
import styles from "../../routes/TableList.less";
import request from '../../utils/request';
import List from './CreditCardsList';
import {reg} from '../../utils/CheckList';
const FormItem = Form.Item;
const {Option} = Select;
const {RangePicker} = DatePicker;
const RadioGroup = Radio.Group;
@connect(state => ({
  job: state.job
}))
@Form.create()
export default class extends PureComponent {
  state={
    detail:{},
    list:[],
    flag:false
  }
  handleSubmit=(e)=>{
    let _this=this;
    let {detail} = this.state;
    let {customerId} = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let params=Object.assign({},detail,values);
        let url=detail.cardId ? `/v1/customer/creditCard/${customerId}/edit` : `/v1/customer/creditCard/${customerId}/add`;
        return request(url,{
          method:'POST',
          body:{
            ...params
          }
        }).then(jsonData=>{
          if(jsonData.result){
            message.success('操作成功');
            _this.getCardDetail();
          }else{
            message.error(jsonData.msg || '操作失败');
          }
          return jsonData;
        })

      }
    })
  }
  updateList=(list)=>{
    let _this=this;
    let {customerId} = this.props;
    let arr = list.map(item=>({
      cardNo:item.cardNo,
      credit:item.credit,
      grantTime:item.grantTime
    }));
    let {detail}= this.state;
    let params={...detail};
    params.cards=arr;
    let url=detail.cardId ? `/v1/customer/creditCard/${customerId}/edit` : `/v1/customer/creditCard/${customerId}/add` ;
    return request(url,{
      method:'POST',
      body:{
        ...params
      }
    }).then(jsonData=>{
      if(jsonData.result){
        message.success('操作成功');
        _this.getCardDetail();
      }else{
        message.error(jsonData.msg || '操作失败');
      }
      return jsonData;
    })
  }
  getCardDetail=()=>{
    let {customerId} = this.props;
    let _this=this;
    request(`/v1/customer/creditCard/${customerId}/detail`).then(jsonData=>{
      if(jsonData.result){
        let data = jsonData.data || {};
        _this.setState({
          detail:data,
          list:data.cards || [],
          flag:true
        })
      }else{
        message.error(jsonData.msg || '获取信用卡信息失败');
      }
    })
  }
  componentDidMount(){
    this.getCardDetail();
  }
  render(){
    let {getFieldDecorator} =  this.props.form;
    let detail= this.state.detail;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    return (<div>
      <Form onSubmit={this.handleSubmit}>
        <Row gutter={24}>
          <Col span={8}>
            <FormItem label='芝麻信用分' {...formItemLayout}>
              {getFieldDecorator('sesame',{
                initialValue:detail.sesame * 1,

              })(
                <RadioGroup>
                  <Radio value={1}>有</Radio>
                  <Radio value={0}>无</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label='芝麻信用分' {...formItemLayout}>
              {getFieldDecorator('sesameScore',{
                initialValue:detail.sesameScore,
                rules:[{pattern:reg.intNumber,message:'输入格式不正确'}]
              })(
                <Input replaceholder=""/>
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label='发卡总数' {...formItemLayout}>
              {getFieldDecorator('cardNum',{
                initialValue:detail.cardNum,
                rules:[{pattern:reg.intNumber,message:'输入格式不正确'}]
              })(
                <Input replaceholder=""/>
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label='机构数' {...formItemLayout}>
              {getFieldDecorator('organizationNum',{
                initialValue:detail.organizationNum,
                rules:[{pattern:reg.intNumber,message:'输入格式不正确'}]
              })(
                <Input replaceholder=""/>
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label='授信总额' {...formItemLayout}>
              {getFieldDecorator('grantingAmount',{
                initialValue:detail.grantingAmount,
                rules:[{pattern:reg.money,message:'输入格式不正确'}]
              })(
                <Input replaceholder=""/>
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label='已使用额度' {...formItemLayout}>
              {getFieldDecorator('usedAmount',{
                initialValue:detail.usedAmount,
                rules:[{pattern:reg.money,message:'输入格式不正确'}]
              })(
                <Input replaceholder=""/>
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label='近6个月平均使用额' {...formItemLayout}>
              {getFieldDecorator('averageAmount',{
                initialValue:detail.averageAmount,
                rules:[{pattern:reg.money,message:'输入格式不正确'}]
              })(
                <Input replaceholder=""/>
              )}
            </FormItem>
          </Col>
          <Col span={8}>
            <FormItem label='单家最高授信' {...formItemLayout}>
              {getFieldDecorator('topAmount',{
                initialValue:detail.topAmount,
                rules:[{pattern:reg.money,message:'输入格式不正确'}]
              })(
                <Input replaceholder=""/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Button type='primary' htmlType='submit'>保存以上信息</Button>
        </Row>
      </Form>
      {this.state.flag && <List list={this.state.list} style={{marginTop:20}} updateList={this.updateList}></List>}
      </div>)
  }
}
