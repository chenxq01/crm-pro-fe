/**
 * Created by chenxq on 2018/1/1.
 */
import React, {Component} from 'react';
import { connect} from 'dva';
import {Form ,Input ,Button ,Row, Col ,Select,message,Radio} from 'antd';
import {reg} from '../../utils/CheckList';
import styles from './index.less';
import {channelList,contactsRelation} from '../../routes/defaultData';
import {findObjByKey ,convertLongToString} from '../../utils/index'
import request from '../../utils/request'
const FormItem=Form.Item;
const { Option } = Select;
const RadioGroup = Radio.Group;


@connect(state=>({
  customer:state.customer
}))
@Form.create()
export default class extends Component{
  state={
    detail:{},
    key:Math.random()
  }

  //获取信息
  getDetailInfo=()=>{
    let {customerId} = this.props;
    let _this=this;
    request(`/v1/customer/credit/${customerId}/detail`).then(jsonData=>{
      console.log('get jsonData',jsonData);
      if(jsonData.result){
        let data=jsonData.data || {}
        _this.setState({
          detail:data,
        })
      }else{
        message.error(jsonData.msg || '获取征信错误');
      }
    })
  }

  handleSubmit=(e)=>{
    let _this=this;
    e.preventDefault();
    let {detail} = this.state;
    let {customerId} = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let url=`/v1/customer/credit/${customerId}/add`;
        if(detail.creditId){
          url=`/v1/customer/credit/{customerId}/edit`;
        }
        request(url,{
          method:'POST',
          body:{
            ...values,
            creditId:detail.creditId
          }
        }).then(jsonData=>{
          if(jsonData.result){
            message.success('保存成功',0.8,()=>{
              let data=jsonData.data || values ;
              _this.setState({
                detail:data,
              })

            })

          }else{
            message.error(jsonData.msg || '保存失败');
          }
        })
      }
    });
  }

  onCancel=()=>{
    let detail = this.state.detail;
    let obj={
      allow:detail.allow * 1 ,
      hasLoan:detail.hasLoan * 1,
      netLoan:detail.netLoan * 1,
      doneRemaek:detail.doneRemaek || '',
      loanRemark:detail.loanRemark || '',
      remark:detail.remark || '',

    }
    this.props.form.setFieldsValue({
      ...obj
    });
  }
  componentDidMount(){
    this.getDetailInfo();
  }

  render(){
    let {getFieldDecorator} =  this.props.form;
    let detail= this.state.detail;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const textAreaLayout={
      labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    }
    return (<Form onSubmit={this.handleSubmit}>
      <Row gutter={24}>
        <Col span={8}>
          <FormItem label='客户可做贷款' {...formItemLayout}>
            {getFieldDecorator('allow',{
              initialValue:detail.allow*1,
              rules:[{
                required:true,
                message:'请选择客户可做贷款'
              }]
            })(
              <RadioGroup>
                <Radio value={1}>允许</Radio>
                <Radio value={0}>不允许</Radio>
              </RadioGroup>
            )}
          </FormItem>
        </Col>
        <Col span={8}>
          <FormItem label='征信贷款情况' {...formItemLayout}>
            {getFieldDecorator('hasLoan',{
              initialValue:detail.hasLoan *1,
              rules:[{
                required:true,
                message:'请选择征信贷款情况'
              }]
            })(
              <RadioGroup>
                <Radio value={0}>无</Radio>
                <Radio value={1}>有</Radio>
              </RadioGroup>
            )}
          </FormItem>
        </Col>
        <Col span={8}>
          <FormItem label='网贷情况' {...formItemLayout}>
            {getFieldDecorator('netLoan',{
              initialValue:detail.netLoan *1,
              rules:[{
                required:true,
                message:'请选择网贷情况'
              }]
            })(
              <RadioGroup>
                <Radio value={0}>无</Radio>
                <Radio value={1}>有</Radio>
              </RadioGroup>
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label='征信上贷款信息' {...textAreaLayout}>
            {getFieldDecorator('info',{
              initialValue:detail.info || '',
            })(
              <Input.TextArea replaceholder="征信上贷款信息"/>
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label='已做备注' {...textAreaLayout}>
            {getFieldDecorator('doneRemaek',{
              initialValue:detail.doneRemaek || '',
            })(
              <Input.TextArea replaceholder="请输入已做备注"/>
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label='可做贷款备注' {...textAreaLayout}>
            {getFieldDecorator('loanRemark',{
              initialValue:detail.loanRemark || '',
            })(
              <Input.TextArea replaceholder="请输入已做备注"/>
            )}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label='备注' {...textAreaLayout}>
            {getFieldDecorator('remark',{
              initialValue:detail.remark || '',
            })(
              <Input.TextArea replaceholder="备注"/>
            )}
          </FormItem>
        </Col>

      </Row>
      <Row className="ant-row-flex ant-row-flex-center">
        <Button type="primary" htmlType="submit">保存</Button>
        <Button style={{marginLeft:20}} onClick={this.onCancel}>取消</Button>
      </Row>

    </Form>)
  }
}
