/**
 * Created by chenxq on 2017/12/18.
 */
import React, {Component} from 'react';
import { connect} from 'dva';
import {Card ,Form ,Input ,Button ,Row, Col ,Select,message} from 'antd';
import {reg} from '../../utils/CheckList';
import styles from './index.less';
import {channelList,contactsRelation} from '../../routes/defaultData';
import {findObjByKey ,convertLongToString} from '../../utils/index'
import EditForm from './EditForm'; // 征信信息和 其他联系信息
import Enterprise from './Enterprise'; //企业
import Job from './Job'; //职业
import Car from './Car'; //职业
import House from './House'; //职业
import CreditCard from './CreditCard'; //职业
import BaseInfo from './BaseInfo';  // 用户基本信息
import Credit from './Credit'; // 征信
const FormItem=Form.Item;
const { Option } = Select;
const tabList=[{
  key:'info',
  tab:'基本信息',
},{
  key:'job',
  tab:'职业情况',
},{
  key:'contacts',
  tab:'其他联系方式',
},{
  key:'house',
  tab:'房产信息',
},{
  key:'car',
  tab:'车辆信息',
},{
  key:'enterprises',
  tab:'企业信息',
},{
  key:'creditCard',
  tab:'信用卡信息',
},{
  key:'credit',
  tab:'征信信息',
}];

@connect(state=>({
  customer:state.customer
}))
export default class Info extends Component{
  constructor(props){
    super(props);
    this.state={
      tabKey:'info'
    }
  }
  onChangeTab=(key)=>{
    this.setState({
      tabKey:key
    })
  }

  getContacts=()=>{
    let {curInfo} = this.props.customer;
    let customerId=curInfo.id;
    let columns=[{
      title:'联系人姓名',
      dataIndex:'contactsName',
      rules:[{
        required:true,
        message:'请输入联系人姓名'
      }]
    },{
      title:'所在地',
      dataIndex:'contactsAddr',
    },{
      title:'关系',
      dataIndex:'contactsRelation',
      type:'select',
      data:contactsRelation,
    },{
      title:'电话',
      dataIndex:'contactsPhone',
      rules:[{
        pattern:reg.mobile,
        message:'手机号格式不正确'
      }]
    },{
      title:'公司',
      dataIndex:'contactsCompany'
    },{
      title:'备注',
      dataIndex:'contactsRemark'
    }];
    return <EditForm key="contacts"
                     title="新增联系人"
                     defaultObj={{
                       contactsName:'',
                       contactsAddr:'',
                       contactsRelation:'6',
                       contactsPhone:'',
                       contactsCompany:'',
                       contactsRemark:'',
                     }}
                     columns={columns}
                     keyName="contactsId"
                     get={`/v1/customer/contacts/${customerId}/list`}
                     add={`/v1/customer/contacts/${customerId}/add`}
                     edit={`/v1/customer/contacts/${customerId}/edit`}
                     remove={`/v1/customer/contacts/${customerId}/remove`}
    />
    /*info: ()=><BaseInfo></BaseInfo>,
    jobs:<div>职业信息</div>,
    contacts:,
    houses: <div>房产信息</div>,
    cars: <div>车辆信息</div>,
    enterprises: <div>企业信息</div>,
    creditCard: <div>信用卡信息</div>,
    credit: <div>征信信息</div>*/
  }

  getCredit=()=>{
    let {curInfo} = this.props.customer;
    let customerId=curInfo.id;
    let columns=[{
      title:'征信信息',
      dataIndex:'info',
      rules:[{
        required:true,
        message:'请输入征信信息'
      }]
    },{
      title:'有无征信',
      dataIndex:'hasLoan',
      type:'radio',
      data:[{key:0,name:'无征信贷款'},{key:1,name:'有征信贷款'}]
    },{
      title:'网贷情况',
      dataIndex:'netLoan',
      type:'radio',
      data:[{key:0,name:'无网贷'},{key:1,name:'有网贷'}]
    },{
      title:'客户贷款情况',
      dataIndex:'allow',
      type:'radio',
      data:[{key:0,name:'不允许贷款'},{key:1,name:'允许贷款'}]
    },{
      title:'备注',
      dataIndex:'remark',
    }];
    return <EditForm key="credit"
                     title="新增征信信息"
                     defaultObj={{
                       info:'',
                       hasLoan:0,
                       netLoan:0,
                       allow:0,
                       contactsCompany:'',
                       contactsRemark:'',
                       remark:''
                     }}
                     columns={columns}
                     keyName="creditId"
                     get={`/v1/customer/credit/${customerId}/list`}
                     add={`/v1/customer/credit/${customerId}/add`}
                     edit={`/v1/customer/credit/${customerId}/edit`}
                     remove={`/v1/customer/credit/${customerId}/remove`}
    />
  }

  render(){
    let {tabKey} = this.state;
    let {curInfo} = this.props.customer;
    let customerId=curInfo.id;
    return (<Card
      bordered={false}
      tabList={tabList}
      onTabChange={this.onChangeTab}
    >
      {'info' == tabKey && <BaseInfo></BaseInfo>}
      {'credit'== tabKey && <Credit customerId={customerId} />}
      {'contacts'===tabKey && this.getContacts()}
      {'enterprises'=== tabKey && <Enterprise customerId={customerId} />}
      {'job'=== tabKey && <Job customerId={customerId} />}
      {'car'=== tabKey && <Car customerId={customerId} />}
      {'house'=== tabKey && <House customerId={customerId} />}
      {'creditCard'=== tabKey && <CreditCard customerId={customerId} />}
    </Card>)
  }
}



