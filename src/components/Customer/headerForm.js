/**
 * Created by chenxq on 2017/12/15.
 */
import React ,{ Component} from 'react';
import {Button,Select,Input} from 'antd';
const {Option} = Select;
const HeaderForm=(props)=>{
  const { getFieldDecorator } = props.form;
  return (<Form onSubmit={this.handleSearch} layout="inline">
    <Row gutter={{ md: 24, lg: 15, xl: 15 }}>
      <Col md={6}>
        <FormItem label="产品名称" >
          {getFieldDecorator('searchName')(
            <Input placeholder="请输入产品名称" />
          )}
        </FormItem>
      </Col>
      <Col md={6}>
        <FormItem label="合同模板">
          {getFieldDecorator('searchTemplate')(
            <Input placeholder="请输入合同模板" />
          )}
        </FormItem>
      </Col>
      <Col md={5}>
        <FormItem label="产品状态">
          {getFieldDecorator('searchState')(
            <Select placeholder="请选择" style={{ width: '100%' }}>
              <Option value={null}>请选择</Option>
              {
                productStatus.map(item=><Option key={item.key} value={item.key}>{item.name}</Option>)
              }
            </Select>
          )}
        </FormItem>
      </Col>
      <Col md={5} sm={10}>
        <FormItem label="产品类型">
          {getFieldDecorator('searchType')(
            <Select placeholder="请选择" style={{ width: '100%' }}>
              <Option value={null}>请选择</Option>
              {
                productType.map(item=><Option key={item.key} value={item.key}>{item.name}</Option>)
              }
            </Select>
          )}
        </FormItem>
      </Col>
      <Col md={1} sm={24}>
        <FormItem >
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">查询</Button>
            </span>
        </FormItem>
      </Col>
    </Row>
  </Form>)
}

const searchArr=[{key:'customerName',title:'客户姓名'},{key:'phone',title:'手机号码'},{key:'grade',title:'等级'}];


export class SearchInput extends Component{
  constructor(props){
    super(props);
    let arr=this.props.searchArr || searchArr;
    this.state={
      searchArr:arr,
      key:this.props.key ||  arr[0].key,
      keyword:''
    }
  }
  hanldeSearch=()=>{
    let {key,keyword} = this.state;
    this.props.search && this.props.search.call(null,{
      searchKey:key,
      searchVal:keyword
    })
  }
  render(){
    let {searchArr,key} = this.state;
    return (<div className="flex-row">
      <Input  addonBefore={
        <Select value={key} style={{ width: 100 }} onChange={(value)=>{
          this.setState({
            key:value
          })
        }
        }>{
          searchArr.map(item=>(<Option key={item.key} value={item.key}>{item.title}</Option>))
        }
        </Select>
      }  placeholder={this.props.placeholder || "关键字查询"}
              onChange={(e)=>{
                this.setState({
                  keyword:e.target.value
                })
              }}
             onPressEnter={this.hanldeSearch} />
      <Button style={{marginLeft:15}} type="primary" onClick={this.hanldeSearch}>{this.props.searchBtnText || '查询'}</Button>
    </div>)
  }
}
