/**
 * Created by chenxq on 2017/12/18.
 */
import React, {Component} from "react";
import {connect} from "dva";
import {Button, Card, message, Table} from "antd";
import Info from "../ClientDetail/Info";
import {convertLongToString, findObjByKey} from "../../utils/index";
import {planType} from "./defaultData";
import moment from "moment";

@connect(state => ({
  customer: state.customer,
  plan: state.plan
}))

export default class Plan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customerId: this.props.customerId || 0,
      editObj: {},
      planType: "PHONE",
      planContent: "",
      planAlert: null,
    }
  }

  columns = [{
    dataIndex: "planAlert",
    title: "计划时间",
    render: (time) => convertLongToString(time)
  }, {
    dataIndex: "planType",
    title: "计划类型",
    render: (type) => {
      let obj = findObjByKey(planType, type);
      return <span>{obj ? obj.name : ""}</span>
    }
  }, {
    dataIndex: "planContent",
    title: "跟进内容",
  }, {
    dataIndex: "operation",
    title: "操作",
    render: (op, record) => {
      //<a onClick={this.removeOneRecord.bind(this,record)}>删除</a>
      return record.state === "TODO" ? <a onClick={this.planExecute.bind(this, record.id)}>执行计划</a> : <p>完成</p>;
    }

  }];
  removeOneRecord = (record) => {
    let {dispatch} = this.props;
    // dispatch(AjaxAction.planRemove(record.id))
  };

  planExecute = (id) => {
    console.log(id)
    this.props.dispatch({
      type: 'plan/fetchRemove',
      payload: {
        planId: id,
      }
    }).then(jsonData => {
      if (jsonData.result) {
        message.success('保存成功', 0.8, () => {
          this.setState({});
          this.getPlanList();
        })
      } else {
        message.error(jsonData.msg || '操作失败')
      }
    })
  };

  addOneRecord = () => {
    let {curInfo} = this.props.customer;
    let customerId = curInfo.id;
    let {editObj} = this.state;

    if (!this.state.planAlert) {
      message.warn("请填写计划时间");
      return;
    }

    let params = Object.assign({}, this.state, {
      customerId: customerId,
      planAlert: moment(this.state.planAlert).toDate().getTime(),
    });

    let type = editObj.planId ? 'plan/fetchEdit' : 'plan/fetchAdd';
    params.planId = editObj.planId;
    this.props.dispatch({
      type,
      payload: {
        customerId: customerId,
        params: params
      }
    }).then(jsonData => {
      if (jsonData.result) {
        message.success('保存成功', 0.8, () => {
          this.setState({});
          this.getPlanList();
        })
      } else {
        message.error(jsonData.msg || '操作失败')
      }
    })
  };
  getPlanList = (cId) => {
    let {curInfo} = this.props.customer;
    let customerId=curInfo.id;
    this.props.dispatch({
      type: 'plan/fetchList',
      payload: customerId
    })
  };

  componentWillReceiveProps(nextProps) {
    // let {currentCustomerId} = this.props;
    // let {curInfo} = this.props.customer;
    // let customerId=curInfo.id;
    // if (nextProps.currentCustomerId && nextProps.currentCustomerId !== currentCustomerId) {
    //   this.getPlanList(nextProps.currentCustomerId);
    // }
  }

  componentDidMount() {
    this.getPlanList();
  }

  render() {
    let {state} = this;
    return (
      <Card
        title="回访计划"
        bordered={false}
      >
        <div style={{paddingTop: 10}}>

          <div className="dp-f a-c j-c">
            <Info required label="计划时间" type="datetime" value={state.planAlert} onChange={(value) => {
              this.setState({planAlert: value})
            }}/>
          </div>
          <div className="dp-f a-c j-c">
            <Info label="计划方式" type="radio" data={planType} id="planType" value={state.planType} onChange={(value) => {
              this.setState({planType: value})
            }}/>
          </div>
          <div className="dp-f a-c j-c">
            <Info label="计划内容" id="planContent" value={state.planContent} onChange={(value) => {
              this.setState({planContent: value})
            }}/>
          </div>

          <div><Button type="primary" onClick={this.addOneRecord}>新增计划</Button></div>

          <div style={{paddingTop: 10}}>
            <h1 style={{textAlign: "center", marginBottom: 10, fontSize: 20}}>预约计划</h1>
            <Table columns={this.columns} dataSource={this.props.plan.list} rowKey="id" size="small" pagination={false}/>
          </div>
        </div>
      </Card>

    )
  }
}



