/**
 * Created by coolguy on 2017/4/17.
 */
export const callState = [{
    key:"START",
    name:"发起"
},{
    key:"ACCESS",
    name:"接通"
},{
    key:"LOST",
    name:"未接通"
},{
    key:"DURATION",
    name:"通话时长"
},]
export const contactsRelation = [{
   key:"1",
    name:"配偶(直系)"
}, {
    key:"2",
    name:"父母(直系)"
},{
    key:"3",
    name:"子女(直系)"
},{
    key:"4",
    name:"兄妹(直系)"
},{
    key:"5",
    name:"同事"
},{
    key:"6",
    name:"其他"
},{
    key:"7",
    name:"本人"
},{
    key:"8",
    name:"其他"
}];
export const followType = [{
    key: "PHONE",
    name: "电话"
},{
    key: "SMS",
    name: "短信",
},  {
    key: "INVITE",
    name: "邀请"
},{
    key: "VISIT",
    name: "拜访"
}, {
    key: "POST_LOAN",
    name: "贷后"
}, {
    key: "OTHERS",
    name: "其他"
}];
export const planType = followType;



export const marriage = [{
    key:"1",
    name:"未婚"
}, {
    key:"2",
    name:"已婚"
}, {
    key:"3",
    name:"离婚"
}, {
    key:"4",
    name:"丧偶"
}, {
    key:"5",
    name:"再婚"
}];
export const education = [{
    key:"1",
    name:"小学"
}, {
    key:"2",
    name:"初中"
}, {
    key:"3",
    name:"高中"
}, {
    key:"4",
    name:"中专"
}, {
    key:"5",
    name:"大专"
}, {
    key:"6",
    name:"本科"
}, {
    key:"7",
    name:"硕士"
}, {
    key:"8",
    name:"博士"
}, {
    key:"9",
    name:"其他"
}];
export const listParams = {
    "dir": "DESC",
    "current": 1,//保持antd的风格
    "pageSize": 10,
    "sortKey": "createTime",
};