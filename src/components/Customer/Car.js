/**
 * Created by chenxq on 2017/12/25.
 */
import React, {PureComponent} from "react";
import {connect} from "dva";
import {
  Button,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  message,
  Modal,
  Popconfirm,
  Radio,
  Row,
  Select,
  Table
} from "antd";
import styles from "../../routes/TableList.less";
const FormItem = Form.Item;
const {Option} = Select;
const {RangePicker} = DatePicker;
const RadioGroup = Radio.Group;
@connect(state => ({
  car: state.car
}))
@Form.create()
export default class extends PureComponent {
  state = {
    customerId: this.props.customerId || 0,
    editObj: {},
    isvisible: false,
    loading:false,
  };
  defaultData = {};
  columns = [{
    title: '车辆品牌',
    dataIndex: 'brand'
  }, {
    title: '车牌号码',
    dataIndex: 'no'
  }, {
    title: '排量',
    dataIndex: 'displacement'
  }, {
    title: '裸车价',
    dataIndex: 'nakedPrica'
  }, {
    title: '购买时间',
    dataIndex: 'purchaseTime'
  }, {
    title: '是否按揭',
    dataIndex: 'isMortgage'
  }, {
    title: '备注',
    dataIndex: 'remark'
  }, {
    title: '操作',
    dataIndex: 'carId',
    render: (carId, record) => {
      return (<div className="operations">
        <a onClick={this.showDetail.bind(this, record)}>详情</a>
        <Divider type="vertical"/>
        <Popconfirm title="确认要删除该车辆信息?" onConfirm={this.remove.bind(this, record.carId)}><a>删除</a></Popconfirm>
      </div>)
    }
  }];
  showDetail = (editObj) => {
    this.setState({
      isvisible: true,
      editObj: {
        ...editObj
      }
    })
  };
  handleCancel = () => {
    this.setState({
      isvisible: false
    })
  };
  remove = (id) => {
    let _this = this;
    this.props.dispatch({
      type: 'car/fetchRemove',
      payload: {
        customerId: this.state.customerId,
        ids: [id]
      }
    }).then(jsonData => {
      if (jsonData.result) {
        message.success('删除成功', 0.8, () => {
          _this.getList();
        })
      } else {
        message.error(jsonData.msg || '删除失败')
      }
    })
  };
  submit = () => {
    let _this = this;
    let {editObj} = this.state;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('values', values);
        let type = editObj.carId ? 'car/fetchEdit' : 'car/fetchAdd';
        values.carId = editObj.carId;
        _this.props.dispatch({
          type,
          payload: {
            customerId: this.state.customerId,
            params: values
          }
        }).then(jsonData => {
          if (jsonData.result) {
            message.success('保存成功', 0.8, () => {
              _this.setState({
                isvisible: false
              });
              _this.getList();
            })
          } else {
            message.error(jsonData.msg || '操作失败')
          }
        })
      }
    })
  };
  getList = () => {
    let {customerId} = this.state;
    this.props.dispatch({
      type: 'car/fetchList',
      payload: customerId
    })
  };
  getFormCell = () => {
    return [{
      label: '车辆品牌',
      key: 'brand',
      component: <Input placeholder=""/>,
      field: {rules: [{required: true, message: '请输入车辆品牌'}]},
      text: ''
    }, {
      label: '车牌号码',
      key: 'no',
      component: <Input placeholder=""/>,
      field: {rules: [{required: true, message: '请输入车牌号码'}]},
      text: ''
    }, {
      label: '排量',
      key: 'industryProperty',
      component: <Input placeholder=""/>,
    }, {
      label: '裸车价',
      key: 'nakedPrica',
      component: <Input placeholder=""/>,
    }, {
      label: '首次登记时间',
      key: 'registerTime',
      component: <Input placeholder=""/>,
    }, {
      label: '购买时间',
      key: 'purchaseTime',
      component: <Input placeholder=""/>,
    }, {
      label: '当前公里数',//TODO
      key: 'mileage',
      component: <Input placeholder=""/>,
    }, {
      label: '是否按揭',
      key: 'isMortgage',
      component: <Input placeholder=""/>,
    }, {
      label: '货款机构',
      key: 'loanOrganization',
      component: <Input placeholder=""/>,
    }, {
      label: '其他备注',
      key: 'otherRemark',
      component: <Input placeholder=""/>,
    }, {
      label: '贷款方式',
      key: 'loanWay',
      component: <Input placeholder=""/>,
    }, {
      label: '货款时间',
      key: 'loanTime',
      component: <Input placeholder=""/>,
    }, {
      label: '货款年限',
      key: 'loanYear',
      component: <Input placeholder=""/>,
    }, {
      label: '货款金额',
      key: 'loanAmount',
      component: <Input placeholder=""/>,
    }, {
      label: '月供金额',//TODO
      key: 'loanPayAmount',
      component: <Input placeholder=""/>,
    }, {
      label: '登记人',
      key: 'registrant',
      component: <Input placeholder=""/>,
    }, {
      label: '登记地',
      key: 'registration',
      component: <Input placeholder=""/>,
    },  {
      label: '备注',
      key: 'remark',
      span: 24,
      formItemLayout: {
        labelCol: {
          xs: {span: 24},
          sm: {span: 4},
        },
        wrapperCol: {
          xs: {span: 24},
          sm: {span: 16},
        },
      },
      component: <Input.TextArea placeholder=""/>
    }]
  }

  componentDidMount() {
    let {customerId} = this.state;
    if (!customerId) {
      message.error('用户信息有误');
    }
    this.getList();
  }

  render() {
    let {getFieldDecorator} = this.props.form;
    let {editObj} = this.state;
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 8},
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
      },
    };

    return (<div>
      <div className={styles.tableListOperator}>
        <Button icon="plus" type="primary" onClick={this.showDetail.bind(this, this.defaultData)}>
          新增车辆
        </Button>
      </div>
      <div style={{marginTop: "20px"}}>
        <Table rowKey="carId"
               loading={this.props.car.loading}
               columns={this.columns}
               dataSource={this.props.car.list}
        />
      </div>
      <Modal title={editObj.carId ? "编辑车辆信息" : "新增车辆信息"}
             onOk={this.submit}
             onCancel={this.handleCancel}
             visible={this.state.isvisible}
             width={700}
      >
        <Form className={styles.modal}>
          <Row gutter={24}>
            {this.getFormCell().map((item, index) => {
              return <Col span={item.span || 12} key={item.key}>
                <FormItem label={item.label} {...(item.formItemLayout || formItemLayout)}>
                  {getFieldDecorator(item.key, {
                    initialValue: editObj[item.key] || '',
                    ...item.field
                  })(
                    item.component
                  )}
                </FormItem>
              </Col>
            })}
          </Row>
        </Form>
      </Modal>

    </div>)
  }
}
