/**
 * Created by chenxq on 2017/12/18.
 */
import React, {Component} from "react";
import {connect} from "dva";
import {Button, Card, message, Popconfirm, Table} from "antd";
import Info from "../ClientDetail/Info";
import {convertLongToString, findObjByKey} from "../../utils/index";
import {followType} from "./defaultData";

@connect(state => ({
  customer: state.customer,
  follow: state.follow
}))

export default class Follow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editObj: {},
      followType: "PHONE",
      followContent: "",
    }
  }


  columns = [{
    dataIndex: "createTime",
    title: "跟进时间",
    render: (time) => convertLongToString(time)
  }, {
    dataIndex: "followType",
    title: "跟进方式",
    render: (type) => {
      return <span>{findObjByKey(followType, type).name}</span>
    }
  }, {
    dataIndex: "followContent",
    title: "跟进内容",
  }, {
    dataIndex: "followerName",
    title: "跟进人",
  }/*,{
   dataIndex:"operation",
   title:"操作",
   render: (op,record)=><a onClick={this.removeOneRecord.bind(this,record)}>删除</a>
   }*/];
  // data = [{
  //     key:"1",
  //     followContent:"跟进内容哦",
  //     follower:"丁丁",
  // }];
  removeOneRecord = (record) => {
    let {dispatch} = this.props;
    // dispatch(AjaxAction.planFollowRemove(record.id))
  };
  addOneRecord = (shortcut) => {
    let {curInfo} = this.props.customer;
    let customerId = curInfo.id;
    let {editObj} = this.state;
    let params = Object.assign({}, this.state, {
      customerId: customerId
    });

    //几个快捷键的操作
    if (shortcut && typeof shortcut === "string") {//必须判断string，否则可能是个很奇葩的对象
      params.shortcut = shortcut;
      delete params.followContent;
      delete params.followType;
    } else {
      if (!params.followContent) {
        message.warn("内容不能为空");
        return;
      }
    }
    let type = editObj.followId ? 'follow/fetchEdit' : 'follow/fetchAdd';
    params.followId = editObj.followId;
    this.props.dispatch({
      type,
      payload: {
        customerId: customerId,
        params: params
      }
    }).then(jsonData => {
      if (jsonData.result) {
        message.success('保存成功', 0.8, () => {
          this.setState({});
          this.getPlanFollowList();
        })
      } else {
        message.error(jsonData.msg || '操作失败')
      }
    })
  };

  getPlanFollowList = (cId) => {
    let {curInfo} = this.props.customer;
    let customerId = curInfo.id;
    this.props.dispatch({
      type: 'follow/fetchList',
      payload: customerId
    })
  };


  shortCutOperation = (shortcut) => {
    this.addOneRecord(shortcut);
  };

  componentWillReceiveProps(nextProps) {
    // let {currentCustomerId} = this.props;
    // //console.log(currentCustomerId,nextProps.currentCustomerId);
    // if(nextProps.currentCustomerId && nextProps.currentCustomerId !== currentCustomerId){
    //   this.getPlanFollowList(nextProps.currentCustomerId);
    // }
  }

  componentDidMount() {
    this.getPlanFollowList();
  }

  render() {
    let {followList} = this.props;
    let {state} = this;
    return (
      <Card
        title="跟进记录"
        bordered={false}
      >
        <div style={{paddingTop: 10}}>
          <div className="dp-f a-c j-c">
            <div className="flex1 dp-f a-c fastOperation">
              <div className="label">快捷操作</div>
              <div className="flex1">
                <Popconfirm onConfirm={this.addOneRecord.bind(this, "halt")}
                            title={"标记为\"停机\"的用户将被加入到无效列表中，是否还需要继续将其标记为\"停机\"?"}><Button
                  type="danger">停机</Button></Popconfirm>
                <Popconfirm onConfirm={this.addOneRecord.bind(this, "empty")}
                            title={"标记为\"空号\"的用户将被加入到无效列表中，是否还需要继续将其标记为\"空号\"?"}><Button
                  type="danger">空号</Button></Popconfirm>
                <Popconfirm onConfirm={this.addOneRecord.bind(this, "shutdown")}
                            title={"标记为\"关机\"的用户将被加入到无效列表中，是否还需要继续将其标记为\"空号\"?"}><Button
                  type="danger">关机</Button></Popconfirm>
                <Button onClick={this.addOneRecord.bind(this, "online")}>通话中</Button>
                <Button onClick={this.addOneRecord.bind(this, "miss")}>无人接听</Button>
                <Button onClick={this.addOneRecord.bind(this, "reject")}>无需求</Button>
              </div>
            </div>


          </div>

          <div className="dp-f a-c j-c">
            <Info label="跟进方式" type="radio" data={followType} value={state.followType} onChange={(value) => {
              this.setState({followType: value})
            }}/>
          </div>
          <div className="dp-f a-c j-c">
            <Info label="跟进内容" value={state.followContent} onChange={(value) => {
              this.setState({followContent: value})
            }}/>
          </div>

          <div><Button type="primary" onClick={this.addOneRecord}>新增记录</Button></div>

          <div style={{paddingTop: 10}}>
            <h1 style={{textAlign: "center", marginBottom: 10, fontSize: 20}}>跟进历史</h1>
            <Table columns={this.columns} dataSource={this.props.follow.list} rowKey="id" size="small" pagination={false}/>
          </div>
        </div>
      </Card>

    )
  }
}



