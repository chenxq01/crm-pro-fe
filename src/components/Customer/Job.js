/**
 * Created by chenxq on 2017/12/25.
 */
import React, {PureComponent} from "react";
import {connect} from "dva";
import {
  Button,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  message,
  Modal,
  Popconfirm,
  Radio,
  Row,
  Select,
  Table
} from "antd";
import styles from "../../routes/TableList.less";
const FormItem = Form.Item;
const {Option} = Select;
const {RangePicker} = DatePicker;
const RadioGroup = Radio.Group;
@connect(state => ({
  job: state.job
}))
@Form.create()
export default class extends PureComponent {
  state = {
    customerId: this.props.customerId || 0,
    editObj: {},
    isvisible: false,
    loading:false,
  };
  defaultData = {};
  columns = [{
    title: '单位名称',
    dataIndex: 'company'
  }, {
    title: '职务',
    dataIndex: 'duty'
  }, {
    title: '单位地址',
    dataIndex: 'companyAddr'
  }, {
    title: '社保情况',
    dataIndex: 'provident'
  }, {
    title: '公积金',
    dataIndex: 'social'
  }, {
    title: '保单',
    dataIndex: 'insurance'
  }, {
    title: '备注',
    dataIndex: 'remark'
  }, {
    title: '操作',
    dataIndex: 'jobId',
    render: (jobId, record) => {
      return (<div className="operations">
        <a onClick={this.showDetail.bind(this, record)}>详情</a>
        <Divider type="vertical"/>
        <Popconfirm title="确认要删除该职业信息?" onConfirm={this.remove.bind(this, record.jobId)}><a>删除</a></Popconfirm>
      </div>)
    }
  }];
  showDetail = (editObj) => {
    this.setState({
      isvisible: true,
      editObj: {
        ...editObj
      }
    })
  };
  handleCancel = () => {
    this.setState({
      isvisible: false
    })
  };
  remove = (id) => {
    let _this = this;
    this.props.dispatch({
      type: 'job/fetchRemove',
      payload: {
        customerId: this.state.customerId,
        ids: [id]
      }
    }).then(jsonData => {
      if (jsonData.result) {
        message.success('删除成功', 0.8, () => {
          _this.getList();
        })
      } else {
        message.error(jsonData.msg || '删除失败')
      }
    })
  };
  submit = () => {
    let _this = this;
    let {editObj} = this.state;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('values', values);
        let type = editObj.jobId ? 'job/fetchEdit' : 'job/fetchAdd';
        values.jobId = editObj.jobId;
        _this.props.dispatch({
          type,
          payload: {
            customerId: this.state.customerId,
            params: values
          }
        }).then(jsonData => {
          if (jsonData.result) {
            message.success('保存成功', 0.8, () => {
              _this.setState({
                isvisible: false
              });
              _this.getList();
            })
          } else {
            message.error(jsonData.msg || '操作失败')
          }
        })
      }
    })
  };
  getList = () => {
    let {customerId} = this.state;
    this.props.dispatch({
      type: 'job/fetchList',
      payload: customerId
    })
  };
  getFormCell = () => {
    return [{
      label: '单位名称',
      key: 'company',
      component: <Input placeholder=""/>,
      field: {rules: [{required: true, message: '请输入单位名称'}]},
      text: ''
    }, {
      label: '单位性质',
      key: 'companyProperty',
      component: <Input placeholder=""/>,
      field: {rules: [{required: true, message: '请输入单位性质'}]},
      text: ''
    }, {
      label: '行业性质',
      key: 'industryProperty',
      component: <Input placeholder=""/>,
    }, {
      label: '单位地址',
      key: 'companyAddr',
      component: <Input placeholder=""/>,
    }, {
      label: '职务',
      key: 'duty',
      component: <Input placeholder=""/>,
    }, {
      label: '代发月薪',
      key: 'salary',
      component: <Input placeholder=""/>,
    }, {
      label: '工资发放形式',//TODO
      key: 'salaryType',
      component: <Input placeholder=""/>,
    }, {
      label: '代发银行',
      key: 'salaryBank',
      component: <Input placeholder=""/>,
    }, {
      label: '发薪日',
      key: 'salaryDate',
      component: <Input placeholder=""/>,
    }, {
      label: '其他收入',
      key: 'otherSalary',
      component: <Input placeholder=""/>,
    }, {
      label: '收入来源',
      key: 'salarySource',
      component: <Input placeholder=""/>,
    }, {
      label: '最近半年流水额',
      key: 'salaryAmount',
      component: <Input placeholder=""/>,
    }, {
      label: '总工龄',
      key: 'workYears',
      component: <Input placeholder=""/>,
    }, {
      label: '单位电话',
      key: 'landline',
      component: <Input placeholder=""/>,
    }, {
      label: '社保情况',//TODO
      key: 'provident',
      component: <Input placeholder=""/>,
    }, {
      label: '公积金账号',
      key: 'providentNo',
      component: <Input placeholder=""/>,
    }, {
      label: '公积金密码',
      key: 'providentPwd',
      component: <Input placeholder=""/>,
    }, {
      label: '现单位购买时间',
      key: 'providentPayDate',
      component: <Input placeholder=""/>,
    }, {
      label: '公积金所在地',
      key: 'providentPlace',
      component: <Input placeholder=""/>,
    }, {
      label: '单边缴费',
      key: 'providentHalf',
      component: <Input placeholder=""/>,
    }, {
      label: '单边缴费类型',
      key: 'providentType',
      component: <Input placeholder=""/>,
    }, {
      label: '公积金',
      key: 'social',
      component: <Input placeholder=""/>,
    }, {
      label: '社保账号账号',
      key: 'socialNo',
      component: <Input placeholder=""/>,
    }, {
      label: '社保密码',
      key: 'socialPwd',
      component: <Input placeholder=""/>,
    }, {
      label: '现单位购买时间',
      key: 'socialPayDate',
      component: <Input placeholder=""/>,
    }, {
      label: '社保所在地',
      key: 'socialPlace',
      component: <Input placeholder=""/>,
    }, {
      label: '医疗基数',
      key: 'medical',
      component: <Input placeholder=""/>,
    }, {
      label: '保单',//TODO
      key: 'insurance',
      component: <Input placeholder=""/>,
    }, {
      label: '购买时间',
      key: 'insurancePayDate',
      component: <Input placeholder=""/>,
    }, {
      label: '保险类型',
      key: 'insuranceType',
      component: <Input placeholder=""/>,
    }, {
      label: '购买公司',
      key: 'insuranceCompany',
      component: <Input placeholder=""/>,
    }, {
      label: '缴费周期',
      key: 'insurancePeriod',
      component: <Input placeholder=""/>,
    }, {
      label: '缴费额',
      key: 'insuranceAmount',
      component: <Input placeholder=""/>,
    }, {
      label: '员工人数',
      key: 'employeeNum',
      component: <Input placeholder=""/>,
    }, {
      label: '入职时间',
      key: 'hireDate',
      component: <Input placeholder=""/>,
    }, {
      label: '任职部门',
      key: 'department',
      component: <Input placeholder=""/>,
    }, {
      label: '备注',
      key: 'remark',
      span: 24,
      formItemLayout: {
        labelCol: {
          xs: {span: 24},
          sm: {span: 4},
        },
        wrapperCol: {
          xs: {span: 24},
          sm: {span: 16},
        },
      },
      component: <Input.TextArea placeholder=""/>
    }]
  }

  componentDidMount() {
    let {customerId} = this.state;
    if (!customerId) {
      message.error('用户信息有误');
    }
    this.getList();
  }

  render() {
    let {getFieldDecorator} = this.props.form;
    let {editObj} = this.state;
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 8},
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 16},
      },
    };

    return (<div>
      <div className={styles.tableListOperator}>
        <Button icon="plus" type="primary" onClick={this.showDetail.bind(this, this.defaultData)}>
          新增职业
        </Button>
      </div>
      <div style={{marginTop: "20px"}}>
        <Table rowKey="jobId"
               loading={this.props.job.loading}
               columns={this.columns}
               dataSource={this.props.job.list}
        />
      </div>
      <Modal title={editObj.jobId ? "编辑职业信息" : "新增职业信息"}
             onOk={this.submit}
             onCancel={this.handleCancel}
             visible={this.state.isvisible}
             width={700}
      >
        <Form className={styles.modal}>
          <Row gutter={24}>
            {this.getFormCell().map((item, index) => {
              return <Col span={item.span || 12} key={item.key}>
                <FormItem label={item.label} {...(item.formItemLayout || formItemLayout)}>
                  {getFieldDecorator(item.key, {
                    initialValue: editObj[item.key] || '',
                    ...item.field
                  })(
                    item.component
                  )}
                </FormItem>
              </Col>
            })}
          </Row>
        </Form>
      </Modal>

    </div>)
  }
}
