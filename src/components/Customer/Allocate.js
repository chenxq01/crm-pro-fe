/**
 * Created by chenxq on 2017/12/16.
 */
import React, {Component} from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { Form ,Input  ,Select  ,Modal,Checkbox } from 'antd';
import styles from './index.less';
import {reg} from '../../utils/CheckList';
const FormItem =Form.Item;
const CheckboxGroup = Checkbox.Group;

@Form.create()
export default class extends Component{
  static defaultProps={
    num:0,
    visible:false,
    userList:[]
  }

  constructor(props){
    super(props);
    this.state={
      userIds:[],
      userError:false
    }
  }

  submit=()=>{
    let _this=this;
    this.props.form.validateFieldsAndScroll((err,values)=> {
      let userIds=_this.state.userIds;
      if(userIds.length<=0){
        _this.setState({
          userError:true
        })
        return ;
      }
      if (!err) {
        values.userIds=userIds;
        this.props.onSubmit && this.props.onSubmit(values);
      }
    })
  }
  cancel=()=>{
    this.props.onCancel && this.props.onCancel();
  }

  onChangeUserId=(userIds)=>{
    this.setState({
      userIds,
      userError:userIds.length<=0?true:false
    })

  }

  render(){
    let {num, visible} = this.props;
    let {getFieldDecorator} =  this.props.form;
    let {userError} = this.state;
    const col={
      labelCol:{span:5},
      wrapperCol:{span:12}
    }
    return (<Modal title='分配客户'
                    onOk={this.submit}
                    onCancel={this.cancel}
                    visible={visible}>
      <Form>
        <FormItem label="分配数量" {...col}>
          {
            getFieldDecorator('num',{
              initialValue:num || '',
              rules:[{required:true,message:'请输入分配数量'},{
                pattern:reg.number, message:'请输入小于10000正整数'
              }]
            })(
              <Input placeholder="请输入分配数量" disabled={!!num}/>
            )
          }
        </FormItem>
        <div className="ant-row ant-form-item">
          <div className="ant-col-5 ant-form-item-label">
            <label className="ant-form-item-required ">
              分配人
            </label>
          </div>
          <div className="ant-col-12 ant-form-item-control-wrapper">
            <div className={"ant-form-item-control "+(userError?'has-error':'') }>
              {num>0?<SelectOne userList={this.props.userList}
                                onChangeUserId={this.onChangeUserId}></SelectOne>:
                <SelectTwo userList={this.props.userList}
                           onChangeUserId={this.onChangeUserId}></SelectTwo>
              }
              {userError && <div className="ant-form-explain">请选择分配人</div>}
            </div>
          </div>
        </div>

      </Form>


    </Modal>)
  }
}


class SelectOne extends Component{
  static defaultProps={
    userList:[],
    onChangeUserId:null
  }
  state={
    userId:0
  }
  choseUser=(user)=>{
    this.setState({
      userId:user.id
    },()=>{
      this.props.onChangeUserId && this.props.onChangeUserId([user.id]);
    })

  }
  getValue(){
    return [this.state.userId];
  }
  render(){
    let {userList} = this.props;
    let {userId}= this.state;
    return (<div className="select-content">
      {
        userList.map((item,index)=>{
          let users=item.users || [];
          if(users.length==0){
            return null;
          }
          return <div key={index}>
            <span className="groupSpan">{item.groupName}</span>
            <ul>
              {users.map(user=>{
                return (<li key={user.id} onClick={this.choseUser.bind(this,user)}>
                  <span className={userId===user.id?'li-active':"li-span"} >{user.userName}</span>
                </li>)
              })}
            </ul>

          </div>
        })
      }
    </div>)
  }
}


class SelectTwo extends Component{
  static defaultProps={
    userList:[],
    onChangeUserId:null
  }
  state={
    checkedList:[],
    indeterminate:false
  }
  onCheckAllChange=(index,e)=>{
    let {checkedList} = this.state;
    let {userList} = this.props;
    let group= userList[index] || {};
    let checked=e.target.checked;
    let users=group.users || [];
    users.forEach(item=>{
      let id=item.id;
      let index= checkedList.findIndex(one=>one == id);
      if(checked && index<0){
        checkedList.push(id);
      }
      if(!checked && index>=0){
        checkedList.splice(index,1);
      }
    })
    this.setState({
      checkedList
    },()=>{
      this.props.onChangeUserId && this.props.onChangeUserId(checkedList);
    })
  }
  onCheck=(id,e)=>{
    let {checkedList}=this.state;
    let index=checkedList.findIndex(i=>i==id);
    if(e.target.checked && index<0){
      checkedList.push(id);
    }
    if(!e.target.checked && index>=0){
      checkedList.splice(index,1);
    }
    this.setState({
      checkedList
    },()=>{
      this.props.onChangeUserId && this.props.onChangeUserId(checkedList);
    })
  }
  getCheckAllState=(index)=>{
    let {checkedList} = this.state;
    let {userList} = this.props;
    let group= userList[index] || {};
    let users=group.users || [];
    let flag=true;
    users.forEach(item=>{
      if(checkedList.findIndex(one=>one==item.id)<0){
        flag=false;
      }
    })
    return flag;
  }
  getValue(){
    return this.state.checkedList;
  }
  render(){
    let {userList} = this.props;
    let _this=this;
    let {checkedList}=this.state;
    return (<div className="select-content">
      {
        userList.map((item,index)=>{
          let users=item.users || [];
          if(users.length==0){
            return null;
          }

          return (<div key={index}>
            <Checkbox
              indeterminate={_this.state.indeterminate}
              onChange={_this.onCheckAllChange.bind(_this,index)}
              checked={_this.getCheckAllState(index)}
            >
              {item.groupName}
            </Checkbox>
            <div style={{paddingLeft:15}}>
              {users.map(user=>{
                let checked= checkedList.findIndex(i=>i==user.id) >=0
                return (<Checkbox
                  indeterminate={_this.state.indeterminate}
                  onChange={_this.onCheck.bind(_this,user.id)}
                  checked={checked}
                  key={user.id}
                >
                  {user.userName}
                </Checkbox>)
              })}
            </div>
          </div>)
        })
      }

    </div>)
  }
}
