/**
 * Created by chenxq on 2017/12/25.
 */
import React,{PureComponent} from 'react';
import { connect } from 'dva';
import { Table, Card, Button ,Form ,Input ,Row ,Col ,Select ,Popconfirm ,Modal ,message ,Divider,DatePicker,Radio, } from 'antd';
import {findObjByKey ,convertLongToString} from '../../utils/index';
import styles from '../../routes/TableList.less';
import reg from '../../utils/CheckList';
const FormItem = Form.Item;
const { Option } = Select;
const {RangePicker} = DatePicker;
const RadioGroup = Radio.Group;
@connect(state=>({
  enterprise:state.enterprise
}))
@Form.create()
export default class extends PureComponent{
  state={
    customerId:this.props.customerId || 0,
    editObj:{},
    isvisible:false,
  }
  defaultData={};
  columns=[{
    title:'企业名称',
    dataIndex:'name'
  },{
    title:'企业性质',
    dataIndex:'property'
  },{
    title:'所属行业',
    dataIndex:'industry'
  },{
    title:'企业地址',
    dataIndex:'addr'
  },{
    title:'客户占股比例',
    dataIndex:'customerShare'
  },{
    title:'年营业额（万）',
    dataIndex:'annualSales'
  },{
    title:'年纯利（万）',
    dataIndex:'annualProfit'
  },{
    title:'注册时间',
    dataIndex:'registerTime',
  },{
    title:'备注',
    dataIndex:'remark'
  },{
    title:'操作',
    dataIndex:'enterpriseId',
    render:(enterpriseId,record)=>{
      return (<div className="operations">
        <a onClick={this.showDetail.bind(this,record)}>详情</a>
        <Divider type="vertical" />
        <Popconfirm title="确认要删除该条企业信息?" onConfirm={this.remove.bind(this,record.enterpriseId)}><a>删除</a></Popconfirm>
      </div>)
    }
  }];
  showDetail=(editObj)=>{
    this.setState({
      isvisible:true,
      editObj:{
        ...editObj
      }
    })
  }
  handleCancel=()=>{
    this.setState({
      isvisible:false
    })
  }
  remove=(id)=>{
    let _this=this;
    this.props.dispatch({
      type:'enterprise/fetchRemove',
      payload:{
        customerId:this.state.customerId,
        ids:[id]
      }
    }).then(jsonData=>{
      if(jsonData.result){
        message.success('删除成功',0.8,()=>{
          _this.getList();
        })
      }else{
        message.error(jsonData.msg || '删除失败')
      }
    })
  }
  submit=()=>{
    let _this=this;
    let {editObj}= this.state;
    this.props.form.validateFieldsAndScroll((err,values)=>{
      if(!err) {
        console.log('values',values);
        let type= editObj.enterpriseId?'enterprise/fetchEdit':'enterprise/fetchAdd';
        values.enterpriseId=editObj.enterpriseId;
        _this.props.dispatch({
          type,
          payload:{
            customerId:this.state.customerId,
            params:values
          }
        }).then(jsonData=>{
          if(jsonData.result){
            message.success('保存成功',0.8,()=>{
              _this.setState({
                isvisible:false
              })
              _this.getList();
            })
          }else{
            message.error(jsonData.msg || '操作失败')
          }
        })
      }
    })
  }
  getList=()=>{
    let {customerId} = this.state;
    this.props.dispatch({
      type:'enterprise/fetchList',
      payload:customerId
    })
  }
  getFormCell=()=>{
    return [{
      label:'企业名称',
      key:'name',
      component: <Input placeholder="请输入企业名称" />,
      field:{rules:[{required:true,message:'请输入企业名称'}]},
      text:''
    },{
      label:'企业性质',
      key:'property',
      component: <Input placeholder="请输入企业性质" />,
      field:{rules:[{required:true,message:'请输入企业性质'}]},
      text:''
    },{
      label:'所属行业',
      key:'industry',
      component: <Input placeholder="请输入所属行业" />,
    },{
      label:'企业法人',
      key:'businessEntity',
      component: <Input placeholder="请输入企业法人" />,
    },{
      label:'股东人数',
      key:'shareholderNum',
      component: <Input placeholder="请输入股东人数" />,
      field:{
        rules:[{pattern:reg.intNumber,message:'请输入正整数'}]
      }
    },{
      label:'客户占股比例',
      key:'customerShare',
      component: <Input placeholder="请输入客户占股比例" />,
    },{
      label:'注册时间',
      key:'registerTime',
      component: <Input placeholder="请输入注册时间" />,
    },{
      label:'实际经营地',
      key:'addr',
      span:24,
      formItemLayout : {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 4 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
      },
      component: <Input placeholder="请输入地址" />,
    },{
      label:'员工数',
      key:'employeeNum',
      component: <Input placeholder="请输入员工数" />,
    },{
      label:'对私流水',
      key:'privateWater',
      component: <Input placeholder="请输入对私流水" />,
    },{
      label:'对公流水',
      key:'publicWater',
      component: <Input placeholder="请输入对公流水" />,
    },{
      label:'年营业额(万)',
      key:'annualSales',
      component: <Input placeholder="请输入年营业额" />,
    },{
      label:'年纯利(万)',
      key:'annualProfit',
      component: <Input placeholder="请输入年纯利" />,
    },{
      label:'年税收额',
      key:'annualTax',
      component: <Input placeholder="请输入年税收额" />,
    },{
      label:'备注',
      key:'remark',
      span:24,
      formItemLayout : {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 4 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
      },
      component: <Input.TextArea  placeholder="请输入备注"  />
    }]
  }
  componentDidMount(){
    let {customerId} = this.state;
    if(!customerId){
      message.error('用户信息有误');
    }
    this.getList();
  }
  render(){
    let {getFieldDecorator} = this.props.form;
    let {editObj}= this.state;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };

    return (<div>
      <div className={styles.tableListOperator}>
        <Button icon="plus" type="primary" onClick={this.showDetail.bind(this,this.defaultData)}>
          新增企业
        </Button>
      </div>
      <div style={{marginTop: "20px"}}>
        <Table rowKey="enterpriseId"
               loading={this.props.enterprise.loading}
               columns={this.columns}
               dataSource={this.props.enterprise.list}
        />
      </div>
      <Modal title={editObj.enterpriseId ? "编辑企业信息":"新增企业信息"}
             onOk={this.submit}
             onCancel={this.handleCancel}
             visible={this.state.isvisible}
             width={700}
      >
        <Form className={styles.modal}>
          <Row gutter={24}>
            {this.getFormCell().map((item,index)=>{
              return <Col span={item.span || 12} key={item.key}>
                <FormItem label={item.label} {...(item.formItemLayout || formItemLayout)}>
                  {getFieldDecorator(item.key,{
                    initialValue:editObj[item.key] || '',
                    ...item.field
                  })(
                    item.component
                  )}
                </FormItem>
              </Col>
            })}
          </Row>
        </Form>
      </Modal>

    </div>)
  }
}
