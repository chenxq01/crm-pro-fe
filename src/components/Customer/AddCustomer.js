/**
 * Created by chenxq on 2017/12/16.
 */
import React,{PureComponent,Component} from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { Form ,Input  ,Select  ,Modal } from 'antd';
import {channelList} from '../../routes/defaultData';
import {reg} from '../../utils/CheckList';
import styles from '../../routes/TableList.less';
const FormItem=Form.Item;
const { Option } = Select;
@Form.create()
export default class extends Component{
  static defaultProps={
    type: 'PUBLIC',  // PRIVATE   PUBLIC
    onSubmit:null,
    onCancel:null,
    addVisible:false
  }

  submit=()=>{
    this.props.form.validateFieldsAndScroll((err,values)=> {
      if (!err) {
        this.props.onSubmit && this.props.onSubmit(values);
      }
    })
  }

  cancel=()=>{
    this.props.onCancel && this.props.onCancel()
  }
  render(){
    let {getFieldDecorator} =  this.props.form;
    return (<Modal title='新增客户'
                   onOk={this.submit}
                   onCancel={this.cancel}
                   visible={this.props.addVisible}
    >
      <Form className={styles.modal}>
        <div className="flex-row">
          <div className="flex1">
            <FormItem label="客户姓名">
            {
              getFieldDecorator('name',{
                initialValue:'',
                rules:[{
                  required:true,
                  message:'请输入客户姓名'
                }]
              })(
                <Input placeholder="请输入客户姓名" style={{width:"80%"}} />
              )

            }
            </FormItem>
          </div>
          <div className="flex1">
            <FormItem label="拼音">
            {
              getFieldDecorator('pinyin',{
                initialValue:'',
              })(
                <Input placeholder="" style={{width:"80%"}} />
              )
            }
            </FormItem>
          </div>
        </div>
        <div className="flex-row">
          <div className="flex1">
            <FormItem label="性别">
              {
                getFieldDecorator('gender',{
                  initialValue:'MAN',
                  rules:[{
                    required:true,
                    message:"请选择客户性别"
                  }]
                })(

                  <Select style={{width:"80%"}}>
                    <Option value="MAN">男</Option>
                    <Option value="WOMAN">女</Option>
                  </Select>
                )

              }
            </FormItem>
          </div>
          <div className="flex1">
            <FormItem label="手机号码">
              {
                getFieldDecorator('phone',{
                  initialValue:'',
                  rules:[{
                    required:true,
                    message:'请输入手机号码'
                  },{
                    pattern:reg.mobile,message:'手机号格式不正确'
                  }]
                })(
                  <Input placeholder="" style={{width:"80%"}} />
                )
              }
            </FormItem>
          </div>
        </div>
        <div className="flex-row">
          <div className="flex1">
            <FormItem label="来源渠道">
              {
                getFieldDecorator('channel',{
                  initialValue:channelList[0].key,
                  rules:[{required:true,message:"请选择来源渠道"}]
                })(

                  <Select style={{width:"80%"}}>
                    {channelList.map(item=><Option value={item.key} key={item.key}>{item.name}</Option>)}
                  </Select>
                )

              }
            </FormItem>
          </div>
          <div className="flex1">
            <FormItem label="等级">
              {
                getFieldDecorator('grade',{
                  initialValue:'',
                  rules:[{
                    required:false,
                    message:'客户等级'
                  }]
                })(
                  <Input placeholder="请输入客户等级" style={{width:"80%"}} />
                )
              }
            </FormItem>
          </div>
        </div>
        <FormItem label="备注">
          {getFieldDecorator('remark',{
            initialValue:'',
          })(
            <Input.TextArea placeholder='请输入客户备注' style={{height:"70px"}}  />
          )}
        </FormItem>
      </Form>


    </Modal>)
  }
}
