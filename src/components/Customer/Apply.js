/**
 * Created by chenxq on 2017/12/17.
 */
import React, {Component} from 'react';
import { Form,Input,Modal,Radio } from 'antd';
import {reg} from '../../utils/CheckList';
import {customerType} from '../../routes/defaultData';
const FormItem =Form.Item;
const RadioGroup = Radio.Group;
@Form.create()
export default class Apply extends Component{

  submit=()=>{
    let _this=this;
    this.props.form.validateFieldsAndScroll((err,values)=>{
      if(!err){
        _this.props.onSubmit && _this.props.onSubmit(values);
      }
    })
  }

  cancel=()=>{
    this.props.onCancel && this.props.onCancel();

  }

  render(){
    let {getFieldDecorator} =  this.props.form;
    const col={
      labelCol:{span:5},
      wrapperCol:{span:12}
    }

    return (<Modal title='申请客户'
                   onOk={this.submit}
                   onCancel={this.cancel}
                   visible={this.props.visible}
    >
      <Form>
        <FormItem label="申请数量" {...col}>
          {
            getFieldDecorator('num',{
              rules:[{required:true,message:'请输入申请数量'},{
                pattern:reg.number, message:'请输入小于10000正整数'
              }]
            })(
              <Input placeholder="请输入申请数量" />
            )
          }
        </FormItem>
        <FormItem label="客户类型" {...col}>
          {
            getFieldDecorator('type',{
              initialValue:'PRIVATE',
              rules:[{required:true,message:'请选择客户类型'},{
              }]
            })(
              <RadioGroup>
                {customerType.map(item=>{
                  return <Radio key={item.key} value={item.key}>{item.name}</Radio>
                })}

              </RadioGroup>
            )
          }
        </FormItem>

      </Form>
    </Modal>)
  }
}
