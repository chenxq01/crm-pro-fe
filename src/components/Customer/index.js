/**
 * Created by chenxq on 2017/12/8.
 */
import React, { PureComponent } from 'react';
import moment from 'moment';
import { Table, Alert, Badge, Divider ,Popconfirm} from 'antd';
import { convertLongToString } from '../../utils/index';
import styles from './index.less';

const statusMap = ['default', 'processing', 'success', 'error'];
const getName = ({state,type,intention}) => {
  if (state === "FOLLOW") {
    return "营销客户";
  }
  if (state === "INVALID") {
    return "无效客户";
  }
  //if (state === "FOLLOW" && intention === true) {
  //    return "意向客户";
  //}
  if (state === "OVERTIME") {
    return "公共客户";
  }
  if (type === "ORDINARY") {
    return "普通客户";
  }
  if (type === "HIGH") {
    return "优质客户";
  }
  return "未知客户";
};
export default class extends PureComponent {
  state = {
    selectedRowKeys: [],
    totalCallNo: 0,
  };

  componentWillReceiveProps(nextProps) {
    // clean state
    if (nextProps.selectedRows.length === 0) {
      this.setState({
        selectedRowKeys: [],
        totalCallNo: 0,
      });
    }
  }

  handleRowSelectChange = (selectedRowKeys, selectedRows) => {
    const totalCallNo = selectedRows.reduce((sum, val) => {
      return sum + parseFloat(val.callNo, 10);
    }, 0);

    if (this.props.onSelectRow) {
      this.props.onSelectRow(selectedRows);
    }

    this.setState({ selectedRowKeys, totalCallNo });
  }

  handleTableChange = (pagination, filters, sorter) => {
    this.props.onChange(pagination, filters, sorter);
  }

  cleanSelectedKeys = () => {
    this.handleRowSelectChange([], []);
  }

  render() {
    const { selectedRowKeys, totalCallNo } = this.state;
    const { data: { list, pagination }, loading } = this.props;

    const status = ['关闭', '运行中', '已上线', '异常'];

    const columns = [{
      title: '客户姓名',
      dataIndex: 'name'

    }, {
      title: "客户类型",
      dataIndex: "type",
      //eslint-disable-next-line
      render: type => type === "HIGH" ? "优质客户" : "普通客户"
    }, {
      title: "渠道",
      dataIndex: "channel"
    }, {
      title: "跟进人",
      dataIndex: "followerName"
    }, {
      title: "跟进天数",
      dataIndex: "followDays"
    }, {
      title: "最后跟进时间",
      dataIndex: "lastFollowTime",
      //sorter:true,
      render: (datetime)=> {
        return datetime ? convertLongToString(datetime) : ""
      }
    }, {
      title: "认领时间",
      dataIndex: "ownTime",
      render: (datetime)=> {
        return datetime ? convertLongToString(datetime) : ""
      }
    },{
      title:"创建时间",
      dataIndex:"createTime",
      render:(datetime)=>{
        return datetime ? convertLongToString(datetime) : "";
      }
    }, {
      title: '客户状态',
      dataIndex: 'state',
      render: (state, record) => {

        return <span>{state ? getName(record) : ""}</span>
      }
    }, {
      title: "操作",
      dataIndex: "id",
      render: (id, record) => {
        let name = getName(record);
        let type1 = name === "公共客户";
        //let 优质客户 = name==="优质客户";
        //let 普通客户 = name==="普通客户";
        let type4 = name === "营销客户";
        //let 无效客户 = name==="无效客户";
        let type6 = name === "意向客户";
        return (
          <div className="operations" onClick={(e)=>{e.stopPropagation();}}>
            <a onClick={this.showDetail.bind(this,id)}>{(type4 || type6) ? "编辑" : "详情"}</a>
            {(type4 || type6) && <a onClick={this.showDetail.bind(this,id,"2")}>跟进</a>}
            {(type4 || type6) && <a onClick={this.showDetail.bind(this,id,"3")}>计划</a>}
            {(type4 || type6) && <Popconfirm title="确认要放弃跟进该客户?" onConfirm={this.quitCustomer.bind(this,id)}>
              <a>放弃</a>
            </Popconfirm>}
            {(type1) && <a onClick={this.claim.bind(this,id)}>认领</a>}
            {/*!(无效客户) && <Popconfirm title="确认要将该客户标记为无效客户?" onConfirm={this.customerInvalid.bind(this,id)}><a>无效</a></Popconfirm>*/}
          </div>
        )
      }

    }];

    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      ...pagination,
    };

    const rowSelection = {
      selectedRowKeys,
      onChange: this.handleRowSelectChange,
      getCheckboxProps: record => ({
        disabled: record.disabled,
      }),
    };

    return (
      <div className={styles.standardTable}>
        <Table
          loading={loading}
          rowKey={record => record.key}
          rowSelection={rowSelection}
          dataSource={list}
          columns={columns}
          pagination={paginationProps}
          onChange={this.handleTableChange}
        />
      </div>
    );
  }
}

