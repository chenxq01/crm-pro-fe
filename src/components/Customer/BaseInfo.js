/**
 * Created by chenxq on 2017/12/25.
 */
import React, {Component} from 'react';
import { connect} from 'dva';
import {Form ,Input ,Button ,Row, Col ,Select,message} from 'antd';
import {reg} from '../../utils/CheckList';
import styles from './index.less';
import {channelList,contactsRelation} from '../../routes/defaultData';
import {findObjByKey ,convertLongToString} from '../../utils/index'
const FormItem=Form.Item;
const { Option } = Select;

@connect(state=>({
  customer:state.customer
}))
@Form.create()
export default  class BaseInfo extends Component{
  constructor(props){
    super(props);
    this.state={
      editable:false
    }
  }

  changeEdit=(editable)=>{
    this.setState({
      editable
    })
  }

  getCell=()=>{
    let {curInfo} = this.props.customer;
    let baseInfo=curInfo.info || {};
    return [{
      label:'姓名',
      key:'name',
      component: <Input placeholder="请输入客户姓名" />,
      field:{rules:[{required:true,message:'请输入姓名'}]},
      text:''
    },/*{
      label:'手机号码',
      key:'phone',
      component:<Input placeholder="请输入手机号码"/>,
      field:{
        rules:[{
          required:true,message:'请输入手机号码'
        },{
          pattern:reg.mobile,message:'手机号格式不正确'
        }]
      },
      text:''
    },*/{
      label:'性别',
      key:'gender',
      component:<Select>
        <Option value="MAN">男</Option>
        <Option value="WOMAN">女</Option>
      </Select>,
      field:{
        initialValue: baseInfo.gender || 'MAN',
        rules:[{
          required:true,
          message:"请选择客户性别"
        }]
      },
      text: baseInfo.gender==='WOMAN'?'女':'男'
    },{
      label:'来源渠道',
      key:'channel',
      component:<Select>
        {channelList.map(item=><Option value={item.key} key={item.key}>{item.name}</Option>)}
      </Select>,
      field:{
        initialValue: baseInfo.channel || channelList[0].key,
        rules:[{
          required:true,message:'请选择来源渠道'
        }]
      },
      text: baseInfo.channel && findObjByKey(channelList,baseInfo.channel).name
    },{
      label:'拼音',
      key:'pinyin',
      component: <Input placeholder="请输入姓名拼音"/>,
      field:{},
      text:''
    },{
      label:'备用号码',
      key:'alternatePhone',
      component:<Input placeholder="请输入备用号码"/>,
      field:{
        rules:[{

          pattern:reg.mobile,message:'手机号格式不正确'
        }]
      },
      text:''
    },{
      label:'年龄',
      key:'age',
      component:<Input placeholder="请输入客户年龄"/>,
      field:{
        rules:[{
          pattern:/^[1-9][0-9]{0,2}$/,message:'请输入小于四位的正整数'
        }]
      },
      text:''
    },{
      label:'生日',
      key:'birthday',
      component:<Input placeholder="请输入生日"/>,
      text:''
    },{
      label:'户籍所在地',
      key:'censusPlace',
      component:<Input placeholder="请输入户籍所在地"/>,
      text:''
    },{
      label:'有无子女',
      key:'children',
      component:<Input placeholder="请子女相关信息"/>,
      text:''
    },{
      label:'学历',
      key:'education',
      component:<Input placeholder="请输入最高学历"/>,
      text:''
    },{
      label:'email',
      key:'email',
      component:<Input placeholder="请输入email"/>,
      field:{
        rules:[{
          pattern:reg.email,message:'邮件格式不正确'
        }]
      },
      text:''
    },{
      label:'身份证号',
      key:'identityNo',
      component:<Input placeholder="请输入身份证号"/>,
      field:{
        /*rules:[{
         pattern:reg,message:'身份证号格式不正确'
         }]*/
      },
      text:''
    },{
      label:'证件有效期',
      key:'identityPeriod',
      component:<Input placeholder="请输入"/>,
      text:''
    },{
      label:'现居地',
      key:'livePlace',
      component:<Input placeholder="请输入现在居住地址"/>,
      text:''
    },{
      label:'客户所在城市',
      key:'location',
      component:<Input placeholder="请输入客户所在城市"/>,
      text:''
    },{
      label:'婚姻状态',
      key:'marriage',
      component:<Input placeholder="请输入婚姻状态"/>,
      text:''
    },{
      label:'QQ号',
      key:'qq',
      component:<Input placeholder="请输入QQ号"/>,
      text:''
    },{
      label:'微信号',
      key:'wechat',
      component:<Input placeholder="请输入微信号"/>,
      text:''
    },{
      label:'备注',
      key:'remark',
      component:<Input placeholder="请输入备注"/>,
      text:''
    }]
  }

  submit=()=>{
    let _this=this;
    let {curInfo} = this.props.customer;
    this.props.form.validateFieldsAndScroll((err,values)=> {
      if (!err) {
        values={
          ...values,
          id:curInfo.id
        };
        this.props.dispatch({
          type:'customer/fetchEditCustomerInfo',
          payload:values
        }).then(jsonData=>{
          if(jsonData.result){
            message.success('修改成功',0.8,()=>{
              _this.changeEdit(false);
              _this.props.dispatch({
                type:'customer/fetchCustomerDetail',
                payload:_this.state.customerId
              })
            })
          }else{
            message.error(jsonData.msg)
          }
        })
      }
    })
  }

  render(){
    let {curInfo} = this.props.customer;
    let {getFieldDecorator} =  this.props.form;
    let {editable} = this.state;
    let infoRow = this.getCell();
    let baseInfo=curInfo.info || {};
    return <div className={styles.editContent}>
      <div className="top-bar">
        {!editable && <Button type="primary" onClick={this.changeEdit.bind(this,true)}>编辑</Button>}
        {editable && <div>
          <Button type="primary" onClick={this.submit}>保存</Button>
          <Button onClick={this.changeEdit.bind(this,false)}>取消</Button>
        </div>
        }
      </div>
      <Form layout="inline" className="ant-advanced-search-form">
        <Row gutter={24}>
          {
            infoRow.map(item=><Col span={8} key={item.key}>
              <FormItem label={item.label}>
                {getFieldDecorator(item.key,{
                  initialValue:baseInfo[item.key],
                  ...item.field
                })(
                  editable?item.component:<span>{ item.text || baseInfo[item.key]}</span>
                )}
              </FormItem>
            </Col>)
          }
        </Row>
      </Form>
    </div>;
  }
}
