/**
 * Created by chenxq on 2017/12/31.
 */
/**
 * Created by chenxq on 2017/12/16.
 */
import React, {Component} from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { Form,Modal,Checkbox,Row,Col,Radio } from 'antd';
import {reg} from '../../utils/CheckList';
const FormItem =Form.Item;
const CheckboxGroup = Checkbox.Group;
@connect(state=>({

}))
@Form.create()
export default class extends Component{
  static defaultProps={
    isvisible:false,
    onCancel:null,
    onSubmit:null,
    radio:1,
    selectArr:[]
  }

  constructor(props){
    super(props);
    this.state={
      groupList:[],
      userList:[],
      radio:this.props.radio,
      selectArr:this.props.selectArr
    }
  }

  submit=()=>{
    let {radio,selectArr} = this.state;
    this.props.onSubmit && this.props.onSubmit({
      radio,
      selectArr
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      radio:nextProps.radio,
      selectArr:nextProps.selectArr
    })
  }
  cancel=()=>{
    this.props.onCancel && this.props.onCancel();
  }
  typeChange=(e)=>{
    let val=e.target.value;
    this.setState({
      radio:val,
      selectArr:[]
    })
  }
  groupCheckAll=(type,e)=>{
    let groupList = this.props.groupList || [];
    let userList=this.props.userList || [];
    if(e.target.checked){
      let arr=[];
      if(type==1){
        arr=groupList.map(item=>item.id);
      }else{
        arr=userList.map(item=>item.id);
      }
      this.setState({
        selectArr:arr
      })
    }else{
      this.setState({
        selectArr:[]
      })
    }
  }

  getAllChecked=(type)=>{
    let {selectArr} = this.state;
    let {groupList,userList} = this.props;
    if(type==1){//用户组
      return groupList.length === selectArr.length;
    }else{
      return userList.length === selectArr.length;
    }
  }
  changeSingle=(val)=>{
    this.setState({
      selectArr:val
    })
  }
  render(){
    let {isvisible,groupList,userList} = this.props;
    let {radio,selectArr} = this.state;
    const col={
      labelCol:{span:5},
      wrapperCol:{span:12}
    }
    let _this=this;
    return (<Modal title='切换角色'
                   onOk={this.submit}
                   onCancel={this.cancel}
                   visible={isvisible}
                   width={750}
    >
      <Row gutter={24}>
        <Col span={3}>
          <Radio value={1}
                 checked={radio==1?true:false}
                 onChange={this.typeChange}

          >分组</Radio>
        </Col>
        <Col span={21}>
          <Row style={{borderBottom:"1px dashed #dfdfdf",padding:5}}>
            <Col span={24}>
              <Checkbox disabled={radio!==1}
                        onChange={this.groupCheckAll.bind(this,1)}
                        checked={this.getAllChecked(1)}
              >全部</Checkbox>
            </Col>
          </Row>
          <Row style={{padding:5}}>
            <Checkbox.Group disabled={radio!==1}
                            value={selectArr}
                            onChange={this.changeSingle}
            >
              {groupList.map((one)=>{
                return (
                  <Col span={6}
                    key={one.id}
                  >
                    <Checkbox disabled={radio!==1}
                              value={one.id}>{one.name}</Checkbox>
                  </Col>)
              })}
            </Checkbox.Group>
          </Row>
        </Col>
      </Row>
      <Row gutter={24} style={{marginTop:20}}>
        <Col span={3}>
          <Radio value={2}
                 onChange={this.typeChange}
                 checked={radio==2?true:false}>组员</Radio>
        </Col>
        <Col span={21}>
          <Row style={{borderBottom:"1px dashed #dfdfdf",padding:5}}>
            <Col span={24}>
              <Checkbox disabled={radio!==2}
                        onChange={this.groupCheckAll.bind(this,2)}
                        checked={this.getAllChecked(2)}

              >全部</Checkbox>
            </Col>
          </Row>
          <Row style={{padding:5}}>
            <Checkbox.Group disabled={radio!==2}
                            value={selectArr}
                            onChange={this.changeSingle}
            >
              {userList.map((one)=>{
                return (
                  <Col span={6}
                       key={one.id}
                  >
                    <Checkbox disabled={radio!==2}
                              value={one.id}>{one.userName}</Checkbox>
                  </Col>)
              })}
            </Checkbox.Group>
          </Row>
        </Col>
      </Row>



    </Modal>)
  }
}

