// https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
module.exports = {
  // 'primary-color': '#10e99b',
  'card-actions-background': '#f5f8fa',
  'font-size-base': '14px',
  'badge-font-size': '12px',
  'btn-font-size-lg': '@font-size-base',
  'menu-dark-bg': '#262a34',
  'menu-dark-submenu-bg': '#1d202b',
  'layout-sider-background': '#262d37',
  'layout-body-background': '#f0f2f5',
};
